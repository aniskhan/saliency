vpath=/home/anis/Documents
exepath=/home/anis/Téléchargements/Motion2D-1.3.11/bin/Linux

imgpath=$vpath/temp
mkdir $imgpath

j=1
ffmpeg -i $vpath"/clip_"$j".avi" $imgpath/image%d.png
n=$(ls -l $imgpath | grep image | wc -l)
let n--
$exepath"/Motion2D" -m AC -p $imgpath/image%d.png -f 1 -i $n -r $vpath"/clip_"$j".txt" -v

rm $imgpath/*.png

