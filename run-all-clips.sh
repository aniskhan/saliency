
vpath=/home/anis/Documents/videos
ipath=/home/anis/Documents/images

queuelen=13

mspath=$ipath/static
mdpath=$ipath/dynamic_q$queuelen
msdpath=$(printf "%s/fusion_q%d" $ipath $queuelen)

if [ -n "$mspath" ] ; then
rm -r $mspath
mkdir $mspath
echo "Static removed."
fi
if [ -n "$mdpath" ] ; then
rm -r $mdpath
mkdir $mdpath
echo "Dynamic removed."
fi
if [ -n "$mfpath" ] ; then
echo "Do nothing."
fi
if [ -n "$msdpath" ] ; then
rm -r $msdpath
mkdir $msdpath
echo "Fusion removed."
fi

for j in {1..39}
do
	if [ $j -le 9 ] ; then
		./saliency  --save-images --save-text-files -i $vpath -s $mspath -d $mdpath -m $msdpath -t $queuelen "clp0000"$j
	elif [ $j -le 99 ] ; then
		./saliency --save-images --save-text-files -i $vpath -s $mspath -d $mdpath -m $msdpath -t $queuelen "clp000"$j
	fi
done

for j in {1..45}
do
	if [ $j -le 9 ] ; then
		./saliency --save-images --save-text-files -i $vpath -s $mspath -d $mdpath -m $msdpath -t $queuelen "clp0100"$j
	elif [ $j -le 99 ] ; then
		./saliency --save-images --save-text-files -i $vpath -s $mspath -d $mdpath -m $msdpath -t $queuelen "clp010"$j
	fi
done

for j in {1..14}
do
	if [ $j -le 9 ] ; then
		./saliency --save-images --save-text-files -i $vpath -s $mspath -d $mdpath -m $msdpath -t $queuelen "clp0200"$j
	elif [ $j -le 99 ] ; then
		./saliency --save-images --save-text-files -i $vpath -s $mspath -d $mdpath -m $msdpath -t $queuelen "clp020"$j
	fi
done

for j in {1..28}
do
	if [ $j -le 9 ] ; then
		./saliency --save-images --save-text-files -i $vpath -s $mspath -d $mdpath -m $msdpath -t $queuelen "clp0300"$j
	elif [ $j -le 99 ] ; then
		./saliency --save-images --save-text-files -i $vpath -s $mspath -d $mdpath -m $msdpath -t $queuelen "clp030"$j
	fi
done

for j in {1..25}
do
	if [ $j -le 9 ] ; then
		./saliency --save-images --save-text-files -i $vpath -s $mspath -d $mdpath -m $msdpath -t $queuelen "clp0400"$j
	elif [ $j -le 99 ] ; then
		./saliency --save-images --save-text-files -i $vpath -s $mspath -d $mdpath -m $msdpath -t $queuelen "clp040"$j
	fi
done

