
vpath=/home/anis/Documents
ipath=/home/anis/Documents/images

queuelen=5

mspath=$ipath/static
mdpath=$ipath/dynamic_q$queuelen
msdpath=$(printf "%s/fusion_q%d" $ipath $queuelen)

if [ -n "$mspath" ] ; then
rm -r $mspath
mkdir $mspath
echo "Static removed."
fi
if [ -n "$mdpath" ] ; then
rm -r $mdpath
mkdir $mdpath
echo "Dynamic removed."
fi
if [ -n "$mfpath" ] ; then
echo "Do nothing."
fi
if [ -n "$msdpath" ] ; then
rm -r $msdpath
mkdir $msdpath
echo "Fusion removed."
fi

j=1
./saliency --one-path-dynamic --cascade-name haarcascade_frontalface_alt2.xml -i $vpath -t $queuelen -I 5 -N 15 --display-dynamic "clip_"$j
