vpath=/home/anis/Documents/videos
exepath=/home/anis/Téléchargements/Motion2D-1.3.11/bin/Linux

imgpath=$vpath/temp
mkdir $imgpath

for j in {1..39}
do	
	if [ $j -le 9 ] ; then
		ffmpeg -i $vpath"/clp0000"$j".avi" $imgpath/image%d.png
		n=$(ls -l $imgpath | grep image | wc -l)
		let n--
		$exepath"/Motion2D" -m AC -p $imgpath/image%d.png -f 1 -i $n -r $vpath"/clp0000"$j".txt" -v

	elif [ $j -le 99 ] ; then
		ffmpeg -i $vpath"/clp000"$j".avi" $imgpath/image%d.png
		n=$(ls -l $imgpath | grep image | wc -l)
		let n--
		$exepath"/Motion2D" -m AC -p $imgpath/image%d.png -f 1 -i $n -r $vpath"/clp000"$j".txt" -v

	fi

	rm $imgpath/*.png
done

for j in {1..45}
do
	if [ $j -le 9 ] ; then
		ffmpeg -i $vpath"/clp0100"$j".avi" $imgpath/image%d.png
		n=$(ls -l $imgpath | grep image | wc -l)
		let n--
		$exepath"/Motion2D" -m AC -p $imgpath/image%d.png -f 1 -i $n -r $vpath"/clp0100"$j".txt" -v

	elif [ $j -le 99 ] ; then
		ffmpeg -i $vpath"/clp010"$j".avi" $imgpath/image%d.png
		n=$(ls -l $imgpath | grep image | wc -l)
		let n--
		$exepath"/Motion2D" -m AC -p $imgpath/image%d.png -f 1 -i $n -r $vpath"/clp010"$j".txt" -v

	fi
	rm $imgpath/*.png
done

for j in {1..14}
do
	if [ $j -le 9 ] ; then
		ffmpeg -i $vpath"/clp0200"$j".avi" $imgpath/image%d.png
		n=$(ls -l $imgpath | grep image | wc -l)
		let n--
		$exepath"/Motion2D" -m AC -p $imgpath/image%d.png -f 1 -i $n -r $vpath"/clp0200"$j".txt" -v

	elif [ $j -le 99 ] ; then
		ffmpeg -i $vpath"/clp020"$j".avi" $imgpath/image%d.png
		n=$(ls -l $imgpath | grep image | wc -l)
		let n--
		$exepath"/Motion2D" -m AC -p $imgpath/image%d.png -f 1 -i $n -r $vpath"/clp020"$j".txt" -v

	fi

	rm $imgpath/*.png
done

for j in {1..28}
do
	if [ $j -le 9 ] ; then
		ffmpeg -i $vpath"/clp0300"$j".avi" $imgpath/image%d.png
		n=$(ls -l $imgpath | grep image | wc -l)
		let n--
		$exepath"/Motion2D" -m AC -p $imgpath/image%d.png -f 1 -i $n -r $vpath"/clp0300"$j".txt" -v

	elif [ $j -le 99 ] ; then
		ffmpeg -i $vpath"/clp030"$j".avi" $imgpath/image%d.png
		n=$(ls -l $imgpath | grep image | wc -l)
		let n--
		$exepath"/Motion2D" -m AC -p $imgpath/image%d.png -f 1 -i $n -r $vpath"/clp030"$j".txt" -v

	fi

	rm $imgpath/*.png
done

for j in {1..25}
do
	if [ $j -le 9 ] ; then
		ffmpeg -i $vpath"/clp0400"$j".avi" $imgpath/image%d.png
		n=$(ls -l $imgpath | grep image | wc -l)
		let n--
		$exepath"/Motion2D" -m AC -p $imgpath/image%d.png -f 1 -i $n -r $vpath"/clp0400"$j".txt" -v

	elif [ $j -le 99 ] ; then
		ffmpeg -i $vpath"/clp040"$j".avi" $imgpath/image%d.png
		n=$(ls -l $imgpath | grep image | wc -l)
		let n--
		$exepath"/Motion2D" -m AC -p $imgpath/image%d.png -f 1 -i $n -r $vpath"/clp040"$j".txt" -v

	fi

	rm $imgpath/*.png
done
'
