CUDA_INSTALL_PATH 	?= /usr/local/cuda
CUDA_SDK_INSTALL_PATH 	?= /home/anis/NVIDIA_GPU_Computing_SDK
#OPENCV_INSTALL_PATH 	?= /home/anis/OpenCV-2.3.1
BOOST_INSTALL_PATH 	?= /usr/local/boost_1_49_0

MOTION2D_HOME		= /home/anis/Documents/Motion2D-1.3.11
MOTION2D_DIR_INCLUDE	= $(MOTION2D_HOME)/include

CXX 	:= g++
CC 	:= gcc
LINK 	:= g++ -fPIC
NVCC  	:= $(CUDA_INSTALL_PATH)/bin/nvcc -ccbin /usr/bin

OS			= Linux
# Compiling
CONF_CXX_VERSION	= $(shell $(CXX) -dumpversion)
MOTION2D_DIR_LIB	= $(MOTION2D_HOME)/lib
MOTION2D_LIBPATH	= $(MOTION2D_DIR_LIB)/$(OS)/$(CONF_CXX_VERSION)

# Includes
INCLUDES = -I. -I./inc -I$(CUDA_INSTALL_PATH)/include -I$(CUDA_SDK_INSTALL_PATH)/shared/inc/ -I$(CUDA_SDK_INSTALL_PATH)/C/common/inc -I$(BOOST_INSTALL_PATH)/include -I ./src -I$(MOTION2D_DIR_INCLUDE)

# Common flags
COMMONFLAGS 	+= $(INCLUDES)
NVCCFLAGS 	+= $(COMMONFLAGS) -use_fast_math
CXXFLAGS 	+= $(COMMONFLAGS)
CFLAGS 		+= $(COMMONFLAGS)

LIB_CUDA := -L$(CUDA_INSTALL_PATH)/lib64 -L$(CUDA_SDK_INSTALL_PATH)/C/common/lib $(CUDA_SDK_INSTALL_PATH)/shared/lib/libshrutil_x86_64.a -L$(BOOST_INSTALL_PATH)/lib -lcudart -lcufft -lboost_thread -lboost_program_options -fopenmp `pkg-config opencv --cflags` `pkg-config opencv --libs` -lopencv_gpu -L$(MOTION2D_LIBPATH) $(MOTION2D_LIBPATH)/libMotion2D.a
 
SRC_FOLDER := ./src

OBJS = main04.cpp.o STA_Pathway.cu.o STA_Mask.cu.o STA_Gabor.cu.o STA_Transform.cu.o STA_Interact.cu.o STA_Reduce.cu.o STA_Normalize.cu.o DYN_Common.cu.o DYN_Gabor.cu.o DYN_GaussianRecursive.cu.o DYN_Gradients.cu.o DYN_MedianFilter.cu.o DYN_Modulation.cu.o DYN_MotionCompensation.cu.o DYN_MotionEstimation.cu.o DYN_Pathway.cu.o DYN_Retina.cu.o FAC_Pathway.cpp.o UTY_ImageTransform.cu.o  UTY_ConvolutionFFT2D.cu.o UTY_HelperProcs.cpp.o UTY_Debug.cpp.o

TARGET = saliency
LINKLINE = $(LINK) -o $(TARGET) $(OBJS) $(LIB_CUDA)

.SUFFIXES: .c .cpp .cu .o

%.c.o: $(SRC_FOLDER)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

%.cu.o: $(SRC_FOLDER)/%.cu
	$(NVCC) $(NVCCFLAGS) -c $< -o $@

%.cpp.o: $(SRC_FOLDER)/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(TARGET): $(OBJS) Makefile
	$(LINKLINE)


clean:
	rm *.o
	rm $(TARGET)

