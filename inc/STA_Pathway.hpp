
#ifndef _STA_PATHWAY_H
#define _STA_PATHWAY_H

// includes
#include "struct.hpp"
#include "error.hpp"

#include "STA_Mask.hpp"
#include "STA_Transform.hpp"
#include "STA_Gabor.hpp"
#include "STA_Interact.hpp"
#include "STA_Normalize.hpp"
//#include "UTY_Debug.hpp"
//#include "DYN_GaussianRecursive.hpp"
//#include "UTY_ImageTransform.hpp"

/**
* This namespace wraps the static pathway's functionality.
*/
namespace Static {

	/**
	* A class with functions to compute the static visual saliency map for the STVS model.
	*/
	class Pathway {

	private:

		/**
		* Source image size.
		*/
		siz_t im_size_;

		/**
		* Output image size.
		*/
		siz_t im_size_scaled_;

		/**
		* No. of pixels in destination image.
		*/
		unsigned int size_;

		/**
		* No. of pixels in destination image.
		*/
		unsigned int size_scaled_;

		/**
		* Initializes the static pathway of STVS model.
		*/
		void Init();
		
		/**
		* Cleans up the static pathway of STVS model.
		*/
		void Clean();

	public:

		// declarations
		float 
		*d_idata
		, *d_temp
		, *d_temp_scaled
		, *d_odata
		, *d_maps;

		complex_t 
		*d_data_freq
		, *d_data_spat			
		, *d_maps_freq
		, *d_maps_spat;

		/**
		* Default contructor for Pathway class.
		* @param im_size is image size.
		* @param scale is scale factor for pathway.
		*/
		inline Pathway( siz_t const & im_size, float const & scale = 1.0f)
		: oMask( im_size)
		, oTransform( im_size)
		, oGabor( im_size)
		, oInteract( im_size)
		, oNormalize( im_size)
		{
			im_size_.w = im_size.w;
			im_size_.h = im_size.h;

			size_ = im_size_.w*im_size_.h;

			im_size_scaled_.w = (int)( im_size.w * scale);
			im_size_scaled_.h = (int)( im_size.h * scale);

			size_scaled_ = im_size_scaled_.w*im_size_scaled_.h;

			Init();
		}

		/**
		* Destructor for Pathway class.
		*/
		inline ~Pathway(){ Clean();}

		/**
		* Object of Reduce class used for retinal filtering operations.
		*/
		//Retina oRetina;

		/**
		* Object of Reduce class used for masking operations.
		*/
		Mask oMask;

		/**
		* Object of Reduce class used for fourier transformations.
		*/
		Transform oTransform;

		/**
		* Object of Reduce class used for gabor filtering operations.
		*/
		Gabor oGabor;

		/**
		* Object of Reduce class used for interaction operations.
		*/
		Interact oInteract;

		/**
		* Object of Reduce class used for normalizations.
		*/
		Normalize oNormalize;

		//		Dynamic::GaussianRecursive oGaussianRecursive;
		//		Utility::ImageTransform oImageTransform;

		/**
		* Computes static saliency map.
		* The function computes static saliency map for STVS model. 
		* It takes an input video frames or image, and computes the salience map as:
		* Step 1: Apply the retinal filtering.
		* Step 2: Apply the mask.
		* Step 3: Move to frequency domain.
		* Step 4: Apply the Gabor filtering.
		* Step 5: Move back to spatial domain.
		* Step 6: Perform the short interactions.
		* Step 7: Perform the normalizations.
		* Step 8: Apply the mask.
		* @param out a destination image pointer.
		* @param in a source image pointer.
		* @param im_size is image size.
		* @return dynamic saliency map.
		*/
		void Apply(
		float *out
		, float *in
		, siz_t im_size_scaled
		, siz_t im_size
		);

		/**
		* Property to set the input data onto GPU memory. [float data version]
		* @param out a destination image pointer.
		* @param in a source image pointer.
		*/
		void SetData(
		float* out
		, float* in
		);

		/**
		* Property to get the output data from GPU memory.
		* @param out a destination image pointer.
		* @param in a source image pointer.
		*/
		void GetData( 
		float* out
		, float* in
		); 

		/**
		* Property to get the output data from GPU memory. [Complex data version]
		* @param out a destination image pointer.
		* @param in a source image pointer.
		*/
		void GetData( 
		complex_t* out
		, complex_t* in
		);

	}; // class Pathway

} // namespace Static

#endif // _STA_PATHWAY_H
