#ifndef _IMAGETRANSFORM_H
#define _IMAGETRANSFORM_H

// includes
#include "struct.hpp"

/**
* This namespace wraps the common functionality.
*/
namespace Utility {

	/**
	* A class with functions to apply the simple matrix transforms.
	*/
	class ImageTransform
	{

	private:

		/**
		* Source image size.
		*/
		siz_t im_size_in_;

		/**
		* Output image size.
		*/
		siz_t im_size_out_;

		/**
		* No. of pixels in destination image.
		*/
		unsigned int size_in_;

		/**
		* No. of pixels in destination image.
		*/
		unsigned int size_out_;

		/**
		* Initializes the static pathway of STVS model.
		*/
		void Init();
		
		/**
		* Cleans up the static pathway of STVS model.
		*/
		void Clean();

	public:

		float2 *f_points;
		float2 *d_points;

		cudaArray *d_input;

		/**
		* Default contructor for ImageTransform class
		*/
		inline ImageTransform(){}

		/**
		* Default contructor for ImageTransform class
		*/
		inline ImageTransform( siz_t const & im_size, float const & scale_in = 1.0f, float const & scale_out = 1.0f)
		{
			im_size_in_.w = (int)( im_size.w * scale_in);;
			im_size_in_.h = (int)( im_size.h * scale_in);;

			im_size_out_.w = (int)( im_size.w * scale_out);
			im_size_out_.h = (int)( im_size.h * scale_out);

			size_in_  = im_size_in_.w *im_size_in_.h;
			size_out_ = im_size_out_.w*im_size_out_.h;

			Init();
		}

		/**
		* Destructor for ImageTransform class
		*/
		inline ~ImageTransform(){ Clean(); }

		/**
		* Applies the motion compensation.
		* @param out a destination image pointer.
		* @param in a source image pointer.
		* @param im_size_out is destination image size.
		* @param im_size_in is source image size.
		* @returns rescaled image.
		*/
		void Resize(
		float *out
		, float *in
		, siz_t im_size_out
		, siz_t im_size_in										
		);

		/**
		* Applies the motion compensation.
		* @param out a destination image pointer.
		* @param in a source image pointer.		
		* @returns rescaled image.
		*/
		void Resize(
		float *out
		, float *in
		);

		/**
		* Applies the bilinear interpolation.
		* @param out a destination image pointer.
		* @param in a source image pointer.
		* @param points a vx and vy kernal pointer.
		* @param im_size is image size.		
		* @returns interpolated image.
		*/
		void BilinearInterpolation ( 
		float *out
		, float *in
		, float2 *points
		, siz_t im_size 
		);


	}; // class Transform

	/**
	* Device kernel applies the bilinear interpolation.
	* @param out a destination image pointer.	
	* @param points a vx and vy kernal pointer.
	* @param output_numel is destination image total pixels.
	* @param output_width is destination image width.
	* @returns interpolated image.
	*/
	__global__ void KernelBilinearInterpolation( 
	float *out
	, float2 *points		
	, int output_numel
	, int output_width
	);

} // namespace Utility

#endif
