var searchData=
[
  ['reduce',['Reduce',['../classStatic_1_1Reduce.html#aa7baeea2b2fbb2a9c8ac01b6a2526d47',1,'Static::Reduce']]],
  ['resize',['Resize',['../classUtility_1_1ImageTransform.html#ab93b5f62734fb01a3d2808426d815cd3',1,'Utility::ImageTransform::Resize(float *out, float *in, siz_t im_size_out, siz_t im_size_in)'],['../classUtility_1_1ImageTransform.html#ae79c0c4a032f84f67d7d8513923a32f0',1,'Utility::ImageTransform::Resize(float *out, float *in)']]],
  ['retina',['Retina',['../classDynamic_1_1Retina.html#a76386631d8c2c7952071aa359cb78e9c',1,'Dynamic::Retina']]],
  ['retinafilter',['RetinaFilter',['../classDynamic_1_1Retina.html#a9164db13044fc80ce0bf5e39b643168f',1,'Dynamic::Retina']]],
  ['rotate90',['Rotate90',['../classDynamic_1_1Retina.html#ac5f08cf202bf4129bf895ef137e911fb',1,'Dynamic::Retina']]]
];
