var searchData=
[
  ['gabor',['Gabor',['../classDynamic_1_1Gabor.html',1,'Dynamic']]],
  ['gabor',['Gabor',['../classStatic_1_1Gabor.html',1,'Static']]],
  ['gabor',['Gabor',['../classDynamic_1_1Gabor.html#a288be96754efef10f20eb7e16279a2c7',1,'Dynamic::Gabor::Gabor()'],['../classStatic_1_1Gabor.html#abcc7364d7c93a228b9a20311c382cc89',1,'Static::Gabor::Gabor()']]],
  ['gaussianfilter',['GaussianFilter',['../classDynamic_1_1Retina.html#a00f9fd24de0766e1462ae68e006c2671',1,'Dynamic::Retina']]],
  ['gaussianrecursive',['GaussianRecursive',['../classDynamic_1_1GaussianRecursive.html',1,'Dynamic']]],
  ['gaussianrecursive',['GaussianRecursive',['../classDynamic_1_1GaussianRecursive.html#a92443bb4652252d191cd1881a9b7d8a4',1,'Dynamic::GaussianRecursive']]],
  ['getdata',['GetData',['../classStatic_1_1Pathway.html#a2750330d5fe4eb741e2545c2bc21171e',1,'Static::Pathway::GetData(float *out, float *in)'],['../classStatic_1_1Pathway.html#a4d3ac5b9f3b9d6c9a6d952cf5b6954d5',1,'Static::Pathway::GetData(complex_t *out, complex_t *in)']]],
  ['getfacemap',['GetFaceMap',['../classFace_1_1Pathway.html#a75131ba5a927befe2a70ec5e172431f1',1,'Face::Pathway']]],
  ['gradients',['Gradients',['../classDynamic_1_1Gradients.html#a276e72d3e4b23aa1dc669db929d1d557',1,'Dynamic::Gradients']]],
  ['gradients',['Gradients',['../classDynamic_1_1Gradients.html',1,'Dynamic']]],
  ['gx',['gx',['../structpyramid.html#aadce4cffbd07ff71f0328594ad53d84a',1,'pyramid']]]
];
