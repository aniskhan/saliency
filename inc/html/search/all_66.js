var searchData=
[
  ['face',['Face',['../namespaceFace.html',1,'']]],
  ['fcomplex',['fComplex',['../structfComplex.html',1,'']]],
  ['fetchcompensationvariables',['FetchCompensationVariables',['../classDynamic_1_1MotionCompensation.html#a27e3858200455b931742a4b01fc7070f',1,'Dynamic::MotionCompensation']]],
  ['fft',['FFT',['../classStatic_1_1Transform.html#a42085c811acde5d66f2612ac026a86b7',1,'Static::Transform']]],
  ['filt',['filt',['../structlevel__t.html#a2aa2aeed755c6daae6977e4172a0100b',1,'level_t']]],
  ['frame',['frame',['../structlevel__t.html#a5127f9d53e3788718ef90721ce208065',1,'level_t']]],
  ['fusion',['Fusion',['../namespaceUtility.html#a21928102c4062c5ec362ea4d15afef7e',1,'Utility::Fusion(float *out, float *im_static, float *im_dynamic, siz_t im_size)'],['../namespaceUtility.html#a1e377aed0eb49710ecba4bf07c23bb03',1,'Utility::Fusion(float *out, float *im_static, float *im_dynamic, float *im_face, std::vector&lt; int &gt; weights, siz_t im_size)']]]
];
