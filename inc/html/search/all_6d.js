var searchData=
[
  ['mask',['Mask',['../classStatic_1_1Mask.html',1,'Static']]],
  ['mask',['Mask',['../classStatic_1_1Mask.html#a843dd48db4ea10b1138a8127201f72a1',1,'Static::Mask']]],
  ['maskkernel',['MaskKernel',['../namespaceStatic.html#ab489b45cbe0dfe049ea9aca90d048bcc',1,'Static::MaskKernel(float *out, float *in, siz_t im_size)'],['../namespaceStatic.html#a6043e63046b820049a9491fb62c0e66a',1,'Static::MaskKernel(complex_t *out, float *in, siz_t im_size)']]],
  ['max',['Max',['../namespaceUtility.html#afb692cef1bdbb426bc72c3eead39bc3a',1,'Utility']]],
  ['mean',['Mean',['../namespaceUtility.html#aa38990ecdb49a666e832ea5fd5c2d82b',1,'Utility::Mean(float *im, siz_t im_size)'],['../namespaceUtility.html#a0256507553cd2efe6829ff84c8f171b6',1,'Utility::Mean(std::vector&lt; int &gt; values)']]],
  ['medianfilter',['MedianFilter',['../classDynamic_1_1MedianFilter.html',1,'Dynamic']]],
  ['medianfilter',['MedianFilter',['../classDynamic_1_1MedianFilter.html#aba64b3feb5863364ea2cfa51ae916beb',1,'Dynamic::MedianFilter']]],
  ['min',['Min',['../namespaceUtility.html#ae4653fd1e84cf5ec599bccbcb2bfc4a8',1,'Utility']]],
  ['mod',['mod',['../structpyramid.html#a235f4fec73f423d4d0c7b9312feeab76',1,'pyramid']]],
  ['modulation',['Modulation',['../classDynamic_1_1Modulation.html#a082a4b09bbb0a8ed2740d67849e2e90d',1,'Dynamic::Modulation']]],
  ['modulation',['Modulation',['../classDynamic_1_1Modulation.html',1,'Dynamic']]],
  ['motioncompensation',['MotionCompensation',['../classDynamic_1_1MotionCompensation.html#ab09d3110cd52dbf58b48445aad6ed09e',1,'Dynamic::MotionCompensation']]],
  ['motioncompensation',['MotionCompensation',['../classDynamic_1_1MotionCompensation.html',1,'Dynamic']]],
  ['motionestimation',['MotionEstimation',['../classDynamic_1_1MotionEstimation.html#ac97f27444a02e1316194f7ad508e1418',1,'Dynamic::MotionEstimation']]],
  ['motionestimation',['MotionEstimation',['../classDynamic_1_1MotionEstimation.html',1,'Dynamic']]]
];
