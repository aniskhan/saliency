#ifndef CONVOLUTION_H
#define CONVOLUTION_H

#include "struct.hpp"

class UTY_Convolution
{
private:

	UTY_Convolution();
	~UTY_Convolution();

	static int calculateFFTsize( int dataSize);

public:

	static void UTY_convolve_2d( 
		float *out
		, float *in
		, float *kernel
		, siz_t im_size
		, siz_t kernel_size
		);

};

__global__ void padKernel( 
						  float *d_PaddedKernel, 
						  int fftW, 
						  int fftH, 
						  int kernelW, 
						  int kernelH, 
						  int kernelX, 
						  int kernelY
						);

__global__ void padData( 
						float *d_PaddedData, 
						int fftW, 
						int fftH, 
						int dataW, 
						int dataH, 
						int kernelW, 
						int kernelH, 
						int kernelX, 
						int kernelY
						);

__device__ void complexMulAndScale(
								   complex_t& a
								   , complex_t b
								   , float c
								   );

__global__ void modulateAndNormalize(
									 complex_t *fft_PaddedData
									 , complex_t *fft_PaddedKernel
									 , int dataN
									 );

__global__ void StripOutPadding(
								float *out
								, float *in								
								, siz_t im_size
								, siz_t fft_size
								);

#endif