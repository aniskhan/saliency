#ifndef MOTIONCOMPENSATION_H
#define MOTIONCOMPENSATION_H

// includes
#include "struct.hpp"
#include "UTY_ImageTransform.hpp"

#include<vector>
#include <iostream>

#include <CMotion2DEstimator.h>
#include <CMotion2DPyramid.h>
#include <CMotion2DModel.h>

/**
* This namespace wraps the dynamic pathway's functionality.
*/
namespace Dynamic {

	/**
	* A class with functions to apply the background motion compensation.
	*/
	class MotionCompensation
	{
	private:

        CMotion2DEstimator      estimator;      // Motion estimator
        CMotion2DModel  model;          // Parametric motion model
        CMotion2DPyramid        pyramid1;       // Pyramid on image 1
        CMotion2DPyramid        pyramid2;       // Pyramid on image 2

        unsigned char label;      // Value of the motion estimator support  
        std::string model_id;  // Parametric motion model ID to estimate
        bool var_light; // Lighting variation parameter estimation
        bool model_orig_fixed;// Indicates if an origin is fixed
        double model_row_orig;  // Motion model origin (line coordinate)
        double model_col_orig;  // Motion model origin (column coordinate)
        unsigned pyr_nlevels;       // Number of levels in a multi-resolution pyr
        unsigned pyr_stop_level;    // Pyramid level where the estimator stops      

        bool compute_covariance; // Flag to compute the covariance matrix
        unsigned nbsubsample;       // Number of subsampling to apply

        double multfactor;      // Multiplier factor for motion parameters

        bool useModelAsInitialization; // How is the estimated model init

	unsigned char *sup;

		/**
		* Source image size.
		*/
		siz_t im_size_;

		/**
		* No. of pixels in source image.
		*/
		unsigned int size_;

		float2 *d_points;

		void Compensate(  
		float2 *points
		, const std::vector<float> compensation_vars
		, siz_t im_size	
		);

		void Init();
		void Clean();

	public:

		/**
		* Default contructor for MotionCompensation class.
		* @param im_size is image size.
		*/
		inline MotionCompensation( siz_t const & im_size)
		: oImageTransform( im_size, 1.0f, 1.0f)
		{
			im_size_.w = im_size.w;
			im_size_.h = im_size.h;

			size_ = im_size_.w*im_size_.h;

			label = 234;      // Value of the motion estimator support  
			model_id = "AC";  // Parametric motion model ID to estimate
			var_light = false; // Lighting variation parameter estimation
			model_orig_fixed = false;// Indicates if an origin is fixed
			model_row_orig = -1.;  // Motion model origin (line coordinate)
			model_col_orig = -1.;  // Motion model origin (column coordinate)
			pyr_nlevels = 4;       // Number of levels in a multi-resolution pyr
			pyr_stop_level = 0;    // Pyramid level where the estimator stops      

			compute_covariance = false; // Flag to compute the covariance matrix
			nbsubsample = 0;       // Number of subsampling to apply

			multfactor = 1.0;      // Multiplier factor for motion parameters

			useModelAsInitialization = false; // How is the estimated model init

			Init();
		}

		/**
		* Destructor for MotionCompensation class
		*/
		inline ~MotionCompensation(){  Clean(); }

		/**
		* Object of ImageTransform class used for rescaling of input.
		*/
		Utility::ImageTransform oImageTransform;

		/**
		* Applies the motion compensation.
		* @param out a compensated image pointer.
		* @param in a source image pointer.
		* @param params a compensations variables pointer.
		* @param im_size is image size.
		* @return camera motion compensated image.
		*/
		void Apply(
		float *out
		, float *in
		, const std::vector<float> compensation_vars
		, siz_t im_size
		);

		/**
		* Fetches the background compensation variables.
		* @param out a compensation variables pointer
		* @param filename a compensation variables file name pointer.
		* @param frame_no is frame number.
		* @return background compensation for a video frame.
		*/

		void FetchCompensationVariables(
		float *out
		, const std::string filename
		, unsigned int frame_no
		);

		std::vector<float> FetchCompensationVariables(     
        const std::string filename
        , unsigned int frame_no
        );

		std::vector<float> FetchCompensationVariables( unsigned char* im1, unsigned char* im2);

	}; // class MotionCompensation

	/**
	* Device kernel to apply motion compensation.
	* @param points a stencil pointer.
	* @param params a compensations variables pointer.
	* @param im_size is image size.		
	* @param frame_no is frame number.
	* @return camera motion compensated image.
	*/

	__global__ void KernelCompensate(
	float2 *points
	, float *params
	, siz_t im_size
	, int frame_no 
	);

	/**
	* Device kernel to apply motion compensation.
	* @param points a stencil pointer.
	* @param compensation_vars a compensations variables pointer.
	* @param im_size is image size.		
	* @return camera motion compensated image.
	*/
	__global__ void KernelCompensate(
	float2 *points
	, siz_t im_size
	);

} // namespace Dynamic

#endif
