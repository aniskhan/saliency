#ifndef GABOR_H
#define GABOR_H

// includes
#include "DYN_Common.hpp"
#include "DYN_GaussianRecursive.hpp"
#include "DYN_Modulation.hpp"

/**
* This namespace wraps the dynamic pathway's functionality.
*/
namespace Dynamic {

	/**
	* A class with functions to apply the gaboring filtering.
	*/
	class Gabor
	{
	private:

		/**
		* Object of Common class used for level transform.		
		*/
		Dynamic::Common oCommon;		

		/**
		* Object of Common class used for modulation/demodulation of input.		
		*/
		Dynamic::Modulation oModulation;

		/**
		* Object of GaussianRecursive class used for gabor filter.		
		*/
		Dynamic::GaussianRecursive oGaussianRecursive;

	public:

		/**
		* Default contructor for Gabor class.
		*/
		inline Gabor(){}

		/**
		* Destructor for Gabor class.
		*/
		inline ~Gabor(){}

		/**
		* Applies Gabor filter.
		* Gabor filters are linear filters used for edge detection. 
		* Due to their orientation and frequency representation, they work similar to mechanisms in human visuam system.
		* They are found to particularly useful for texture representation and and dicrimination.
		* @param out a gabor filter mask pointer.
		* @param in a source image pointer.
		* @param mod a modulation matrix pointer.
		* @param level_size a current level image size.
		* @param im_size an original image size.
		* @return gabor filter mask for specified pyramid level.
		*/
		void Apply(
		float *out
		, float *in
		, float *mod
		, siz_t level_size
		, siz_t im_size
		);
	}; // class Gabor

} // namespace Dynamic

#endif