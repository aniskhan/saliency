
#ifndef _NORMALIZE_H
#define _NORMALIZE_H

// includes
#include "struct.hpp"
#include "STA_Reduce.hpp"

/**
* This namespace wraps the static pathway's functionality.
*/
namespace Static {

	/**
	* A class with functions to apply different normalizations to the feature maps.
	*/
	class Normalize {

		/**
		* Source image size.
		*/
		siz_t im_size_;

		/**
		* No. of pixels in source image.
		*/
		unsigned int size_;

		/**
		* Reduction operator results (host).
		*/
		summary_stats_data *result;

		/**
		* Reduction operator results (device).
		*/
		summary_stats_data *d_result;

		/**
		* Object of Reduce class used for reduction operations.		
		*/		
		Static::Reduce oReduce;

		/**
		* Initializes the mask.		
		*/
		void Init();
		
		/**
		* Cleans up the mask.
		*/
		void Clean();

	public:

		/**
		* Default contructor for Normalize class.
		*/
		inline Normalize( siz_t const & im_size)
		{
			im_size_ = im_size;
			size_ = im_size.w*im_size.h;

			Init();
		}

		/**
		* Destructor for Normalize class.
		*/
		inline ~Normalize(){ Clean();}

		/**
		* Normalizes the feature maps and fuses them.
		* The function normalizes the feature maps using several methods before summing them up into the static saliency map.
		* Step 1: Normalization.
		* Step 2: Normalization Itti.
		* Step 3: Normalization and fusion.
		* @param out a destination image pointer.
		* @param in a Feature maps pointer.
		* @param im_size is image size.
		* @return static saliency map.
		*/
		void Apply( 
		float *out
		, float* in
		, siz_t im_size			
		);

	}; // class normalize

	/**
	* Device kernel to normalizes the feature maps.
	* The function normalizes the feature maps using:
	* \frac{\left ( x - \underset{x}{\operatorname{argmin}} \right )}{\underset{x}{\operatorname{argmax}} - \underset{x}{\operatorname{argmin}}}
	* @param maps a feature maps pointer.
	* @param result a statistics vector pointer of feature maps.	
	* @param im_size is image size.	
	* @return in normalized feature maps.
	*/
	__global__  void KernelNormalizeNL( 
	float* maps
	, summary_stats_data* result	
	, siz_t im_size	
	);

	/**
	* Device kernel to applies Itti's normalization to the feature maps.
	* The function normalizes the feature maps using the Itti's normalization:
	* \left ( \underset{x}{argmax}f(x)-\bar{x}  \right )^2 
	* @param maps a feature maps pointer.
	* @param result a statistics vector pointer of feature maps.	
	* @param im_size is image size.	
	* @return in normalized feature maps.
	*/
	__global__  void KernelNormalizeItti(
	float* maps
	, summary_stats_data* result
	, siz_t im_size	
	);

	/**
	* GPU kernel to normalize and fuse all the feature maps into static saliency map.
	* The function normalizes the feature maps by taking only 20% of the values above the threshold (maximum value of the individual feature map).
	* Then fuses the normalized maps through simple summation into a saliency map for the static pathway of the STVS model.
	* @param out a Destination image.
	* @param maps a feature maps pointer.
	* @param result a statistics vector pointer of feature maps.
	* @param im_size is image size.	
	* @return static saliency map.
	*/
	__global__  void KernelNormalizePCFusion(
	float* out
	, float* maps
	, summary_stats_data* result
	, siz_t im_size	
	);

} // namespace Static

#endif // _NORMALIZE_H