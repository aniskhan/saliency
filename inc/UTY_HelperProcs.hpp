#ifndef _UTY_HELPERPROCS
#define _UTY_HELPERPROCS

// includes
#include "struct.hpp"

#include <stdio.h> 
#include <vector> 

namespace Utility {

	/**
	* Computes the mean value.
	* @param im a source image pointer.
	* @param im_size is image size.
	* @return mean value.
	*/
	float Mean(
		float *im
		, siz_t im_size
		);

	/**
	* Computes the mean value.
	* @param im a source image pointer.
	* @param im_size is image size.
	* @return mean value.
	*/
	float Mean(
		std::vector<int> values
		);

	/**
	* Computes the standard deviation value.
	* @param im a source image pointer.
	* @param im_size is image size.
	* @return the standard deviation.
	*/
	float StandardDeviation(
		float *im
		, siz_t im_size
		);

	/**
	* Computes the skewness value.
	* @param im a source image pointer.
	* @param im_size is image size.
	* @param mu is mean value.
	* @param std is standard deviation.
	* @return skewness value.
	*/
	float Skewness(
		float *im
		, siz_t im_size
		, float mu
		, float std
		);

	/**
	* Finds the minimum value.
	* @param im a source image pointer.
	* @param im_size is image size.
	* @return minimum value.
	*/
	float Min(
		float *im
		, siz_t im_size
		);

	/**
	* Finds the maximum value.
	* @param im a source image pointer.
	* @param im_size is image size.
	* @return maximum value.
	*/
	float Max(
		float *im
		, siz_t im_size
		);

	/**
	* Performs the fusion of static and dynamic saliency maps.
	* @param out a destination image pointer.
	* @param im_static a static saliency map pointer.
	* @param im_dynamic a dynamic saliency map pointer.
	* @param im_size is image size.	
	* @return final saliency map for two pathways.
	*/
	void Fusion(
		float *out
		, float *im_static
		, float *im_dynamic
		, siz_t im_size				
		);

	/**
	* Performs the fusion of static, dynamic and face saliency maps.
	* @param out a destination image pointer.
	* @param im_static a static saliency map pointer.
	* @param im_dynamic a dynamic saliency map pointer.
	* @param im_face a face saliency map pointer.
	* @param im_size is image size.	
	* @return final saliency map for three pathways.
	*/
	void Fusion(
		float *out
		, float *im_static
		, float *im_dynamic
		, float *im_face
		, std::vector<int> weights
		, siz_t im_size				
		);

	void Fusion(
		float *out
		, float *im_static
		, float *im_dynamic
		, float *im_face
		, std::vector<int> weights
		, siz_t im_size
		, float &m01
		, float &m02
		, float &m03
		);

	/**
	* Applies bilinear interpolation to image.
	* @param out a destination image pointer.
	* @param in a source image pointer.
	* @param vx a horizental kernel pointer.
	* @param vy a vertical kernel pointer.
	* @param im_size is image size.
	* @return interpolated image.
	*/
	void BilinearInterpolation(
		float *out
		, float *in
		, float *vx
		, float *vy
		, siz_t im_size
		);

	/**
	* Write text file.
	* @param filename a destination text filename pointer.
	* @param im a source image pointer.
	* @param im_size is image size.
	* @return error code.
	*/
	int WriteFile(
		char* filename
		, float *im
		, siz_t im_size
		);

} // namespace Utility

#endif //_UTY_HELPERPROCS
