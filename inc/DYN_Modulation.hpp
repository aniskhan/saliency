#ifndef MODULATION_H
#define MODULATION_H

// includes
#include "struct.hpp"

/**
* An enum for modulation.
* To switch mode of modulation function to demodulation mode. 
*/
typedef enum ModulationType_t{
	CUMD_MO = 0x0, /**< enum value modulation. */ 
	CUMD_DEM = 0x1 /**< enum value demodulation. */ 
} ModulationType;

/**
* This namespace wraps the dynamic pathway's functionality.
*/
namespace Dynamic {

	/**
	* A class with functions to modulate/demodulate the incoming data.
	*/
	class Modulation
	{
	private:

	public:

		/**
		* Default contructor for Modulation class.
		*/
		inline Modulation(){}

		/**
		* Destructor for Modulation class.
		*/
		inline ~Modulation(){}

		/**
		* Initializes the modulation matrix.
		* @param mod a modulation matrix pointer.
		* @param im_size is image size.
		* @param freq is modulation frequency.
		* @return modulation matrix.
		*/
		void Init( 
		float *mod
		, siz_t im_size
		, float freq
		);

		/**
		* Applies image modulation/demodulation.
		* @param out an output image pointer.
		* @param in an input image pointer.
		* @param mod a modulation matrix pointer.
		* @param level_size is current pyramid level size.
		* @param im_size is original image size.
		* @param type is modulation type.
		* @return modulated or demodulated image.
		*/
		void Apply(
		float *out
		, float *in
		, float *mod
		, siz_t level_size
		, siz_t im_size
		, ModulationType_t type
		);

	}; // class Modulation
	
	/**
	* Device kerenel to initializes the modulation matrix.
	* Step 1: t = k(PI)/N
	* Step 2: x(t) = Cos( 2(PI)f( iCos(t)+jSin(t)))
	* Step 3: y(t) = Sin( 2(PI)f( iCos(t)+iSin(t)))
	* @param mod a modulation matrix pointer.
	* @param level_size is current pyramid level size.
	* @param freq is Modulation frequency.
	* @return Modulation matrix.
	*/ 
	__global__ void KernelInitModulation( 
	float *mod
	, siz_t im_size
	, float freq
	);

	/**
	* Device kernel applies image modulation.
	* y(t) = c(t)*I(t)
	* @param out an output image pointer.
	* @param in an input image pointer.
	* @param mod a modulation matrix pointer.
	* @param level_size is current pyramid level size.
	* @param im_size is original image size.
	* @return Modulated image
	*/ 
	__global__ void KernelModulation( 
	float *out
	, float *in
	, float *mod
	, siz_t level_size
	, siz_t im_size
	);

	/**
	* Device kernel applies image demodulation.
	* Step 1: d(r,i) = sqrt(dr^2+di^2)
	* Step 2: x(r,i) = ( dr*mr + di*mi)/d(r,i)
	* Step 3: y(r,i) = (-dr*mi + di*mr)/d(r,i)
	* @param out an output image pointer.
	* @param in an input image pointer.
	* @param mod a modulation matrix pointer.
	* @param level_size is current pyramid level size.
	* @param im_size is original image size
	* @return Demodulated image
	*/
	__global__ void KernelDemodulation(
	float *out
	, float *in
	, float *mod
	, siz_t level_size
	, siz_t im_size
	);

} // namespace Dynamic

#endif