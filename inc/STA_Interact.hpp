
#ifndef _INTERACT_H
#define _INTERACT_H

#include "struct.hpp"

/**
* This namespace wraps the static pathway's functionality.
*/
namespace Static {

	/**
	* A class with functions to compute the interactions among the feature maps.
	*/
	class Interact {

	private:

		/**
		* Source image size.
		*/
		siz_t im_size_;

		/**
		* No. of pixels in source image.
		*/
		unsigned int size_;

		/**
		* Initializes the interactions.
		*/
		void Init();
		/**
		* Cleans up the interactions.
		*/
		void Clean();

	public:

		/**
		* Default contructor for Interact class.
		*/
		inline Interact( siz_t const & im_size)
		{
			im_size_ = im_size;
			size_ = im_size.w*im_size.h;

			Init();
		}

		/**
		* Destructor for Interact class.
		*/
		inline ~Interact(){ Clean();}

		/**
		* Applies short interactions.
		* @param out an interacted Gabor filtered maps pointer. 
		* @param in a source image Gabor filtered maps pointer. 
		* @param im_size is source image size. 
		* @return interacted gabor filtered maps.
		*/
		void Apply(
		float* out
		, complex_t* in
		, siz_t im_size			
		);

	};

	/**
	* GPU kernel to apply the short interactions.
	* Stage 1: Convert and prefetch complex to real numbers.
	* Stage 2: Perform interactions.
	* @param out Interacted Gabor filtered maps. 
	* @param in Source image Gabor filtered maps. 
	* @param im_size Source image size. 
	* @return interacted Gabor filtered maps.
	*/
	__global__ void KernelInteractionShort( 
	float* out
	, complex_t* in
	, siz_t im_size		
	);

} // namespace Static

#endif // _INTERACT_H