
#ifndef _TRANSFORM_H
#define _TRANSFORM_H

// includes
#include "struct.hpp"
#include "cufft_common.hpp"

/**
* This namespace wraps the static pathway's functionality.
*/
namespace Static {

	/**
	* A class with functions to apply the fourier transformations.
	*/
	class Transform {

	private:

		cufftHandle plan;

		/**
		* Source image size.
		*/
		siz_t im_size_;

		/**
		* No. of pixels in source image.
		*/
		unsigned int size_;

		/**
		* Initializes the transform operation.
		*/
		void Init();
		
		/**
		* Cleans up the the transform operation.
		*/
		void Clean();

	public:

		/**
		* Default contructor for Transform class.
		*/
		inline Transform( siz_t const & im_size)
		{
			im_size_ = im_size;
			size_ = im_size.w*im_size.h;

			Init();
		}

		/**
		* Destructor for Transform class.
		*/
		inline ~Transform() { Clean(); }

		/**
		* Calls the cufft library tranforms for an image.
		* @param out a destination image pointer.
		* @param in a source image pointer.
		* @param im_size is image size.
		* @param direction is transform direction.
		* @return transformed image.
		*/
		void FFT( 
		complex_t* out
		, complex_t* in
		, siz_t im_size			
		, int direction
		);

		/**
		* Applies im tranforms on an image.
		* @param out a destination image pointer.
		* @param in a source image pointer.
		* @param im_size is image size.
		* @param direction is transform direction.
		* @return transformed image.
		*/
		void Apply( 
		complex_t* out
		, complex_t* in
		, siz_t im_size
		, int direction
		);

	}; // class transform

	/**
	* Device kernel performing the shift operation on image.
	* @param out a destination image pointer.
	* @param in a source image pointer.
	* @param im_size is image size.
	* @param center is center location.
	* @param is_width_odd is a flag.
	* @param is_height_odd is a flag.
	* @return shifted image.
	*/
	__global__ void KernelShift(
	complex_t* out
	, complex_t* in
	, siz_t im_size
	, point_t center
	, bool is_width_odd
	, bool is_height_odd		
	);

	/**
	* Device kernel performing the shift operation on maps.</summary>
	* @param out a destination image pointer.
	* @param in a source image pointer.
	* @param im_size is image size.
	* @param center is center location.
	* @param is_width_odd is a flag.
	* @param is_height_odd is a flag.
	* @return shifted image.
	*/
	__global__ void KernelShiftInverse(
	complex_t* out
	, complex_t* in
	, siz_t im_size
	, point_t center
	, bool is_width_odd
	, bool is_height_odd		
	);

} // namespace Static

#endif // _TRANSFORM_H
