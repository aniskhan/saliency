
#ifndef _GABOR_H
#define _GABOR_H

#include "struct.hpp"

/**
* This namespace wraps the static pathway's functionality.
*/
namespace Static {

	/**
	* A class with functions to compute the dynamic visual saliency map for the STVS model.
	*/
	class Gabor {

	private:

		/**
		* Source image size.
		*/
		siz_t im_size_;

		/**
		* No. of pixels in source image.
		*/
		unsigned int size_;

		/**
		* Initializes the 2D Gabor bank.
		* The function configures the 2D gabor filter bank for several orientations and frequencies.
		* Step 1: Initialize the orientations.
		* Step 2: Initialize the frequencies.
		* Step 3: Compute the gabor functions.
		* Step 4: Bind all the above to GPU memory.
		*/
		void Init();
		
		/**
		* Cleans up the 2D Gabor bank.
		*/
		void Clean();

		/**
		* gabor mask on host.
		*/
		float 
		*h_gaborMaskU
		, *h_gaborMaskV;

		/**
		* gabor mask on device.
		*/
		cudaArray 
		*cu_gaborU
		, *cu_gaborV;

		/**
		* gabor filter orientations.
		*/
		double h_teta[NO_OF_ORIENTS];

		/**
		* gabor filter frequencies.
		*/
		float h_frequencies[NO_OF_BANDS];
		
		/**
		* gabor filter sigmas.
		*/
		float h_sig_hor[NO_OF_BANDS];

	public:

		/**
		* Default contructor for Pathway class.
		*/
		inline Gabor( siz_t const & im_size)
		{
			im_size_ = im_size;

			size_ = im_size.w*im_size.h;

			Init();
		}

		/**
		* Destructor for Pathway class.
		*/
		inline ~Gabor(){ Clean();}

		/**
		* Generates equally spaced vector.
		* @param in a linear data vector pointer.
		* @param d1 is lower limit.
		* @param d2 is upper limit.
		* @param n are number of points.
		* @return equally spaced vector.
		*/
		void CreateLinear( 
		float *in
		, float d1
		, float d2
		, unsigned int n
		);

		/**
		* Computes gabor filter masks.
		* \f${x}' = xCos\theta + ySin\theta\f$
		* \f${y}' = -xSin\theta + yCos\theta\f$
		* @param u1 a rotation x' pointer.
		* @param v1 a rotation y' pointer.
		* @param teta is orientation.
		* @param im_size is image size.
		* @return rotations x and y.
		*/
		void CreateGaborMasks( 
		float *u1
		, float *v1			
		, siz_t im_size
		, double *teta
		);

		/**
		* Applies Gabor filter.
		* The function returns N gabor filtered results for the input image in different orientations and frequencies.
		* N: NO_OF_ORIENTATIONS * NO_OF_FREQUENCY_BANDS
		* \f$G(x,y) = exp \left \{ -\frac{(x'-f)^2}{2 \sigma^2} + \frac{y'^2}{2 \sigma^2} \right \}\f$
		* @param out a gabor maps pointer.
		* @param in a source image pointer.
		* @param im_size is source image size.
		* @return gabor filtered maps
		*/
		void Apply( 
		complex_t* out
		, complex_t *in
		, siz_t im_size			
		);

	};

	/**
	* GPU kernel applying Gabor filter.
	* @param out a gabor maps pointer.
	* @param in a source image.
	* @param im_size is source image size.	
	* @return gabor filtered maps
	*/
	__global__ void KernelGabor( 
	complex_t* out
	, complex_t* in
	, siz_t im_size		
	);

} // namespace Static

#endif // _GABOR_H