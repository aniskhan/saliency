
#ifndef _UTY_DEBUG
#define _UTY_DEBUG

#include "struct.hpp"

namespace Utility {

		void write_image( char *filename, float *data, siz_t im_size);

		void write_image_device( char *filename, float *data, siz_t im_size);
				
		void write_image_device_v1( char *filename, float *data, siz_t im_size);

		void write_image_device( char *filename, complex_t *data, siz_t im_size);

		void write_image_device( char *filename, cufftComplex *data, siz_t im_size);

		void save_txt( char *filename, float *data, siz_t im_size);

		void save_txt_device( char *filename, float *data, siz_t im_size);

} // namespace Utility

#endif //_UTY_DEBUG
