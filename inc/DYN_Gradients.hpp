#ifndef GRADIENTS_H
#define GRADIENTS_H

// includes
#include "struct.hpp"

/**
* This namespace wraps the dynamic pathway's functionality.
*/
namespace Dynamic {

	/**
	* A class with functions to compute the gradients for the gabor filter bank.
	*/
	class Gradients
	{
	public:

		/**
		* Default contructor for Gradients class.
		*/
		inline Gradients(){}

		/**
		* Destructor for Gradients class.
		*/
		inline ~Gradients(){}
		
		/**
		* Computes the X gradients.
		* @param gx a X gradient pointer.
		* @param curr a current image pointer.
		* @param level_size is image size.
		* @return X gradient.
		*/
		void CalculateGradientX(
		float *gx
		, float *curr
		, siz_t level_size
		);

		/**
		* Computes the Y gradients.
		* @param gy a Y gradient pointer.
		* @param curr a current image pointer.
		* @param level_size is image size.
		* @return Y gradient.
		*/
		void CalculateGradientY(
		float *gy
		, float *curr	
		, siz_t level_size
		);

		/**
		* Computes the gradient in temporal direction..
		* @param gt a temporal gradient pointer.
		* @param curr a current image pointer.
		* @param prev a previous image pointer.
		* @param level_size is image size.
		* @return temporal gradient.
		*/
		void CalculateGradientT(
		float *gt
		, float *curr
		, float *prev	
		, siz_t level_size
		);

		void CalculateGradientXYT(
		float *gx
		, float *gy
		, float *gt
		, float *curr
		, float *prev	
		, siz_t level_size
		);

	}; // class Gradients

	/**
	* Device kernel to computes the X, Y and temporal gradients.
	* @param gx a X gradient pointer.
	* @param gy a Y gradient pointer.
	* @param gt a temporal gradient pointer.
	* @param curr a current image pointer.
	* @param prev a previous image pointer.
	* @param level_size is image size.
	* @return X, Y and temporal gradients.
	*/
	__global__ void KernelGradientXYT( 
	float *gx
	, float *gy
	, float *gt
	, float *curr
	, float *prev
	, siz_t level_size
	);

	/**
	* Device kernel to computes the X gradient.
	* The function calculates the gradient in the X direction using the equation:
	* G(x) = I{-2, -1(-8), +1(+8), +2} / 12
	* @param gx a temporal gradient pointer.
	* @param curr a current image pointer.
	* @param level_size is image size.
	* @param n is filter in gabor bank.
	* @return X gradient.
	*/
	__global__ void KernelGradientX( 
	float *gx
	, float *curr
	, siz_t level_size
	, int n
	);

	/**
	* Device kernel to computes the Y gradient.
	* The function calculates the gradient in the Y direction using the equation:
	* G(y) = I{-2w, -1w(-8), +1w(+8), +2w} / 12	
	* @param gy a temporal gradient pointer.
	* @param curr a current image pointer.	
	* @param level_size is image size.
	* @param n is filter in gabor bank.
	* @return Y gradient.
	*/
	__global__ void KernelGradientY( 
	float *gy
	, float *curr
	, siz_t level_size
	, int n
	);

	/**
	* Device kernel to computes the temporal gradient.
	* The function calculates the temporal gradient using the equation:
	* G(t) = I(t) - I(t-1).
	* @param gt a temporal gradient pointer.
	* @param curr a current image pointer.
	* @param prev a previous image pointer.
	* @param level_size is image size.
	* @param n is filter in gabor bank.
	* @return temporal gradient.
	*/
	__global__ void KernelGradientT( 
	float *gt
	, float *curr
	, float *prev		
	, siz_t level_size
	, int n
	);

} // namespace Dynamic

#endif
