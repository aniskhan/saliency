#ifndef GAUSSIAN_RECURSIVE_H
#define GAUSSIAN_RECURSIVE_H

// includes
#include "struct.hpp"

/**
* This namespace wraps the dynamic pathway's functionality.
*/
namespace Dynamic {

	/**
	* A class with functions to apply a gaussian function recursively.
	*/
	class GaussianRecursive
	{
	public:

		/**
		* Default contructor for GaussianRecursive class.
		*/
		inline GaussianRecursive(){}

		/**
		* Destructor for GaussianRecursive class.
		*/
		inline ~GaussianRecursive(){}

		/**
		* Performs gaussian blur.
		* The function performs recursive gaussian blur on image using single intensity parameter.
		* @param out Destination image or multiple destination image pointer.
		* @param in source image or multiple source image pointer.
		* @param level_size is level image size.
		* @param order is number of images.
		* @param sigma is intensity factor.
		* @return Smooth image or multiple images.
		*/
		void Apply(			
			float *out
			, float *in
			, siz_t im_size			
			, int order
			, float sigma
			);

		void Apply( 
			float *out
			, float *in
			, float *temp
			, siz_t im_size                                                                         
			, int order
			, float sigma
			);


	}; // class GaussianRecursive

	/**
	* 
	* @param out
	* @param in
	* @param im_size
	* @param B
	* @param b0
	* @param b1
	* @param b2
	* @param b3
	* @return 
	*/
	__global__ void KernelGaussianRecursiveCausal ( 
		float *odat
		, float *idat
		, siz_t im_size		
		, float B
		, float b0
		, float b1
		, float b2
		, float b3
		);

	/**
	* 
	* @param out
	* @param in
	* @param im_size
	* @param B
	* @param b0
	* @param b1
	* @param b2
	* @param b3
	* @return 
	*/
	__global__ void KernelGaussianRecursiveAntiCausal ( 
		float *odat
		, float *idat
		, siz_t im_size	
		, float B
		, float b0
		, float b1
		, float b2
		, float b3
		);

} // namespace Dynamic

#endif
