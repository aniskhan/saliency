#ifndef _MASK_H
#define _MASK_H

// includes
#include "struct.hpp"

/**
* This namespace wraps the static pathway's functionality.
*/
namespace Static {

	/**
	* A class with functions to compute and apply the hanning mask to discard the borders.
	*/
	class Mask {

	private:
		/**
		* Source image size.
		*/
		siz_t im_size_;

		/**
		* No. of pixels in source image.
		*/
		unsigned int size_;

		/**
		* Hanning mask.
		*/
		float *h_mask;

		/**
		* Defines array variable to bind to gpu's texture memory.
		*/
		cudaArray *cu_mask;

		/**
		* Initializes the mask.
		*/
		void Init();
		
		/**
		* Cleans up the mask.
		*/
		void Clean();

	public:

		/**
		* Default contructor for Mask class.
		* @param im_size is image size.
		*/
		inline Mask( siz_t const & im_size)
		{ 
			im_size_ = im_size;
			size_ = im_size_.w*im_size_.h;

			Init();
		}

		/**
		* Destructor for Mask class.
		*/
		inline ~Mask(){ Clean();}

		/**
		* Computes the mask.
		* The function creates a mask similar to a hanning function to discard the unnecessary boundaries.
		* @param mask a output mask pointer.
		* @param im_size is mask size.
		* @param mu is sigma 
		* @return the mask.
		*/
		void CreateMask( 
		float *mask
		, siz_t im_size
		, int mu
		);

		/**
		* Applies the mask.
		* @param out a destination image pointer. 
		* @param in a source image pointer. 
		* @param im_size is image size. 		
		* @return the masked image.
		*/
		void Apply( 
		float *out
		, float *in
		, siz_t const & im_size			
		);

		/**
		* Applies the mask.
		* @param in a source image pointer. 		
		* @param out a destination image pointer. 
		* @param im_size is image size. 
		* @return the masked image.
		*/
		void Apply( 
		complex_t *out
		, float *in
		, siz_t const & im_size			
		);

	}; // class Mask		

	/**
	* Device kernel to apply the mask.
	* @param in a source image pointer. 		
	* @param out a destination image pointer. 
	* @param im_size is image size. 
	* @return the masked image.
	*/	
	__global__ void MaskKernel(
	float *out
	, float* in
	, siz_t im_size		
	);
	
	/**
	* Device kernel to apply the mask.
	* @param in a source image pointer. 		
	* @param out a destination image pointer. 
	* @param im_size is image size. 
	* @return the masked image.
	*/	
	__global__ void MaskKernel( 
	complex_t *out
	, float* in
	, siz_t im_size		
	);

} // namespace Static

#endif // _MASK_H