
/*
* Copyright 1993-2010 NVIDIA Corporation.  All rights reserved.
*
* Please refer to the NVIDIA end user license agreement (EULA) associated
* with this source code for terms and conditions that govern your use of
* this software. Any use, reproduction, disclosure, or distribution of
* this software and related documentation outside the terms of the EULA
* is strictly prohibited.
*
*/

#ifndef _CONVOLUTIONFFT2D_H_
#define _CONVOLUTIONFFT2D_H_

// includes
#include <cufft.h>
#include "struct.hpp"

/// <summary>
/// This namespace wraps the common utility functionality.
/// </summary>
namespace Utility {

	/// <summary>
	/// A class with functions to apply the 2D convolution.
	/// </summary>
	class ConvolutionFFT2D
	{
	private:

		cufftHandle fftPlan;

		float
			*d_PaddedData
			, *d_PaddedKernel;

		fComplex
			*d_DataSpectrum0
			, *d_KernelSpectrum0;

		siz_t 
			im_size
			, kernel_size
			, padded_size;

		int FFT_SIZE, SPECTRUM_SIZE;

		void Init();
		void Clean();

	public:

		/// <summary>
		/// Default contructor for Convolution2d class.
		/// </summary>
		inline ConvolutionFFT2D(siz_t im_size_, siz_t kernel_size_)
		{
			im_size = im_size_;
			kernel_size = kernel_size_;

			Init();
		}

		/// <summary>
		/// Destructor for Convolution2d class.
		/// </summary>
		inline ~ConvolutionFFT2D(){ Clean(); }

		int snapTransformSize(int dataSize);

		void Convolve( 
			float *d_odata
			, float *d_idata
			, float *d_kernel						 
			, siz_t im_size
			, siz_t kernel_size									
			, point_t kernel_center
			);

		void convolutionClampToBorderCPU(
			float *h_Result,
			float *h_Data,
			float *h_Kernel,
			int dataH,
			int dataW,
			int kernelH,
			int kernelW,
			int kernelY,
			int kernelX
			);

		void padKernel(
			float *d_PaddedKernel,
			float *d_Kernel,
			int fftH,
			int fftW,
			int kernelH,
			int kernelW,
			int kernelY,
			int kernelX
			);

		void padDataClampToBorder(
			float *d_PaddedData,
			float *d_Data,
			int fftH,
			int fftW,
			int dataH,
			int dataW,
			int kernelH,
			int kernelW,
			int kernelY,
			int kernelX
			);

		void modulateAndNormalize(
			fComplex *d_Dst,
			fComplex *d_Src,
			int fftH,
			int fftW,
			int padding
			);

		void spPostprocess2D(
			void *d_Dst,
			void *d_Src,
			uint DY,
			uint DX,
			uint padding,
			int dir
			);

		void spPreprocess2D(
			void *d_Dst,
			void *d_Src,
			uint DY,
			uint DX,
			uint padding,
			int dir
			);

		void spProcess2D(
			void *d_Data,
			void *d_Data0,
			void *d_Kernel0,
			uint DY,
			uint DX,
			int dir
			);

		void GaussianFilter( 
			float *kernel
			, siz_t kernel_size
			, float sigma
			);

	}; // class ConvolutionFFT2D

	__global__ void padKernel_kernel(
		float *d_Dst,
		float *d_Src,
		int fftH,
		int fftW,
		int kernelH,
		int kernelW,
		int kernelY,
		int kernelX
		);


	__global__ void padDataClampToBorder_kernel(
		float *d_Dst,
		float *d_Src,
		int fftH,
		int fftW,
		int dataH,
		int dataW,
		int kernelH,
		int kernelW,
		int kernelY,
		int kernelX
		);

	inline __device__ void mulAndScale(fComplex& a, const fComplex& b, const float& c);

	__global__ void modulateAndNormalize_kernel(
		fComplex *d_Dst,
		fComplex *d_Src,
		int dataSize,
		float c
		);

	inline __device__ void spPostprocessC2C(fComplex& D1, fComplex& D2, const fComplex& twiddle);

	inline __device__ void spPreprocessC2C(fComplex& D1, fComplex& D2, const fComplex& twiddle);

	inline __device__ void getTwiddle(fComplex& twiddle, float phase);

	inline __device__ uint mod(uint a, uint DA);

	inline __device__ void udivmod(uint& dividend, uint divisor, uint& rem);

	__global__ void spProcess2D_kernel(
		fComplex *d_Dst,
		fComplex *d_SrcA,
		fComplex *d_SrcB,
		uint DY,
		uint DX,
		uint threadCount,
		float phaseBase,
		float c
		);

	__global__ void StripOutPadding( 
		float *out
		, float *in
		, siz_t im_size
		, siz_t fft_size
		);

} // namespace Utility

#endif //_CONVOLUTIONFFT2D_H_
