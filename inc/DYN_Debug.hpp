
#ifndef _DEBUG_H
#define _DEBUG_H

#include "struct.hpp"

namespace Dynamic {

	class Debug
	{
	private:


	public:

		inline Debug(){}
		inline ~Debug(){}

		void write_image( char *filename, float *data, siz_t im_size);

		void write_image_device( char *filename, float *data, siz_t im_size);

		void write_image_device( char *filename, complex_t *data, siz_t im_size);

		void save_txt( char *filename, float *data, siz_t im_size);

		void save_txt_device( char *filename, float *data, siz_t im_size);
	};

} // namespace Dynamic

#endif