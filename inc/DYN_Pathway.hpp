#ifndef _DYN_PATHWAY_H
#define _DYN_PATHWAY_H

// includes
#include "struct.hpp"
#include "error.hpp"
#include "GPUDefines.h"

//#include "UTY_Transform.hpp"
#include "DYN_MotionCompensation.hpp"
#include "DYN_Retina.hpp"
#include "DYN_Gabor.hpp"
#include "DYN_Common.hpp"
#include "DYN_MedianFilter.hpp"
#include "DYN_GaussianRecursive.hpp"
#include "DYN_Gradients.hpp"
#include "DYN_Modulation.hpp"
#include "DYN_MotionEstimation.hpp"

/**
* This namespace wraps the dynamic pathway's functionality.
*/
namespace Dynamic {

	/**
	* A class with functions to compute the dynamic visual saliency map for the STVS model.
	*/
	class Pathway {

	private:

		/**
		* Source image size.
		*/
		siz_t im_size_;

		/**
		* Scaled image size.
		*/
		siz_t im_size_scaled_;

		/**
		* No. of pixels in source image.
		*/
		unsigned int size_;

		/**
		* No. of pixels in scaled image.
		*/
		unsigned int size_scaled_;

		/**
		* Maximum No. of input frames.
		*/
		unsigned int max_frames_;

		/**
		* Maximum No. of input frames.
		*/
		std::string filename_;

		/**
		* Image pyramid.
		*/
		pyramid p;

		/**
		* Previous frame.
		*/
		float *d_im_prev;

		/**
		* Current frame.
		*/
		float *d_im_curr;

		/**
		* Temporary frame.
		*/
		float *d_im_temp;

		/**
		* Previous frame scaled.
		*/
		float *d_im_prev_scaled;

		/**
		* Current frame scaled.
		*/
		float *d_im_curr_scaled;

		/**
		* Compensated frame.
		*/
		float *d_im_mcom;

		/**
		* Computed frame buffer.
		*/
		float *d_im_buffer;

		/**
		* Computed motion vectors.
		*/
		float *d_optflow;

		/**
		* Compensation parameters on host.
		*/
//		float *params;
		
		/**
		* Compensation parameters on device.
		*/
//		float *d_params;
		
		/**
		* Temporal queue id.
		*/ 
		int queue_id;
		
		/**
		* Temporal queue available/full flag.
		*/ 
		bool apply_temporal_filter;

		/**
		* Initializes the dynamic pathway variables.
		*/
		void Init();
		
		/**
		* Cleans up the dynamic pathway variables.
		*/
		void Clean();

	public:		

		/**
		* Default contructor for Pathway class.
		* @param im_size is image size.
		* @param scale is scale factor for pathway.
		* @param filename is compensation variables text file name pointer.
		* @param max_frames is maximum length of compensation variables pointer.
		*/ 
		inline Pathway( 
		siz_t const & im_size
		, float const & scale
		, std::string filename
		, int const & max_frames = 2000
		)
		: oMotionCompensation( im_size)
		, oRetina(im_size)
		, oImageTransform1( im_size, 1.0f, 0.5f)
		, oImageTransform2( im_size, 0.5f, 1.0f)
		{

			im_size_.w = im_size.w;
			im_size_.h = im_size.h;

			size_ = im_size_.w*im_size_.h;

			im_size_scaled_.w = (int)( im_size.w*scale);
			im_size_scaled_.h = (int)( im_size.h*scale);

			size_scaled_ = im_size_scaled_.w*im_size_scaled_.h;

			max_frames_ = max_frames;

			filename_ = filename;

			queue_id = 0;
			apply_temporal_filter = false;

			Init();
		}

		/**
		* Destructor for Pathway class.
		*/
		inline ~Pathway(){ Clean(); }

		/**
		* Object of Common class used for common dynamic pathway procedures.
		*/
		Common oCommon;
		
		/**
		* Object of ImageTransform class used for image rescaling.
		*/
		Utility::ImageTransform oImageTransform1, oImageTransform2;
		
		/**
		* Object of MotionCompensation class used for camera motion compensation.
		*/
		MotionCompensation oMotionCompensation;
		
		/**
		* Object of Retina class used for retina filter.
		*/
		Retina oRetina;
		
		/**
		* Object of Gabor class used for gabor filter.
		*/
		Gabor oGabor;
		
		/**
		* Object of Gradients class used for gradients.
		*/
		Gradients oGradients;
		
		/**
		* Object of Modulation class used for modulation.
		*/
		Modulation oModulation;
		
		/**
		* Object of MedianFilter class used for median filter.
		*/
		MedianFilter oMedianFilter;
		
		/**
		* Object of GaussianRecursive class used for recursive filter.
		*/
		GaussianRecursive oGaussianRecursive;
		
		/**
		* Object of MotionEstimation class used for estimation.
		*/
		MotionEstimation oMotionEstimation;

		/**
		* The function computes dynamic saliency map for STVS model. 
		* It takes two input video frames, computes the motion map as:
		* Step 1: Compute the image pyramids.
		* Step 2: Apply the retinal filtering.
		* Step 3: Apply the transform.
		* Step 4: Apply the gabor filtering.
		* Step 5: Compute the gradients.
		* Step 6: Compute the velocity vectors.
		* Step 7: Combine the velocity vectors.
		* @param out a destination image/frame pointer.
		* @param in a source image/frame pointer.
		* @param compensation_vars a compensations variables pointer.
		* @param im_size_scaled is scaled image/frame size.
		* @param im_size is image/frame size.
		* @return the dynamic saliency map.
		*/
		void Apply(
		float* out
		, float *in
		, const std::vector<float> compensation_vars
		, siz_t im_size_scaled
		, siz_t im_size
		, unsigned int idx
		);

	}; // class Pathway

} // namespace Dynamic

#endif //_DYN_PATHWAY_H
