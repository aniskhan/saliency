#ifndef RETINA_H
#define RETINA_H

// includes
#include <cufft.h>

#include "struct.hpp"

#include "DYN_GaussianRecursive.hpp"

/**
* This namespace wraps the dynamic pathway's functionality.
*/
namespace Dynamic {

	/**
	* A class with functions to apply the retinal filtering that gives more detail to the visual input.
	*/
	class Retina
	{
	private:

		/**
		* Source image size.
		*/
		siz_t im_size_;

		/**
		* Photoreceptor cells kernel size.
		*/
		siz_t kernel_photoreceptor_;

		/**
		* Photoreceptor cells device pointer.		*/
		float* d_photoreceptor;

		/**
		* Horizental cells device pointer.		*/
		float* d_horizental;

		/**
		* Photoreceptor cells host pointer.		*/
		float* h_photoreceptor;

		/**
		* Photoreceptor cells gaussian kernel device pointer.		*/
		float* d_kernel;

		/**
		* Photoreceptor cells support host pointer.		*/
		float* sup;

		/**
		* Object of GaussianRecursive class used for gaussian filtering.		*/
		Dynamic::GaussianRecursive oGaussianRecursive;

		/**
		* Initializes the retina model.
		*/
		void Init();

		/**
		* Cleans up the retina model.
		*/
		void Clean();

	public:

		/**
		* Default contructor for Retina class.
		* @param im_size is image size.
		*/
		inline Retina( siz_t const & im_size)//: oConvolutionFFT2D( im_size, siz_t(KERNEL_PHOTORECEPTOR, KERNEL_PHOTORECEPTOR))
		{
			im_size_.w = im_size.w;
			im_size_.h = im_size.h;

			kernel_photoreceptor_.w = kernel_photoreceptor_.h = KERNEL_PHOTORECEPTOR;

			Init();
		}

		/**
		* Destructor for Retina class.
		*/
		inline ~Retina(){ Clean(); }

		/**
		* Object of GaussianRecursive class used for gaussian filtering.
		*/
		//Utility::ConvolutionFFT2D oConvolutionFFT2D;

		/**
		* Computes gaussian function.
		* @param kernel a gaussian function pointer.
		* @param kernel_size is gaussian kernel size.
		* @param sigma is intensity.
		* @return gaussian kernel window.
		*/
		void GaussianFilter( 
		float *kernel
		, siz_t kernel_size
		, float sigma
		);

		/**
		* Image rotation.
		* The funtion rotates the input in by 90 degrees.
		* @param out a destination image pointer.
		* @param in a source image pointer.
		* @param im_size is image size.
		* @return rotated image
		*/
		void Rotate90(
		float *out
		, float *in 
		, siz_t im_size
		);

		/**
		* Applies low pass filtering.
		* The function applies a low-pass filter using a gaussian kernel that is convolved to input source in.
		* @param out a destination image pointer.
		* @param in a source image pointer.
		* @param kernel a convolution kernel pointer.
		* @param im_size is image size.
		* @param kernel_size is kernel kernel size.
		* @return result of Bipolar cells.
		*/
		/*
		void LowPassFilter( 
		float *out
		, float *in
		, float *kernel 
		, siz_t im_size
		, siz_t kernel_size
		);
*/

		/**
		* Applies Retina filtering.
		* The filtering results in increased detail in input to the retina.
		* Step 1: Photoreceptor cells.
		* Step 2: Horizental cells.
		* Step 3: Bipolar cells.
		* @param out a destination image pointer.
		* @param in a source image pointer.
		* @param im_size is image size.
		* @param beta is type of ganglion cells.
		* @return result of retinal filtering.
		*/
		void RetinaFilter( 
		float *out
		, float *in
		, siz_t im_size
		, int beta
		);

	}; // class Retina

	/**
	* Applies the interactions of Bipolar cells.
	* @param out a destination image pointer.
	* @param Y a source image pointer (Photoreceptor cells).
	* @param H a source image pointer (Horizental cells).
	* @param im_size is image size.
	* @param beta is type of ganglion cells.
	* @return result of Bipolar cells
	*/
	__global__ void Bipolar( 
	float *out
	, float *Y 
	, float *H
	, siz_t im_size
	, int beta
	);

} // namespace Dynamic

#endif
