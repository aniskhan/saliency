#ifndef COMMON_H
#define COMMON_H

#include "struct.hpp"
#include "DYN_GaussianRecursive.hpp"

/**
* This namespace wraps the dynamic pathway's functionality.
*/
namespace Dynamic {

	/**
	* A class with miscelleneous functions.
	*/
	class Common
	{
	private:

		/**
		* Object of GaussianRecursive class used for level transform.
		*/
		GaussianRecursive oGaussianRecursive;

	public:

		/**
		* Default contructor for Common class.
		*/
		inline Common(){}

		/**
		* Destructor for Common class.
		*/
		inline ~Common(){}

		/**
		*
		* @param frame
		* @param shift
		* @param temp
		* @param vx
		* @param vy
		* @param level_size
		*/
		void ApplyInterpolation ( 
		float *frame
		, float *shift
		, float *temp
		, float *vx
		, float *vy
		, siz_t level_size
		);

		/**
		*
		* @param shift
		* @param temp
		* @param level_size
		*/
		void ApplyCoefficient (
		float *shift
		, float *temp
		, siz_t level_size
		);

		/**
		*
		* @param out
		* @param vx
		* @param vy
		* @param thr
		* @param level_size
		*/
		void CombineMotionVectors(
		float *out
		, float *vx
		, float *vy
		, float *thr
		, siz_t level_size
		);

		/**
		*
		* @param curr
		* @param prev
		* @param level_size
		* @param seuil
		*/
		void ApplyThreshold( 
		float* curr
		, float *prev
		, siz_t level_size
		, float *threshold
		);

		/**
		* 
		* @param vx0
		* @param vy0
		* @param level_size_curr
		* @param vx1
		* @param vy1
		* @param level_size_prev
		* @param shift
		* @param temp
		*/
		void ApplyProjection ( 
		float *vx0
		, float *vy0
		, siz_t level_size_curr
		, float *vx1
		, float *vy1
		, siz_t level_size_prev
		, float *shift
		, float *temp
		);

		/**
		*
		* @param bank
		* @param mat
		* @param level_size
		*/
		void ApplyHighPass(
		float *bank
		, float *mat									
		, siz_t level_size
		);

		/**
		*
		* @param levels
		* @param frame_01
		* @param frame_02
		* @param im_size
		* @param temp
		*/
		void CalculatePyramids ( 
		level_t levels[K]
		, float *frame_01
		, float *frame_02
		, siz_t im_size
		, float *temp
		);

		/**
		*
		* @param frame
		* @param shift
		* @param vx0
		* @param vy0
		* @param level_size_curr
		* @param vx1
		* @param vy1
		* @param level_size_prev
		* @param temp
		*/
		void TransformLevel ( 
		float *frame
		, float *shift
		, float *vx0
		, float *vy0
		, siz_t level_size_curr
		, float *vx1
		, float *vy1
		, siz_t level_size_prev
		, float *temp
		);

		/**
		*
		* @param out
		* @param in
		* @param im_size
		*/
		void TemporalFilter ( 
		float *out
		, float *in
		, siz_t im_size
		);

	}; // class Common

	/**
	*
	* @param frame
	* @param shift
	* @param temp
	* @param vx
	* @param vy
	* @param level_size
	*/
	__global__ void KernelInterpolation ( 
	float *frame
	, float *shift
	, float *temp
	, float *vx
	, float *vy
	, siz_t level_size
	);

	/**
	*
	* @param out
	* @param in
	* @param level_size
	*/		
	__global__ void KernelCoefficient ( 
	float *out
	, float *in
	, siz_t level_size
	);

	/**
	*
	* @param out
	* @param vx
	* @param vy
	* @param threshold
	* @param level_size
	*/
	__global__ void KernelCombineVectors( 
	float *out
	, float *vx
	, float *vy
	, float *threshold
	, siz_t level_size
	);

	/**
	* 
	* @param curr
	* @param prev
	* @param level_size
	* @param threshold
	*/
	__global__ void KernelThreshold( 
	float *curr
	, float *prev
	, siz_t level_size
	, float *threshold
	);

	/**
	* 
	* @param vx0
	* @param vy0
	* @param level_size_curr
	* @param vx1
	* @param vy1
	* @param level_size_prev
	* @param shift
	* @param temp
	*/
	__global__ void KernelProjection ( 
	float *vx0
	, float *vy0
	, siz_t level_size_curr
	, float *vx1
	, float *vy1
	, siz_t level_size_prev
	, float *shift
	, float *temp
	);

	/**
	*
	* @param bank
	* @param mat
	* @param level_size
	*/
	__global__ void KernelHighPass ( 
	float *bank
	, float *mat
	, siz_t level_size
	);

	/**
	*
	* @param curr
	* @param prev
	* @param level_size_curr
	* @param level_size_prev
	* @return
	*/
	__global__ void KernelCalculateLevel ( 
	float *curr
	, float *prev
	, siz_t level_size_curr
	, siz_t level_size_prev	
	);

	/**
	*
	* @param out a motion map pointer.
	* @param in a input buffer pointer to previous motion maps.
	* @param im_size a motion map size. 
	* @return temporily median filtered motion map.
	*/
	__global__ void KernelTemporalFilter ( 
	float *out
	, float *in
	, siz_t im_size
	);

} // namespace Dynamic

#endif
