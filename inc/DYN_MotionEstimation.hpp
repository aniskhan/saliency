#ifndef MOTIONESTIMATION_H
#define MOTIONESTIMATION_H

// includes
#include "struct.hpp"

/**
* This namespace wraps the dynamic pathway's functionality.
*/ 
namespace Dynamic {

	/**
	* A class with functions to estimate the motion or velocity vectors.
	*/ 
	class MotionEstimation
	{
	private:

	public:

		/**
		/ Default contructor for MotionEstimation class.
		*/ 
		inline MotionEstimation(){}

		/**
		* Destructor for MotionEstimation class.
		*/ 
		inline ~MotionEstimation(){}

		/**
		* Estimates the velocity vectors.
		* @param vx a X velocity vector pointer.
		* @param vy a Y velocity vector pointer.
		* @param gx a X gradient pointer.
		* @param gy a Y gradient pointer.
		* @param gt a temporal gradient pointer.
		* @param level_size is current pyramid level size.
		* @param addtype is motion vector accumulation type.
		* @return estimatated velocity vectors.
		*/
		void Apply( 
		float *vx
		, float *vy
		, float *gx
		, float *gy
		, float *gt
		, siz_t level_size
		, bool is_top_level
		);

	}; // class MotionEstimation

	/**
	* GPU kernel estimates the velocity vectors.
	* @param vx a X velocity vector pointer.
	* @param vy a Y velocity vector pointer.
	* @param gx a X gradient pointer.
	* @param gy a Y gradient pointer.
	* @param gt a temporal gradient pointer.
	* @param level_size is current pyramid level size.
	* @param addtype is motion vector accumulation type.
	* @return estimatated velocity vectors.
	*/
	__global__ void KernelMotionEstimation ( 
	float *vx
	, float *vy
	, float *gx
	, float *gy
	, float *gt	
	, siz_t level_size
	, bool is_top_level
	);

} // namespace Dynamic

#endif