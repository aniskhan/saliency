#ifndef MEDIAN_FILTER_H
#define MEDIAN_FILTER_H

// includes
#include "struct.hpp"

/**
* This namespace wraps the dynamic pathway's functionality.
*/
namespace Dynamic {

	/**
	* A class with functions for median filtering.
	*/
	class MedianFilter
	{
	private:

	public:

		/**
		* Default contructor for MedianFilter class.
		*/
		inline MedianFilter(){}

		/**
		* Destructor for MedianFilter class.
		*/
		inline ~MedianFilter(){}

		/**
		* Applies the median filter.
		* @param out a destination image pointer.
		* @param in a source image pointer.
		* @param level_size is current pyramid level size.
		* @return the smoothed image.
		*/
		void Apply( 
		float *out
		, float *in
		, siz_t level_size	
		);

	}; // class MedianFilter

	/**
	* Device kernel applies the median filter.
	* @param out a destination image pointer.
	* @param in a source image pointer.
	* @param level_size is current pyramid level size.
	* @return the smoothed image.
	*/
	__global__ void KernelMedianFilter( 
	float *out
	, float *in
	, siz_t level_size	
	);

} // namespace Dynamic

#endif