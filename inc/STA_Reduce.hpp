
#ifndef _REDUCE_H
#define _REDUCE_H

// includes
#include <limits>
#include "struct.hpp"

// structure used to accumulate the moments and other 
// statistical properties encountered so far.
struct summary_stats_data
{    
	float sum;
	float min;
	float max;

	// initialize to the identity element
	void initialize()
	{
		sum = 0;
		min = std::numeric_limits<float>::max();
		max = std::numeric_limits<float>::min();
	}
};

/**
* This namespace wraps the static pathway's functionality.
*/
namespace Static {

	/**
	* A class with functions to compute the reduction operations.
	*/
	class Reduce {

	private:

	public:

		/**
		* Default contructor for Reduce class.
		*/
		inline Reduce(){}

		/**
		* Destructor for Reduce class.
		*/
		inline ~Reduce(){}

		/**
		* Compute feature map statistics.
		* @param data a feature map pointer.
		* @param im_size is feature map size.
		* @return statistics for feature map.
		*/
		summary_stats_data Apply( float *data, siz_t im_size);
	}; // class Normalize

} // namespace Static

#endif // _REDUCE_H
