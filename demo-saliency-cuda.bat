echo saliency-cuda --three-path -i %~dp0data\input -C %~dp0data\input --display-master --cascade-name %~dp0data\input\haarcascade_frontalface_alt2.xml --iext mpg --oext png -t 13 "arianebisons"

echo saliency-cuda --one-path-static -i %~dp0data\input --display-static --iext mpg --oext png -I 62 -N 5 "arianebisons"

echo saliency-cuda --one-path-dynamic -i %~dp0data\input --display-dynamic --iext mpg --oext png -I 3 -N 2 "arianebisons"

echo saliency-cuda --one-path-face -i %~dp0data\input --display-face --cascade-name %~dp0data\input\haarcascade_frontalface_alt2.xml --iext mpg --oext png -I 62 -N 5 -t 13 "arianebisons"

echo saliency-cuda --two-path -i %~dp0data\input --display-master --iext mpg --oext png -I 62 -N 5 "arianebisons"

echo saliency-cuda --three-path --display-master --fixed-camera --use-camera --cascade-name %~dp0data\input\haarcascade_frontalface_alt2.xml

echo saliency-cuda --one-path-static --display-static --fixed-camera --use-camera

saliency-cuda --one-path-dynamic --display-dynamic --fixed-camera --use-camera

echo saliency-cuda --one-path-face --display-face --cascade-name %~dp0data\input\haarcascade_frontalface_alt2.xml --fixed-camera --use-camera

echo saliency-cuda --two-path --display-master --fixed-camera --use-camera

echo saliency-cuda --three-path --display-master --cascade-name %~dp0data\input\haarcascade_frontalface_alt2.xml --fixed-camera --use-camera