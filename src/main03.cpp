/// <summary>
/// change 11:33
/// Four threads for a get and three compute operations.
/// Fusion thread is the master, invokes the get frame thread
/// Then invokes the other two compute threads, and waits for them to finish
/// Finally performs the fusion into final saliency map
/// </summary>
///**********************************************************************************
///********FLAGS(is_completed,is_input_ready,is_static_ready,is_dynamic_ready)*******
///**********************************************************************************
///************************┌▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┐************************************
///************************|	compute_fusion	|************************************
///************************|		(0111)		|************************************
///************************└▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┘************************************
///************************▲║********▲║********║▲************************************
///************************║║*(0100)*║║*(0100)*║║************************************
///*****************(0110)*║║*(1000)*║║********║║*(0101)*****************************
///************************║▼********║║********▼║************************************
///************┌▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┐***║║***┌▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┐************************
///************| compute_static  |***║║***| compute_dynamic |************************
///************|	  (0010)	 |***║║***|		(0001)	    |************************
///************└▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┘***║║***└▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┘************************
///**********************************║║**********************************************
///**********************************║║*(0000)***************************************
///***************************(0100)*║║*(1000)***************************************
///**********************************║▼**********************************************
///************************┌▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┐**************************************
///************************|	get_frame	  |**************************************
///************************|	  (0100)	  |**************************************
///************************└▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┘**************************************
///**********************************************************************************

//#define _CRTDBG_MAP_ALLOC

// Utilities and system includes
//#include <boost/thread.hpp>
#include <pthread.h>
#include <ctime>
//#include <cstdlib>
#include <stdlib.h>
//#include <crtdbg.h>
#include <assert.h>
#include <fstream>
#include <cmath>
//#include <vld.h>

#include <shrUtils.h>
#include <shrQATest.h>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "cutil_inline.h"

#include "inc/error.hpp"

#include "inc/STA_Pathway.hpp"
#include "inc/DYN_Pathway.hpp"

#include "inc/UTY_HelperProcs.hpp"
#include "inc/UTY_Visualize.hpp"

//#define VERBOSE

//#define __STATIC
#define __DYNAMIC

static char *sSDKsample		= "stvs";

static int init_frame		= 0;
static int init_snippet		= 3;
static int no_of_snippets	= 3;
static int queue_len		= 13;
static int max_images		= 10;

static float scale			= 0.5f;

static char *clipname	= 0;
static char *iformat	= 0;
static char *oformat	= 0;
static char *idir		= 0;
static char *odir		= 0;
static char *odir_s		= 0;
static char *odir_st	= 0;
static char *odir_d		= 0;
static char *odir_dt	= 0;
static char *odir_sd	= 0;
static char *mc_dir		= 0;	
static char *mc_exe		= 0;
static char *iext		= 0;
static char *oext		= 0;

static char iformat_str[1024];
static char oformat_str[1024];
static char command[1024];

void temporal_filtering( float *out, float** in, siz_t scaled_im_size);

void save_txt(char *filename, float *data, siz_t in_size_cropped)
{ 
	// writing on a text file
	std::ofstream myfile (filename);
	if (myfile.is_open())
	{
		for( int i=0;i<in_size_cropped.w*in_size_cropped.h;++i) {
			myfile<<data[i]<<"\t";
		}
		myfile.close();
	}
	else std::cout << "Unable to open file";
}

bool is_completed = false;

bool is_snippet_completed = false;			

// input buffer state
bool is_input_ready = true;

// static buffer state
bool is_static_ready = true;

// static buffer state
bool is_dynamic_ready = true;

// mutex to synchronize access to buffer
//boost::mutex g_mutex ;

void GetFrameThread();
void ComputeStaticThread();
void ComputeDynamicThread();
void ComputeFusionThread();

/// <summary>
/// Variables declarations.
/// </summary>
float *h_idata
, *h_odata_s, *h_odata_d;

static int in_nbpixels, out_nbpixels
,in_nbpixels_cropped, out_nbpixels_cropped;

static siz_t in_size(720,576), out_size
, in_size_cropped, out_size_cropped;

static int snippet_idx;
static int frame_idx;

static int queue_idx;
static bool queue_full;

static unsigned int in_offset_w = 0;
static unsigned int in_offset_h = 0;

static unsigned int out_offset_w = 0;
static unsigned int out_offset_h = 0;

/// <summary>
/// Main program entry point.
/// <param name="argc">Argument count.</param>
/// <param name="argv">Arguments.</param>
/// <returns>TODO:: Returns status of program.</returns>
/// </summary>
int main(int argc, char** argv)
{
	shrQAStart(argc, argv);
	printf("[ %s ]\n", sSDKsample);

	shrSetLogFileName ("stvs.txt");
	shrLog("%s Starting (STVS tests)...\n\n", argv[0]);

	/// <summary>
	/// Read the program input arguments.
	/// </summary>
	shrGetCmdLineArgumenti(argc, (const char**)argv, "nsnippets", &no_of_snippets);
	shrLog("No. of snippets = %d\n", no_of_snippets);

	shrGetCmdLineArgumenti(argc, (const char**)argv, "queuelen", &queue_len);
	shrLog("Queue length = %d\n", queue_len);

	shrGetCmdLineArgumenti(argc, (const char**)argv, "maximages", &max_images);
	shrLog("Max limit of images = %d\n", max_images);

	shrGetCmdLineArgumentf(argc, (const char**)argv, "scale", &scale);
	shrLog("Scale = %f\n", scale);

	shrGetCmdLineArgumentstr(argc, (const char**)argv, "in", &idir);
	shrLog("Image directory [ %s ]\n", idir);
	if( idir==NULL) idir = "C:/Sophie_these/Program_Marat_21-01-10/saillance/data/original/bmp/";

	shrGetCmdLineArgumentstr(argc, (const char**)argv, "out", &odir);
	shrLog("Output image directory [ %s ]\n", odir);
	if( odir==NULL) odir = "D:/tmp/";

	shrGetCmdLineArgumentstr(argc, (const char**)argv, "mcexe", &mc_exe);
	shrLog("Background compensation directory [ %s ]\n", mc_exe);	
	if( mc_exe==NULL) mc_exe = "C:/CUDA/C/bin/Win32/Debug/Motion2D/Motion2D";

	shrGetCmdLineArgumentstr(argc, (const char**)argv, "mcdir", &mc_dir);
	shrLog("Background compensation files [ %s ]\n", mc_dir);	
	if( mc_dir==NULL) mc_dir = odir;

	shrGetCmdLineArgumentstr(argc, (const char**)argv, "static", &odir_s);
	shrLog("Static images directory [ %s ]\n", odir_s);
	if( odir_s==NULL) odir_s = "D:/tmp/static/";

	shrGetCmdLineArgumentstr(argc, (const char**)argv, "statictxt", &odir_st);
	shrLog("Dynamic text directory [ %s ]\n", odir_st);
	if( odir_st==NULL) odir_st = "D:/tmp/dynamic/";

	shrGetCmdLineArgumentstr(argc, (const char**)argv, "dynamic", &odir_d);
	shrLog("Dynamic images directory [ %s ]\n", odir_d);
	if( odir_d==NULL) odir_d = "D:/tmp/dynamic/";

	shrGetCmdLineArgumentstr(argc, (const char**)argv, "dynamictxt", &odir_dt);
	shrLog("Dynamic text directory [ %s ]\n", odir_dt);
	if( odir_dt==NULL) odir_dt = "D:/tmp/dynamic/";

	shrGetCmdLineArgumentstr(argc, (const char**)argv, "fusion", &odir_sd);
	shrLog("Fusion images directory [ %s ]\n", odir_sd);
	if( odir_sd==NULL) odir_sd = "D:/tmp/fusion/";

	shrGetCmdLineArgumentstr(argc, (const char**)argv, "clipname", &clipname);
	shrLog("Clip name [ %s ]\n", clipname);
	if( clipname==NULL) clipname = "ClipRMX_";

	shrGetCmdLineArgumentstr(argc, (const char**)argv, "iformat", &iformat);
	shrLog("Input format [ %s ]\n", iformat);
	if( iformat==NULL) iformat = "%d_%04d";

	shrGetCmdLineArgumentstr(argc, (const char**)argv, "oformat", &oformat);
	shrLog("Output format [ %s ]\n", oformat);
	if( oformat==NULL) oformat = "%d_%d";

	shrGetCmdLineArgumentstr(argc, (const char**)argv, "iext", &iext);
	shrLog("Image extension [ %s ]\n", iext);
	if( iext==NULL) iext = "bmp";

	shrGetCmdLineArgumentstr(argc, (const char**)argv, "oext", &oext);
	shrLog("Image extension [ %s ]\n", oext);
	if( oext==NULL) oext = "png";

	shrGetCmdLineArgumenti(argc, (const char**)argv, "width", &in_size.w);
	shrLog("Width = %d\n", in_size.w);

	shrGetCmdLineArgumenti(argc, (const char**)argv, "height", &in_size.h);
	shrLog("Height = %d\n", in_size.h);

	shrGetCmdLineArgumenti(argc, (const char**)argv, "initsnippet", &init_snippet);
	shrLog("Init snippet = %d\n", init_snippet);

	shrGetCmdLineArgumenti(argc, (const char**)argv, "initframe", &init_frame);
	shrLog("Init frame = %d\n", init_frame);

	/// <summary>
	/// Set the format string to read the images.
	/// </summary>
	sprintf( iformat_str, "%s%s%s%s","%s", clipname, iformat, "%s%s");
	sprintf( oformat_str, "%s%s%s%s","%s", clipname, oformat, "%s%s");

	/// <summary>
	/// Input images are rescaled, if the size dimensions are not a multiple of 16.
	/// The reason is multiple of 16 being the ideal size of data access and processing on GPUs.
	/// Also, cufft library requires the data to be a multiple of 16.
	/// </summary>
	{
		out_size.w = in_size.w / 2;
		out_size.h = in_size.h / 2;

		in_size_cropped.w = (int) ( floor(in_size.w/16.0f) * 16.0f);
		in_size_cropped.h = (int) ( floor(in_size.h/16.0f) * 16.0f);

		in_offset_w = (in_size.w - in_size_cropped.w) / 2;
		in_offset_h = (in_size.h - in_size_cropped.h) / 2;

		out_size_cropped.w = ( int)( in_size_cropped.w * scale);
		out_size_cropped.h = ( int)( in_size_cropped.h * scale);

		out_offset_w = (int)( ( out_size.w - out_size_cropped.w) / 2);
		out_offset_h = (int)( ( out_size.h - out_size_cropped.h) / 2);

		in_nbpixels  =  in_size.w *  in_size.h;
		out_nbpixels = out_size.w * out_size.h;

		in_nbpixels_cropped  =  in_size_cropped.w *  in_size_cropped.h;
		out_nbpixels_cropped = out_size_cropped.w * out_size_cropped.h;
	}

	h_idata	  = ( float *)malloc( in_nbpixels_cropped * sizeof( float));
	h_odata_s = ( float *)malloc( out_nbpixels_cropped * sizeof( float));
	h_odata_d = ( float *)malloc( out_nbpixels_cropped * sizeof( float));

	pthread_t get_frame_thread, compute_static_thread, compute_dynamic_thread, compute_fusion_thread;
	int iret1, iret2, iret3, iret4;
	char *message1 = "Get frame thread";
	char *message2 = "Compute static thread";
	char *message3 = "Compute dynamic thread";
	char *message4 = "Compute fusion thread";

	// create a get input frame thread
	//boost::thread get_frame_thread( &GetFrameThread ) ;
	iret1 = pthread_create( &thread get_frame_thread, NULL, GetFrameThread, (void*) message1);

	#ifdef __STATIC
	// start static pass thread
	//boost::thread compute_static_thread( &ComputeStaticThread ) ;
	iret2 = pthread_create( &compute_static_thread, NULL, ComputeStaticThread, (void*) message2);
	
#endif

#ifdef __DYNAMIC
	// start dynamic pass thread
	//boost::thread compute_dynamic_thread( &ComputeDynamicThread ) ;
	iret3 = pthread_create( &compute_dynamic_thread, NULL, ComputeDynamicThread, (void*) message3);
#endif

	// start fusion thread
	//boost::thread compute_fusion_thread( &ComputeFusionThread ) ;
	iret4 = pthread_create( &compute_fusion_thread, NULL, ComputeFusionThread, (void*) message4);
	
	// wait for timer_thread to finish
	//get_frame_thread.join();
	pthread_join( get_frame_thread, NULL);

#ifdef __STATIC
	//compute_static_thread.join();
	pthread_join( compute_static_thread, NULL);
#endif

#ifdef __DYNAMIC
	//compute_dynamic_thread.join();
	pthread_join( compute_dynamic_thread, NULL);
#endif

	//compute_fusion_thread.join();
	pthread_join( compute_fusion_thread, NULL);

	shrLog("calculation of saliency maps completed.\n");

	printf("Thread 1 returns: %d\n",iret1);
	printf("Thread 2 returns: %d\n",iret2);
	printf("Thread 3 returns: %d\n",iret3);
	printf("Thread 4 returns: %d\n",iret4);

//	_CrtDumpMemoryLeaks();

	free( h_idata);
	free( h_odata_s);
	free( h_odata_d);	

	cutilExit( argc, argv);
}

void GetFrameThread() // get input frame
{
	char filename[1024];

	cv::Mat im = cv::Mat( in_size.h, in_size.w, CV_8UC1 );

	while(1){

		while( is_input_ready && !is_completed);

		if( is_completed) break;

#ifdef VERBOSE
		shrLog("getting input [%d %d] ...\n", snippet_idx, frame_idx);		
#endif

		std::cout << "[" << snippet_idx << " " << frame_idx << "]" << "\n";

		sprintf( filename, iformat_str, idir, snippet_idx, frame_idx, ".", iext);

		im = cv::imread( filename, CV_LOAD_IMAGE_GRAYSCALE);

		if (!im.empty()){

			for( int j=0;j<in_size_cropped.h;++j){
				for( int i=0;i<in_size_cropped.w;++i){
					h_idata[i + j*in_size_cropped.w] =  im.data[(i+in_offset_w) + (j+in_offset_h)*in_size.w];					
				}
			}

#ifdef VERBOSE
			shrLog("input ready [%d %d].\n", snippet_idx, frame_idx);
#endif

			// get exclusive ownership of mutex (wait for light to tuen green)
			boost::mutex::scoped_lock lock_it( g_mutex ) ;
			// ok, now we have exclusive access to the light

			is_input_ready = true;

			// destructor for lock_it will release the mutex
		}			
		else{
			std::cout << "snippet ended." << std::endl ;

			// get exclusive ownership of mutex (wait for light to tuen green)
			boost::mutex::scoped_lock lock_it( g_mutex ) ;
			// ok, now we have exclusive access to the light

			is_snippet_completed = true;
			is_input_ready = true;
		}
	}

	im.release();
}


void ComputeStaticThread() // get input frame
{
	cudaSetDevice(1);	

	/// <summary>
	/// Get device properties.
	/// </summary>
	int dev_id;
	cudaDeviceProp dev_props;

	// get number of SMs on this GPU
	cutilSafeCall(cudaGetDevice(&dev_id));
	cutilSafeCall(cudaGetDeviceProperties(&dev_props, dev_id));

	shrLog("device %d: \"%s\" with Compute %d.%d capability\n", dev_id, dev_props.name, dev_props.major, dev_props.minor);	

	shrLog("starting static saliency thread ...\n");

	float mx;

	char filename[1024];

	cv::Mat im_s = cv::Mat( out_size.h, out_size.w, CV_8UC1 );
	std::vector<int> params;

	params.push_back( CV_IMWRITE_PXM_BINARY);
	params.push_back( 100);

	for( int i=0;i<out_nbpixels;++i){		
		im_s.data[i] = ( char)( unsigned char)( 0);
	}

	float *temp = (float*) calloc(sizeof(float), out_nbpixels);

	Static::Pathway oStaticPathway( in_size_cropped, 0.5f);

	while(1){

		while( (!is_input_ready || is_static_ready) && !is_completed);

		if( is_completed) break;

#ifdef VERBOSE
		shrLog("computing static [%d %d] ...\n", snippet_idx, frame_idx);
#endif

		oStaticPathway.Apply( h_odata_s, h_idata, out_size_cropped, in_size_cropped);

#ifdef VERBOSE
		shrLog("writing static [%d %d] ...\n", snippet_idx, frame_idx);
#endif

		mx = h_odata_s[0];
		for( int i=0;i<out_nbpixels_cropped;++i) 
		{
			if (h_odata_s[i]>mx)
			{
				mx = h_odata_s[i];
			}
		}

		for( int j=0;j<out_size_cropped.h;++j){
			for( int i=0;i<out_size_cropped.w;++i){

				temp	 [(i+out_offset_w) + (j+out_offset_h)*out_size.w] =  h_odata_s[i + j*out_size_cropped.w];
				im_s.data[(i+out_offset_w) + (j+out_offset_h)*out_size.w] =  ( char)( unsigned char)( h_odata_s[i + j*out_size_cropped.w]/mx*255.0f);
			}
		}

		sprintf( filename, oformat_str, odir_st, snippet_idx, frame_idx, "_sta.", "txt");
		save_txt( filename, temp, out_size);

		sprintf( filename, oformat_str, odir_s, snippet_idx, frame_idx, "_sta.", oext);
		cv::imwrite( filename, im_s, params);

#ifdef VERBOSE
		shrLog("static ready [%d %d].\n", snippet_idx, frame_idx);
#endif

		// get exclusive ownership of mutex (wait for light to tuen green)
		boost::mutex::scoped_lock lock_it( g_mutex ) ;
		// ok, now we have exclusive access to the light

		is_static_ready = true;		

		// destructor for lock_it will release the mutex	
	}

	im_s.release();
	free(temp);
}


void ComputeDynamicThread() // get input frame
{
	cudaSetDevice(0);

	/// <summary>
	/// Get device properties.
	/// </summary>
	int dev_id;
	cudaDeviceProp dev_props;

	// get number of SMs on this GPU
	cutilSafeCall(cudaGetDevice(&dev_id));
	cutilSafeCall(cudaGetDeviceProperties(&dev_props, dev_id));

	shrLog("device %d: \"%s\" with Compute %d.%d capability\n", dev_id, dev_props.name, dev_props.major, dev_props.minor);
	shrLog("starting dynamic saliency thread ...\n");

	float mx;

	char filename[1024];

	cv::Mat im_d = cv::Mat( out_size.h, out_size.w, CV_8UC1 );
	std::vector<int> params;

	params.push_back( CV_IMWRITE_PXM_BINARY);
	params.push_back( 100);

	for( int i=0;i<out_nbpixels;++i){				
		im_d.data[i] = ( char)( unsigned char)( 0);		
	}

	Dynamic::Pathway oDynamicPathway( in_size_cropped, 0.5f);

	float **maps = 0;
	maps = ( float**) malloc ( queue_len * sizeof( float*));
	for( int i=0;i<queue_len;++i)
		maps[i] = ( float*) malloc ( out_nbpixels_cropped * sizeof( float));

	float *temp = (float*) calloc(sizeof(float), out_nbpixels);

	while(1){

		while( (!is_input_ready || is_dynamic_ready) && !is_completed);

		if (is_completed) break;

#ifdef VERBOSE
		shrLog("computing dynamic [%d %d] ...\n", snippet_idx, frame_idx);
#endif

		sprintf( filename, "%s%s%d%s", mc_dir, clipname, snippet_idx, ".txt");				
		oDynamicPathway.Apply( h_odata_d, h_idata, out_size_cropped, in_size_cropped, filename, frame_idx);

#ifdef VERBOSE
		shrLog("computing median [%d %d] ...\n", snippet_idx, frame_idx);
#endif

		if(frame_idx!=0) {

			// TODO maps should be local
			if ( queue_full)
			{
				for( int j=0;j<out_nbpixels_cropped;++j){			
					maps[queue_idx][j] = h_odata_d[j];
				}

				temporal_filtering( h_odata_d, maps, out_size_cropped);

				if( ++queue_idx == queue_len) queue_idx = 0;				
			}
			else
			{
				for( int j=0;j<out_nbpixels_cropped;++j){
					maps[queue_idx][j] = h_odata_d[j];
				}

				if( ++queue_idx == queue_len){
					queue_full = true;
					queue_idx = 0;
				}
			}

			mx = h_odata_d[0];
			for( int j=0;j<out_nbpixels_cropped;++j){
				if( h_odata_d[j]>mx)
					mx = h_odata_d[j];
			}

#ifdef VERBOSE
			shrLog("writing dynamic [%d %d] ...\n", snippet_idx, frame_idx);
#endif

			for( int j=0;j<out_size_cropped.h;++j){
				for( int i=0;i<out_size_cropped.w;++i){
					temp	 [(i+out_offset_w) + (j+out_offset_h)*out_size.w] =  h_odata_s[i + j*out_size_cropped.w];
					im_d.data[(i+out_offset_w) + (j+out_offset_h)*out_size.w] =  ( char)( unsigned char)( h_odata_d[i + j*out_size_cropped.w]/mx*255.0f);
				}
			}

			sprintf( filename, oformat_str, odir_dt, snippet_idx, frame_idx, "_dyn.", "txt");
			save_txt( filename, temp, out_size);

			sprintf( filename, oformat_str, odir_d, snippet_idx, frame_idx, "_dyn.", oext);
			cv::imwrite( filename, im_d);
		}

#ifdef VERBOSE
		shrLog("dynamic ready [%d %d].\n", snippet_idx, frame_idx);
#endif		

		// get exclusive ownership of mutex (wait for light to tuen green)
		boost::mutex::scoped_lock lock_it( g_mutex ) ;
		// ok, now we have exclusive access to the light

		is_dynamic_ready = true;		
		// destructor for lock_it will release the mutex
	}

	for( int i=0;i<queue_len;++i)
		free(maps[i]);

	free(maps);

	free(temp);
}

void ComputeFusionThread() // get input frame
{
	float mx;

	char filename[1024];

	float *h_odata_sd = (float*) malloc( out_size_cropped.w*out_size_cropped.h * sizeof(float));	

	cv::Mat im = cv::Mat( in_size.h, in_size.w, CV_8UC1);

	cv::Mat im_sd = cv::Mat( out_size.h, out_size.w, CV_8UC1 );
	std::vector<int> params;

	params.push_back( CV_IMWRITE_PXM_BINARY);
	params.push_back( 100);

	for( int i=0;i<out_nbpixels;++i){				
		im_sd.data[i] = ( char)( unsigned char)( 0);		
	}

	//TODO mutex
	for( snippet_idx=init_snippet;snippet_idx<=no_of_snippets;++snippet_idx)
	{
		std::cout << "Snippet " << snippet_idx << std::endl;

		sprintf( command, "%s%s%s%s%d%s%d%s%s%s%d%s"
			, mc_exe
			, " -m AC -p "
			, idir
			, clipname
			, snippet_idx
			, "_%d.png -f 0 -i "
			, max_images
			, " -r "
			, mc_dir
			, clipname
			, snippet_idx
			, ".txt -v");

		queue_full = false;
		queue_idx = 0;

		frame_idx = init_frame;

		is_snippet_completed = false;

		while( frame_idx<max_images) {

			is_input_ready = false;

			while( !is_input_ready);

			if( is_snippet_completed) break;			

#ifdef __STATIC
			is_static_ready = false;
#else
			is_static_ready = true;
#endif

#ifdef __DYNAMIC
			is_dynamic_ready = false;
#else
			is_dynamic_ready = true;
#endif

			while( !is_static_ready || !is_dynamic_ready);

#ifdef VERBOSE
			shrLog("computing fusion [%d %d]...\n", snippet_idx, frame_idx);
#else
			std::cout << "[" << snippet_idx << " " << frame_idx << "]" << "\r";
#endif

#if !defined(__STATIC) && !defined(__DYNAMIC)

			if( frame_idx!=0){

				Utility::Fusion( h_odata_sd, h_odata_s, h_odata_d, out_size_cropped);

				mx = h_odata_sd[0];
				for( int j=0;j<out_nbpixels_cropped;++j){
					if( h_odata_sd[j]>mx)
						mx = h_odata_sd[j];
				}

				for( unsigned int j=0;j<out_size_cropped.h;++j){
					for( unsigned int i=0;i<out_size_cropped.w;++i){
						im_sd.data[(i+out_offset_w) + (j+out_offset_h)*out_size.w] =  ( char)( unsigned char)( h_odata_sd[i + j*out_size_cropped.w]/mx*255.0f);
					}
				}

				sprintf( filename, oformat_str, odir_sd, snippet_idx, frame_idx, "_fus.", oext);
				cv::imwrite( filename, im_sd, params);
			}
#endif

#ifdef VERBOSE
			shrLog("fusion ready [%d %d].\n", snippet_idx, frame_idx);
#endif

			++frame_idx;
		}
	}

	free( h_odata_sd);

	// get exclusive ownership of mutex (wait for light to tuen green)
	boost::mutex::scoped_lock lock_it( g_mutex ) ;
	// ok, now we have exclusive access to the light

	is_completed = true;

	// destructor for lock_it will release the mutex
}


/// <summary>
/// Temporal median filtering.
/// <param name="out">Destination image</param>
/// <param name="in">Source maps</param>
/// <param name="level_size">Image size</param>
/// <returns>Returns the smoothed dynamic salience map.</returns>
/// </summary>
void temporal_filtering(
						float *out
						, float** in
						, siz_t in_size_cropped
						)
{
	float *arr = (float *) malloc( queue_len*sizeof(float));

	float average;
	float tmp;

	unsigned int min_idx;

	int middle = queue_len/2;

	for( int idx=0;idx<in_size_cropped.w*in_size_cropped.h;++idx)
	{
		for( int i=0;i<queue_len;++i)
			arr[i] = in[i][idx];

		for( int i=0;i<queue_len-1;++i){

			min_idx = i;
			for( int j=i+1;j<queue_len;++j){

				if( arr[min_idx]>arr[j])
					min_idx = j;
			}

			tmp = arr[i];
			arr[i] = arr[min_idx];
			arr[min_idx] = tmp;
		}

		if( queue_len%2==0) average = ( float)( arr[middle-1]+arr[middle])/2;
		else average = arr[middle];

		out[idx] = average;
	}

	free(arr);
}
