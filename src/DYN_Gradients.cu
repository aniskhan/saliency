
// includes
#include "error.hpp"
#include "DYN_Gradients.hpp"

void Dynamic::Gradients::CalculateGradientX(
float *gx
, float *curr
, siz_t level_size
) 
{
	int n;
	dim3 dimb( 16, 16, 1);
	dim3 dimg( 
	( level_size.w/dimb.x)   + 1*( level_size.w%dimb.x!=0)
	, ( level_size.h/dimb.y) + 1*( level_size.h%dimb.y!=0)
	, 1
	);

	for( n=0 ; n<2*N ; n++) 
	{
		Dynamic::KernelGradientX<<<dimg, dimb>>>( gx, curr, level_size, n);
		CUDA_CHECK( cudaDeviceSynchronize());
	}	
}

void Dynamic::Gradients::CalculateGradientY(
float *gy
, float *curr
, siz_t level_size
)
{
	int n;
	
	dim3 dimb( 16, 16, 1);
	dim3 dimg(
	( level_size.w/dimb.x)   + 1*( level_size.w%dimb.x!=0)
	, ( level_size.h/dimb.y) + 1*( level_size.h%dimb.y!=0)
	, 1
	);

	for ( n=0 ; n<2*N ; n++) 
	{
		Dynamic::KernelGradientY<<<dimg, dimb>>>( gy, curr, level_size, n);
		CUDA_CHECK( cudaDeviceSynchronize());
	}
}

void Dynamic::Gradients::CalculateGradientT(
float *gt
, float* curr
, float *prev	
, siz_t level_size
) 
{
	int n;
	
	dim3 dimb( 16, 16, 1);
	dim3 dimg( 
	( level_size.w/dimb.x)   + 1*( level_size.w%dimb.x!=0)
	, ( level_size.h/dimb.y) + 1*( level_size.h%dimb.y!=0)
	, 1
	);

	for( n=0 ; n<2*N ; n++) 
	{
		Dynamic::KernelGradientT<<<dimg, dimb>>>( gt, curr, prev, level_size, n);
		CUDA_CHECK( cudaDeviceSynchronize());
	}
}

void Dynamic::Gradients::CalculateGradientXYT(
float *gx
, float *gy
, float *gt
, float* curr
, float *prev	
, siz_t level_size
) 
{
	dim3 dimb( 16, 16, 1);
	dim3 dimg( 
	( level_size.w/dimb.x)   + 1*( level_size.w%dimb.x!=0)
	, ( level_size.h/dimb.y) + 1*( level_size.h%dimb.y!=0)
	, 1
	);

	Dynamic::KernelGradientXYT<<<dimg, dimb>>>( gx, gy, gt, curr, prev, level_size);
	CUDA_CHECK( cudaDeviceSynchronize());
}

__global__ void Dynamic::KernelGradientXYT( 
float *gx
, float *gy
, float *gt
, float* curr
, float *prev
, siz_t level_size
) 
{
	unsigned int x, y, m;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;

	if( x>=level_size.w || y>=level_size.h) return;

	// G(x)=I{-2(+1),-1(-8),+1(+8),+2(-1)}/12
	// G(y)=I{-2w(+1),-1w(-8),+1w(+8),+2w(-1)}/12
	// G(t)=I(t)-I(t-1)
	for( unsigned int n=0 ; n<2*N ; n++) {

		m = x + level_size.w*(y + level_size.h*n);

		if( x<2 || x>=level_size.w-2 || y<2 || y>=level_size.h-2) {
		  	gx[m] = 0;
			gy[m] = 0;
		}
		else {
			gx[m] = ( 
			curr[m - 2] 
			- 8 * curr[m - 1] 
			+ 8 * curr[m + 1] 
			- curr[m + 2]
			) / 12;

			gy[m] = (
			curr[m - 2*level_size.w] 
			- 8 * curr[m - level_size.w] 
			+ 8 * curr[m + level_size.w] 
			- curr[m + 2*level_size.w]
			) / 12;
		}

		gt[m] = curr[m] - prev[m];
	}
}

__global__ void Dynamic::KernelGradientX( 
float *gx
, float *curr	
, siz_t level_size
, int n	
) 
{
	int x, y, m;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;

	if ( x>=level_size.w || y>=level_size.h) return;

	m = x + level_size.w*(y + level_size.h*n);

	if ( x<2 || x>=level_size.w-2 || y<2 || y>=level_size.h-2){
		gx[m] = 0;
	}
	else{
		gx[m] = ( 
			-1 * curr[m - 2]
		+ 8 * curr[m - 1]
		- 8 * curr[m + 1]
		+ curr[m + 2]
		) / 12;
		/*
		gx[m] = ( 
		curr[m - 2]
		- 8 * curr[m - 1]
		+ 8 * curr[m + 1]
		- curr[m + 2]
		) / 12;
		*/
	}
}

__global__ void Dynamic::KernelGradientY( 
float *gy
, float *curr	
, siz_t level_size
, int n	
) 
{
	int x, y, m;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;

	if ( x>=level_size.w || y>=level_size.h) return;

	m = x + level_size.w*(y + level_size.h*n);

	if ( x<2 || x>=level_size.w-2 || y<2 || y>=level_size.h-2){
		gy[m] = 0;
	}
	else{
		gy[m] = (
		- 1 * curr[m - 2*level_size.w] 
		+ 8 * curr[m - 1*level_size.w] 
		- 8 * curr[m + 1*level_size.w]
		+ 1 * curr[m + 2*level_size.w]
		) / 12;
		/*
		gy[m] = (
		curr[m - 2*level_size.w] 
		- 8 * curr[m - level_size.w] 
		+ 8 * curr[m + level_size.w]
		- curr[m + 2*level_size.w]
		) / 12;
		*/
	}
}

__global__ void Dynamic::KernelGradientT ( 
float *gt
, float* curr
, float *prev	
, siz_t level_size
, int n
) 
{
	int x, y, m;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;

	if( x>=level_size.w || y>=level_size.h) return;

	m = x + level_size.w*(y + level_size.h*n);

	gt[m] = curr[m] - prev[m];
}
