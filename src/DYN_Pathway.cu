
// includes
#include "DYN_Pathway.hpp"

#include "UTY_Debug.hpp"

#include <time.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <math.h>

void Dynamic::Pathway::Init()
{
	unsigned int ORIGINAL_SIZE, SCALED_SIZE, LEVEL_SIZE;

	ORIGINAL_SIZE 	= size_			* sizeof(float);
	SCALED_SIZE		= size_scaled_	* sizeof(float);

	if ( im_size_scaled_.w<64 || im_size_scaled_.h<64) {
		//fprintf( stderr, "Error: The minimum size allowed for frames is 64x64\n");
		exit( EXIT_FAILURE);
	}

	p.im_size = im_size_scaled_;

	for( unsigned int k=0 ; k<K ; ++k) 
	{
		if( k==0) 
		{
			p.levels[k].level_size.w = p.im_size.w;
			p.levels[k].level_size.h = p.im_size.h;
		}
		else 
		{
			p.levels[k].level_size.w = p.levels[k-1].level_size.w/2;
			p.levels[k].level_size.h = p.levels[k-1].level_size.h/2;
		}

		LEVEL_SIZE = p.levels[k].level_size.w * p.levels[k].level_size.h * sizeof(float);

		CUDA_CHECK( cudaMalloc( ( void**)&( p.levels[k].frame),   LEVEL_SIZE));
		CUDA_CHECK( cudaMalloc( ( void**)&( p.levels[k].prev), 	  LEVEL_SIZE));
		CUDA_CHECK( cudaMalloc( ( void**)&( p.levels[k].shift),   LEVEL_SIZE));
		CUDA_CHECK( cudaMalloc( ( void**)&( p.levels[k].vx), 	  LEVEL_SIZE));
		CUDA_CHECK( cudaMalloc( ( void**)&( p.levels[k].vy), 	  LEVEL_SIZE));
		CUDA_CHECK( cudaMalloc( ( void**)&( p.levels[k].vx_med),  LEVEL_SIZE));
		CUDA_CHECK( cudaMalloc( ( void**)&( p.levels[k].vy_med),  LEVEL_SIZE));
		CUDA_CHECK( cudaMalloc( ( void**)&( p.levels[k].filt),	  2*N*LEVEL_SIZE));
		CUDA_CHECK( cudaMalloc( ( void**)&( p.levels[k].prefilt), 2*N*LEVEL_SIZE));
	}

	CUDA_CHECK( cudaMalloc( ( void**)&d_im_curr, ORIGINAL_SIZE));
	CUDA_CHECK( cudaMalloc( ( void**)&d_im_prev, ORIGINAL_SIZE));
	CUDA_CHECK( cudaMalloc( ( void**)&d_im_temp, ORIGINAL_SIZE));

	CUDA_CHECK( cudaMalloc( ( void**)&d_im_curr_scaled, SCALED_SIZE));
	CUDA_CHECK( cudaMalloc( ( void**)&d_im_prev_scaled, SCALED_SIZE));

	//	CUDA_CHECK( cudaMalloc( ( void**)&d_im_mcom, SCALED_SIZE));
	CUDA_CHECK( cudaMalloc( ( void**)&d_im_mcom, ORIGINAL_SIZE));
	CUDA_CHECK( cudaMalloc( ( void**)&d_optflow, SCALED_SIZE));

	CUDA_CHECK( cudaMalloc( ( void**)&d_im_buffer, QUEUE_LEN*SCALED_SIZE));

	CUDA_CHECK( cudaMalloc( ( void**)&( p.gx), 	2*N*SCALED_SIZE));
	CUDA_CHECK( cudaMalloc( ( void**)&( p.gy), 	2*N*SCALED_SIZE));
	CUDA_CHECK( cudaMalloc( ( void**)&( p.gt), 	2*N*SCALED_SIZE));
	CUDA_CHECK( cudaMalloc( ( void**)&( p.mod), 2*N*SCALED_SIZE));

	//CUDA_CHECK( cudaMalloc( ( void**)&( p.dx), ( N*( 2*N - 1)) * SCALED_SIZE));
	//CUDA_CHECK( cudaMalloc( ( void**)&( p.dy), ( N*( 2*N - 1)) * SCALED_SIZE));
	//CUDA_CHECK( cudaMalloc( ( void**)&( p.wi), ( N*( 2*N - 1)) * SCALED_SIZE));

	CUDA_CHECK( cudaMalloc( ( void**)&( p.threshold),	  SCALED_SIZE));
	CUDA_CHECK( cudaMalloc( ( void**)&( p.threshold_med), SCALED_SIZE));
	CUDA_CHECK( cudaMalloc( ( void**)&( p.temp),		  SCALED_SIZE));

	oModulation.Init( p.mod, p.im_size, f0);

#if defined VERBOSE
	std::stringstream ss_imgname;

	for( int k=0 ; k<2*N ; ++k)
	{
		ss_imgname.clear();//clear any bits set
		ss_imgname.str(std::string());
		ss_imgname << "tmp/0_mod_" << k << ".png";
		Utility::write_image_device_v1( (char*) ss_imgname.str().c_str(), &p.mod[k*p.im_size.w*p.im_size.h], p.im_size);
		/*
		ss_imgname.clear();//clear any bits set
		ss_imgname.str(std::string());
		ss_imgname << "tmp/0_mod_" << k << ".txt";
		Utility::save_txt_device( (char*) ss_imgname.str().c_str(), &p.mod[k*p.im_size.w*p.im_size.h], p.im_size);
		*/
	}
#endif

	//	params = (float*)malloc( max_frames_*DATA_LINE_LEN * sizeof(float));

	//	CUDA_CHECK( cudaMalloc((void **)&d_params, max_frames_*DATA_LINE_LEN * sizeof(float)));

	//	oMotionCompensation.FetchCompensationVariables( params, filename_, max_frames_);

	//	CUDA_CHECK( cudaMemcpy(d_params, params, max_frames_*DATA_LINE_LEN * sizeof(float), cudaMemcpyHostToDevice));
}

void Dynamic::Pathway::Apply(
							 float* out
							 , float *in
							 , const std::vector<float> compensation_vars
							 , siz_t im_size_scaled
							 , siz_t im_size
							 , unsigned int idx
							 )
{
#if defined VERBOSE
	std::stringstream ss_imgname;
#endif

	unsigned int ORIGINAL_SIZE, SCALED_SIZE;

	SCALED_SIZE 	= im_size_scaled_.w * im_size_scaled_.h * sizeof(float);
	ORIGINAL_SIZE 	= im_size.w * im_size.h 		* sizeof(float);	

	if( idx == 0){

#if defined RESCALE
		CUDA_CHECK( cudaMemcpy( d_im_curr, in, ORIGINAL_SIZE, cudaMemcpyHostToDevice));

		oGaussianRecursive.Apply( d_im_temp, d_im_curr, im_size, 1, 1.0f);
		oImageTransform1.Resize( d_im_prev_scaled, d_im_temp, im_size_scaled_, im_size);
#else
		CUDA_CHECK( cudaMemcpy( d_im_prev_scaled, in, ORIGINAL_SIZE, cudaMemcpyHostToDevice));
#endif

		return;
	}

#if defined RESCALE
	CUDA_CHECK( cudaMemcpy( d_im_curr, in, ORIGINAL_SIZE, cudaMemcpyHostToDevice));

	if( compensation_vars.empty())
	{
		oGaussianRecursive.Apply( d_im_temp, d_im_curr, im_size, 1, 1.0f);
		oImageTransform1.Resize( d_im_curr_scaled, d_im_temp, im_size_scaled_, im_size);
	}
	else
	{
		oMotionCompensation.Apply( d_im_mcom, d_im_curr, compensation_vars, im_size);
	}

	oGaussianRecursive.Apply( d_im_temp, d_im_mcom, im_size, 1, 1.0f);
	oImageTransform1.Resize( d_im_curr_scaled, d_im_temp, im_size_scaled_, im_size);

#if defined VERBOSE
	Utility::write_image_device( "tmp/1_curr.png", d_im_curr_scaled, im_size_scaled_);
	Utility::write_image_device( "tmp/1_prev.png", d_im_prev_scaled, im_size_scaled_);
#endif

	oCommon.CalculatePyramids( p.levels, d_im_prev_scaled, d_im_curr_scaled, p.im_size, p.temp);

#else
	CUDA_CHECK( cudaMemcpy( d_im_curr_scaled, in, ORIGINAL_SIZE, cudaMemcpyHostToDevice));

	if( compensation_vars.empty())
	{
#if defined VERBOSE
		Utility::write_image_device_v1( "tmp/1_curr.png", d_im_curr_scaled, im_size_scaled_);
		Utility::write_image_device_v1( "tmp/1_prev.png", d_im_prev_scaled, im_size_scaled_);
#endif

		oCommon.CalculatePyramids( p.levels, d_im_prev_scaled, d_im_curr_scaled, p.im_size, p.temp);
	}
	else
	{
		oMotionCompensation.Apply( d_im_mcom, d_im_curr_scaled, compensation_vars, im_size);

#if defined VERBOSE
		Utility::write_image_device_v1( "tmp/1_comp.png", d_im_mcom, im_size);
		Utility::write_image_device_v1( "tmp/1_curr.png", d_im_mcom, im_size_scaled_);
		Utility::write_image_device_v1( "tmp/1_prev.png", d_im_prev_scaled, im_size_scaled_);
#endif

		oCommon.CalculatePyramids( p.levels, d_im_prev_scaled, d_im_mcom, p.im_size, p.temp);
	}
#endif

	for( int k=K-1 ; k>=0 ; k--)
	{	
#if defined VERBOSE
		ss_imgname.clear();//clear any bits set
		ss_imgname.str(std::string());
		ss_imgname << "tmp/1_prev_" << k << ".png";
		Utility::write_image_device_v1( (char*) ss_imgname.str().c_str(), p.levels[k].prev, p.levels[k].level_size);

		ss_imgname.clear();//clear any bits set
		ss_imgname.str(std::string());
		ss_imgname << "tmp/1_curr_" << k << ".png";
		Utility::write_image_device_v1( (char*) ss_imgname.str().c_str(), p.levels[k].frame, p.levels[k].level_size);
#endif

		oRetina.RetinaFilter( p.levels[k].prev, p.levels[k].prev, p.levels[k].level_size, 1);
		oRetina.RetinaFilter( p.levels[k].frame, p.levels[k].frame, p.levels[k].level_size, 1);

#if defined VERBOSE
		ss_imgname.clear();//clear any bits set
		ss_imgname.str(std::string());
		ss_imgname << "tmp/2_prev_ret_" << k << ".png";
		Utility::write_image_device_v1( (char*) ss_imgname.str().c_str(), p.levels[k].prev, p.levels[k].level_size);

		ss_imgname.clear();//clear any bits set
		ss_imgname.str(std::string());
		ss_imgname << "tmp/2_curr_ret_" << k << ".png";
		Utility::write_image_device_v1( (char*) ss_imgname.str().c_str(), p.levels[k].frame, p.levels[k].level_size);
#endif

		if ( k==0)
		{
			oCommon.ApplyThreshold ( p.levels[k].frame, p.levels[k].prev, p.levels[k].level_size, p.threshold);
			oMedianFilter.Apply( p.threshold_med, p.threshold, p.levels[k].level_size);

#if defined VERBOSE
			Utility::write_image_device_v1( "D:/tmp/dynamic/2_thr.png", p.threshold_med, im_size_scaled_);
#endif
		}

		if ( k<K-1) 
		{
			// kernel_proj >> kernel_interp >> kernel_coef
			// outputs: shift[k], temp, vx[k], vy[k]
			oCommon.TransformLevel( p.levels[k].frame, p.levels[k].shift, p.levels[k].vx, p.levels[k].vy, p.levels[k].level_size, p.levels[k+1].vx, p.levels[k+1].vy, p.levels[k+1].level_size, p.temp);

#if defined VERBOSE
			ss_imgname.clear();//clear any bits set
			ss_imgname.str(std::string());
			ss_imgname << "tmp/3_shift_" << k << ".png";
			Utility::write_image_device_v1( (char*) ss_imgname.str().c_str(), p.levels[k].shift, p.levels[k].level_size);
#endif

			// kernel_gaus_rec_hor >> kernel_gaus_rec_ver >> kernel_high_pas >> kernel_modulation >> kernel_demodulation
			// output: p.levels[k].prefilt, p.levels[k].filt
			oGabor.Apply( p.levels[k].prefilt, p.levels[k].prev, p.mod, p.levels[k].level_size, p.levels[0].level_size);			
			oGabor.Apply( p.levels[k].filt, p.levels[k].shift, p.mod, p.levels[k].level_size, p.levels[0].level_size);	
		}
		else 
		{
			// output: p.levels[k].prefilt, p.levels[k].filt
			oGabor.Apply( p.levels[k].prefilt, p.levels[k].prev, p.mod, p.levels[k].level_size, p.levels[0].level_size);			
			oGabor.Apply( p.levels[k].filt, p.levels[k].frame, p.mod, p.levels[k].level_size, p.levels[0].level_size);
		}

#if defined VERBOSE
		ss_imgname.clear();//clear any bits set
		ss_imgname.str(std::string());
		ss_imgname << "tmp/3_prefilt_" << k << ".png";
		Utility::write_image_device_v1( (char*) ss_imgname.str().c_str(), p.levels[k].prefilt, p.levels[k].level_size);

		ss_imgname.clear();//clear any bits set
		ss_imgname.str(std::string());
		ss_imgname << "tmp/3_filt_" << k << ".png";
		Utility::write_image_device_v1( (char*) ss_imgname.str().c_str(), p.levels[k].filt, p.levels[k].level_size);
#endif

		// kernel_gradx >> kernel_grady	>> kernel_gradt
		// output: p.gx[k], p.gy[k], p.gt[k]

		oGradients.CalculateGradientX( p.gx, p.levels[k].filt, p.levels[k].level_size);
		oGradients.CalculateGradientY( p.gy, p.levels[k].filt, p.levels[k].level_size);
		oGradients.CalculateGradientT( p.gt, p.levels[k].filt, p.levels[k].prefilt, p.levels[k].level_size);

#if defined VERBOSE
		ss_imgname.clear();//clear any bits set
		ss_imgname.str(std::string());
		ss_imgname << "tmp/4_gx_" << k << ".png";
		Utility::write_image_device_v1( (char*) ss_imgname.str().c_str(), p.gx, p.levels[k].level_size);

		ss_imgname.clear();//clear any bits set
		ss_imgname.str(std::string());
		ss_imgname << "tmp/4_gy_" << k << ".png";
		Utility::write_image_device_v1( (char*) ss_imgname.str().c_str(), p.gy, p.levels[k].level_size);

		ss_imgname.clear();//clear any bits set
		ss_imgname.str(std::string());
		ss_imgname << "tmp/4_gt_" << k << ".png";
		Utility::write_image_device_v1( (char*) ss_imgname.str().c_str(), p.gt, p.levels[k].level_size);
#endif

		//		oGradients.CalculateGradientXYT( p.gx, p.gy, p.gt, p.levels[k].filt, p.levels[k].prefilt, p.levels[k].level_size);	

		// output: p.levels[k].vx, p.levels[k].vy
		if ( k==K-1) 
			oMotionEstimation.Apply( p.levels[k].vx, p.levels[k].vy, p.gx, p.gy, p.gt, p.levels[k].level_size, true);
		else 
			oMotionEstimation.Apply( p.levels[k].vx, p.levels[k].vy, p.gx, p.gy, p.gt, p.levels[k].level_size, false);

		//oMedianFilter.Apply( p.levels[k].vx_med, p.levels[k].vx, p.levels[k].level_size);
		//oMedianFilter.Apply( p.levels[k].vy_med, p.levels[k].vy, p.levels[k].level_size);

		// kernel_gaus_rec_hor >> kernel_gaus_rec_ver
		// I/O: p.levels[k].vx, p.levels[k].vy
		oGaussianRecursive.Apply( p.levels[k].vx_med, p.levels[k].vx, p.temp, p.levels[k].level_size, 1, s2);
		oGaussianRecursive.Apply( p.levels[k].vy_med, p.levels[k].vy, p.temp, p.levels[k].level_size, 1, s2);

#if defined VERBOSE
		ss_imgname.clear();//clear any bits set
		ss_imgname.str(std::string());
		ss_imgname << "tmp/5_vx_" << k << ".png";
		Utility::write_image_device_v1( (char*) ss_imgname.str().c_str(), p.levels[k].vx, p.levels[k].level_size);

		ss_imgname.clear();//clear any bits set
		ss_imgname.str(std::string());
		ss_imgname << "tmp/5_vy_" << k << ".png";
		Utility::write_image_device_v1( (char*) ss_imgname.str().c_str(), p.levels[k].vy, p.levels[k].level_size);
#endif
	}

	oCommon.CombineMotionVectors( p.levels[0].vx, p.levels[0].vx_med, p.levels[0].vy_med, p.threshold_med, p.levels[0].level_size);

#if defined VERBOSE
	Utility::write_image_device_v1( "tmp/6_v.png", p.levels[0].vx, im_size_scaled_);
#endif

	if (queue_id==QUEUE_LEN)
	{
		queue_id = 0;
		apply_temporal_filter = true;
	}
	else 
	{
		CUDA_CHECK( cudaMemcpy( &d_im_buffer[queue_id * (im_size_scaled_.w*im_size_scaled_.h)], p.levels[0].vx, SCALED_SIZE, cudaMemcpyDeviceToDevice));

		++queue_id;
	}

	if (apply_temporal_filter)
		oCommon.TemporalFilter( p.levels[0].vx, d_im_buffer, im_size_scaled_);		

	oGaussianRecursive.Apply( d_optflow, p.levels[0].vx, p.temp, p.levels[0].level_size, 1, s2);

#if defined VERBOSE
	Utility::write_image_device_v1( "tmp/6_v_temp.png", d_optflow, im_size_scaled_);
#endif

#if defined RESCALE
	oImageTransform2.Resize( d_im_curr, d_optflow, im_size, im_size_scaled_);

	CUDA_CHECK( cudaMemcpy( out, d_im_curr, ORIGINAL_SIZE, cudaMemcpyDeviceToHost));

	CUDA_CHECK( cudaMemcpy( d_im_prev_scaled, d_im_curr_scaled, SCALED_SIZE, cudaMemcpyDeviceToDevice));
#else
	CUDA_CHECK( cudaMemcpy( out, d_optflow, ORIGINAL_SIZE, cudaMemcpyDeviceToHost));

	if( compensation_vars.empty()) {
		CUDA_CHECK( cudaMemcpy( d_im_prev_scaled, d_im_curr_scaled, SCALED_SIZE, cudaMemcpyDeviceToDevice));
	}
	else {
		CUDA_CHECK( cudaMemcpy( d_im_prev_scaled, d_im_mcom, SCALED_SIZE, cudaMemcpyDeviceToDevice));
	}
#endif
}

void Dynamic::Pathway::Clean()
{
	for( unsigned int k=0 ; k<K ; k++)
	{
		p.levels[k].frame = NULL; CUDA_CHECK( cudaFree( p.levels[k].frame));
		p.levels[k].prev = NULL; CUDA_CHECK( cudaFree( p.levels[k].prev));
		p.levels[k].shift = NULL; CUDA_CHECK( cudaFree( p.levels[k].shift));

		p.levels[k].vx = NULL; CUDA_CHECK( cudaFree( p.levels[k].vx));
		p.levels[k].vy = NULL; CUDA_CHECK( cudaFree( p.levels[k].vy));

		p.levels[k].vx_med = NULL; CUDA_CHECK( cudaFree( p.levels[k].vx_med));
		p.levels[k].vy_med = NULL; CUDA_CHECK( cudaFree( p.levels[k].vy_med));

		p.levels[k].filt = NULL; CUDA_CHECK( cudaFree( p.levels[k].filt));
		p.levels[k].prefilt = NULL; CUDA_CHECK( cudaFree( p.levels[k].prefilt));
	}

	p.gx = NULL; CUDA_CHECK( cudaFree( p.gx));
	p.gy = NULL; CUDA_CHECK( cudaFree( p.gy));
	p.gt = NULL; CUDA_CHECK( cudaFree( p.gt));
	p.mod = NULL; CUDA_CHECK( cudaFree( p.mod));

	p.threshold = NULL; CUDA_CHECK( cudaFree( p.threshold));
	p.threshold_med = NULL; CUDA_CHECK( cudaFree( p.threshold_med));
	p.temp = NULL; CUDA_CHECK( cudaFree( p.temp));

	d_optflow = NULL; CUDA_CHECK( cudaFree( d_optflow));

	d_im_curr = NULL; CUDA_CHECK( cudaFree( d_im_curr));
	d_im_prev = NULL; CUDA_CHECK( cudaFree( d_im_prev));
	d_im_temp = NULL; CUDA_CHECK( cudaFree( d_im_temp));

	d_im_buffer = NULL; CUDA_CHECK( cudaFree( d_im_buffer));

	d_im_curr_scaled = NULL; CUDA_CHECK( cudaFree( d_im_curr_scaled));
	d_im_prev_scaled = NULL; CUDA_CHECK( cudaFree( d_im_prev_scaled));

	//CUDA_CHECK( cudaFree( d_params));

	//free( params);

	CUDA_CHECK( cudaThreadExit());
}
