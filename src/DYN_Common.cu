
// includes
#include "error.hpp"
#include "DYN_Common.hpp"
#include "DYN_GaussianRecursive.hpp"
//#include "DYN_Debug.hpp"

void Dynamic::Common::ApplyInterpolation ( 
float *frame
, float *shift
, float *temp
, float *vx
, float *vy
, siz_t level_size
) {}

void Dynamic::Common::ApplyCoefficient (
float *shift
, float *temp
, siz_t level_size
) {}

void Dynamic::Common::CombineMotionVectors(
float *out
, float *vx
, float *vy
, float *threshold
, siz_t level_size
) 
{
	dim3 dimBlock( 16, 16, 1);
	dim3 dimGrid( 
	level_size.w / dimBlock.x + 1*( level_size.w%dimBlock.x!=0)
	, level_size.h / dimBlock.y + 1*( level_size.h%dimBlock.y!=0)
	, 1
	);

	Dynamic::KernelCombineVectors<<<dimGrid, dimBlock>>>( out, vx, vy, threshold, level_size);
	CUDA_CHECK( cudaDeviceSynchronize());
}

void Dynamic::Common::ApplyThreshold( 
float* curr
, float *prev
, siz_t level_size
, float *threshold
) 
{
	dim3 dimb( 16, 16, 1);
	dim3 dimg( 
	( level_size.w/dimb.x)+1*( level_size.w%dimb.x!=0)
	, ( level_size.h/dimb.y)+1*( level_size.h%dimb.y!=0)
	, 1
	);

	Dynamic::KernelThreshold<<<dimg, dimb>>>( curr, prev, level_size, threshold);
	CUDA_CHECK( cudaDeviceSynchronize());
}

void Dynamic::Common::ApplyProjection ( 
float *vx0
, float *vy0
, siz_t level_size_curr
, float *vx1
, float *vy1
, siz_t level_size_prev
, float *shift
, float *temp
) 
{}

void Dynamic::Common::ApplyHighPass(
float *bank
, float *mat									
, siz_t level_size
)
{
	dim3 dimb( 16, 16, 1);
	dim3 dimg( 
	( level_size.w/dimb.x)+1*( level_size.w%dimb.x!=0)
	, ( level_size.h/dimb.y)+1*( level_size.h%dimb.y!=0)
	, 1
	);

	Dynamic::KernelHighPass<<<dimg, dimb>>>( bank, mat, level_size);
	CUDA_CHECK( cudaDeviceSynchronize());
}

void Dynamic::Common::CalculatePyramids ( 
level_t levels[K]
, float *frame_01
, float *frame_02
, siz_t im_size
, float *temp
)
{	
	dim3 dimb( 16, 16, 1);
	dim3 dimg( 1, 1, 1);

	unsigned int SIZE = im_size.w*im_size.h * sizeof( float);

	CUDA_CHECK( cudaMemcpy( levels[0].prev,  frame_01, SIZE, cudaMemcpyDeviceToDevice));
	CUDA_CHECK( cudaMemcpy( levels[0].frame, frame_02, SIZE, cudaMemcpyDeviceToDevice));

	for ( unsigned int k=1;k<K;k++) {

		dimg.x = ( levels[k].level_size.w/dimb.x)+1*( levels[k].level_size.w%dimb.x!=0);
		dimg.y = ( levels[k].level_size.h/dimb.y)+1*( levels[k].level_size.h%dimb.y!=0);

		oGaussianRecursive.Apply( temp, levels[k-1].prev, levels[k-1].level_size, 1, s3);

		Dynamic::KernelCalculateLevel <<<dimg, dimb>>> ( levels[k].prev, temp, levels[k].level_size, levels[k-1].level_size);
		CUDA_CHECK( cudaDeviceSynchronize());

		oGaussianRecursive.Apply( temp, levels[k-1].frame, levels[k-1].level_size, 1, s3);

		Dynamic::KernelCalculateLevel <<<dimg, dimb>>> ( levels[k].frame, temp, levels[k].level_size, levels[k-1].level_size);		
		CUDA_CHECK( cudaDeviceSynchronize());
	}
}

void Dynamic::Common::TransformLevel ( 
float *frame
, float *shift
, float *vx0
, float *vy0
, siz_t level_size_curr
, float *vx1
, float *vy1
, siz_t level_size_prev
, float *temp
)
{
	dim3 dimb( 16, 16, 1);
	dim3 dimg( 
	( level_size_curr.w/dimb.x)+1*( level_size_curr.w%dimb.x!=0)
	, ( level_size_curr.h/dimb.y)+1*( level_size_curr.h%dimb.y!=0)
	, 1
	);

	// Vx0 and Vy0 are outputs and shift/temp are intialized to zero
	Dynamic::KernelProjection <<<dimg, dimb>>> ( vx0, vy0, level_size_curr, vx1, vy1, level_size_prev, shift, temp);
	CUDA_CHECK( cudaDeviceSynchronize());

	// shift and temp are outputs
	Dynamic::KernelInterpolation<<<dimg, dimb>>> ( frame, shift, temp, vx0, vy0, level_size_curr);
	CUDA_CHECK( cudaDeviceSynchronize());

	// shift is output
	Dynamic::KernelCoefficient <<<dimg, dimb>>> ( shift, temp, level_size_curr);
	CUDA_CHECK( cudaDeviceSynchronize());
}

void Dynamic::Common::TemporalFilter(
float *out
, float *in
, siz_t im_size
)
{
	dim3 dimBlock( 16, 16, 1);
	dim3 dimGrid( 
	im_size.w / dimBlock.x + 1*( im_size.w%dimBlock.x!=0)
	, im_size.h / dimBlock.y + 1*( im_size.h%dimBlock.y!=0)
	, 1
	);

	Dynamic::KernelTemporalFilter<<<dimGrid, dimBlock>>>( out, in, im_size);
	CUDA_CHECK( cudaDeviceSynchronize());
}

__global__ void Dynamic::KernelInterpolation ( 
float *frame
, float *shift
, float *temp
, float *vx
, float *vy
, siz_t level_size
) 
{
	int x, y
	, m, l
	, a, b
	, sa, sb;

	float u, out, val
	, k00, k10, k01, k11
	, x00, x10, x01, x11
	, y00, y10, y01, y11;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;
	if ( x>=level_size.w || y>=level_size.h) return;

	m = x + y*level_size.w;

	val = frame[m];

	u	=- vx[m];
	out =- vy[m];

	a = ( int)u;
	b = ( int)out;

	if(   u>=0) sa = 1; else sa =- 1;
	if( out>=0) sb = 1; else sb =- 1;

	u	= fabsf(   u - ( float)a);
	out = fabsf( out - ( float)b);

	x00 = x + a; x01 = x00; x10 = x00 + sa; x11 = x10;
	y00 = y + b; y10 = y00; y01 = y00 + sb; y11 = y01;

	k00 = ( 1-u) * ( 1-out);
	k10 =	  u  * ( 1-out);
	k01 = ( 1-u) * out;
	k11 =	  u  * out;

	if( x00>=0 && x00<level_size.w && y00>=0 && y00<level_size.h){

		l = x00 + y00*level_size.w;

		shift[l] += k00*val;
		temp[l]  += k00;
	}

	if( x10>=0 && x10<level_size.w && y10>=0 && y10<level_size.h){

		l = x10 + y10*level_size.w;

		shift[l] += k10*val;
		temp[l]  += k10;
	}

	if( x01>=0 && x01<level_size.w && y01>=0 && y01<level_size.h){

		l = x01 + y01*level_size.w;

		shift[l] += k01*val;
		temp[l]  += k01;
	}
	if( x11>=0 && x11<level_size.w && y11>=0 && y11<level_size.h){
		l = x11 + y11*level_size.w;

		shift[l] += k11*val;
		temp[l]  += k11;
	}
}

__global__ void Dynamic::KernelCoefficient( 
float *out
, float *in
, siz_t level_size
) 
{
	int x, y, m;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;
	if ( x>=level_size.w || y>=level_size.h) return;

	m = x + y*level_size.w;

	if( in[m]!=0)
	out[m] /= in[m];
	else 
	out[m] = 127;
}

__global__ void Dynamic::KernelCombineVectors( 
float *out
, float *vx
, float *vy
, float *threshold
, siz_t level_size
)
{
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

	if ( x>=level_size.w || y>=level_size.h) return;

	float t1 = 0.0f;

	if ( threshold[y*level_size.w + x]<= VP)
	t1 = 1.0f;

	out[y*level_size.w + x] = t1 * sqrt( powf( vx[y*level_size.w + x], 2) + powf( vy[y*level_size.w + x], 2));		
}

__global__ void Dynamic::KernelThreshold( 
float *curr
, float *prev
, siz_t level_size
, float *threshold)
{
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

	if ( x>=level_size.w || y>=level_size.h) return;

	threshold[y*level_size.w + x] = 100 * ( ( ( fabs( curr[y*level_size.w + x] - prev[y*level_size.w + x])) < 0.5f) ? 1.0f : 0.0f);	
}

__global__ void Dynamic::KernelProjection( 
float *vx0
, float *vy0
, siz_t level_size_curr
, float *vx1
, float *vy1
, siz_t level_size_prev
, float *shift
, float *temp
)
{
	int x0, y0
	, x1, y1
	, m0, m1;


	x0 = threadIdx.x + blockIdx.x*blockDim.x;
	y0 = threadIdx.y + blockIdx.y*blockDim.y;
	if ( x0>=level_size_curr.w || y0>=level_size_curr.h) return;

	x1 = x0/2;
	y1 = y0/2;

	if ( x1==level_size_prev.w) x1--;
	if ( y1==level_size_prev.h) y1--;

	m0 = x0 + y0*level_size_curr.w;
	m1 = x1 + y1*level_size_prev.w;

	vx0[m0] = vx1[m1]*2;
	vy0[m0] = vy1[m1]*2;

	shift[m0] = 0;
	temp [m0] = 0;
}

__global__ void Dynamic::KernelHighPass( 
float *bank
, float *mat	
, siz_t level_size
) 
{
	int x, y
	, n, m
	, size = level_size.w*level_size.h;

	float temp;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;
	if ( x>=level_size.w || y>=level_size.h) return;

	m = x + y*level_size.w;

	temp = mat[m] - bank[m];

	for ( n=0;n<N;n++)
	bank[m + n*size] = temp;
}


__global__ void Dynamic::KernelCalculateLevel ( 
float *curr
, float *prev
, siz_t level_size_curr
, siz_t level_size_prev	
)
{
	int x, y
	, m1, m2;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;
	if ( x>=level_size_curr.w || y>=level_size_curr.h) return;

	m1 = (2*x) + (2*y)* level_size_prev.w;
	m2 =	x  +	y * level_size_curr.w;

	curr[m2] = prev[m1];
}

__global__ void Dynamic::KernelTemporalFilter ( 
float *out
, float *in
, siz_t im_size
)
{
	unsigned int x, y
	, m
	, size = im_size.w*im_size.h
	, middle = QUEUE_LEN/2
	, min_idx;

	float arr[QUEUE_LEN];

	float average, tmp;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;
	if ( x>=im_size.w || y>=im_size.h) return;

	m = x +	y * im_size.w;

	for( int i=0;i<QUEUE_LEN;++i)
	arr[i] = in[m + i*size];

	for( int i=0;i<QUEUE_LEN-1;++i){

		min_idx = i;
		for( int j=i+1;j<QUEUE_LEN;++j){

			if( arr[min_idx]>arr[j])
			min_idx = j;
		}

		tmp = arr[i];
		arr[i] = arr[min_idx];
		arr[min_idx] = tmp;
	}

	if( QUEUE_LEN%2==0) average = ( float)( arr[middle-1]+arr[middle])/2;
	else average = arr[middle];

	out[m] = average;
}
