/*
* Copyright 1993-2010 NVIDIA Corporation.  All rights reserved.
*
* Please refer to the NVIDIA end user license agreement (EULA) associated
* with this source code for terms and conditions that govern your use of
* this software. Any use, reproduction, disclosure, or distribution of
* this software and related documentation outside the terms of the EULA
* is strictly prohibited.
*
*/


#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cutil_inline.h>
#include "UTY_ConvolutionFFT2D.hpp"

#define  USE_TEXTURE 1
#define POWER_OF_TWO 1


#if(USE_TEXTURE)
    texture<float, 1, cudaReadModeElementType> texFloat;
    #define   LOAD_FLOAT(i) tex1Dfetch(texFloat, i)
    #define  SET_FLOAT_BASE cutilSafeCall( cudaBindTexture(0, texFloat, d_Src) )
#else
    #define  LOAD_FLOAT(i) d_Src[i]
    #define SET_FLOAT_BASE
#endif

#if(USE_TEXTURE)
    texture<fComplex, 1, cudaReadModeElementType> texComplexA;
    texture<fComplex, 1, cudaReadModeElementType> texComplexB;
    #define    LOAD_FCOMPLEX(i) tex1Dfetch(texComplexA, i)
    #define  LOAD_FCOMPLEX_A(i) tex1Dfetch(texComplexA, i)
    #define  LOAD_FCOMPLEX_B(i) tex1Dfetch(texComplexB, i)

    #define   SET_FCOMPLEX_BASE cutilSafeCall( cudaBindTexture(0, texComplexA,  d_Src) )
    #define SET_FCOMPLEX_BASE_A cutilSafeCall( cudaBindTexture(0, texComplexA, d_SrcA) )
    #define SET_FCOMPLEX_BASE_B cutilSafeCall( cudaBindTexture(0, texComplexB, d_SrcB) )
#else
    #define    LOAD_FCOMPLEX(i)  d_Src[i]
    #define  LOAD_FCOMPLEX_A(i) d_SrcA[i]
    #define  LOAD_FCOMPLEX_B(i) d_SrcB[i]

    #define   SET_FCOMPLEX_BASE
    #define SET_FCOMPLEX_BASE_A
    #define SET_FCOMPLEX_BASE_B
#endif

#define EPS		2.2204e-016

#define PI 3.1415926535897932384626433832795
#define BLOCKDIM 256

inline uint factorRadix2(uint& log2N, uint n){
    if(!n){
        log2N = 0;
        return 0;
    }else{
        for(log2N = 0; n % 2 == 0; n /= 2, log2N++);
        return n;
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Position convolution kernel center at (0, 0) in the image
////////////////////////////////////////////////////////////////////////////////
void Utility::ConvolutionFFT2D::padKernel(
	float *d_Dst,
	float *d_Src,
	int fftH,
	int fftW,
	int kernelH,
	int kernelW,
	int kernelY,
	int kernelX
	){
		assert(d_Src != d_Dst);
		dim3 threads(32, 8);
		dim3 grid(iDivUp(kernelW, threads.x), iDivUp(kernelH, threads.y));

		SET_FLOAT_BASE;
		padKernel_kernel<<<grid, threads>>>(
			d_Dst,
			d_Src,
			fftH,
			fftW,
			kernelH,
			kernelW,
			kernelY,
			kernelX
			);
		cutilCheckMsg("padKernel_kernel<<<>>> execution failed\n");
}



////////////////////////////////////////////////////////////////////////////////
// Prepare data for "pad to border" addressing mode
////////////////////////////////////////////////////////////////////////////////
void Utility::ConvolutionFFT2D::padDataClampToBorder(
	float *d_Dst,
	float *d_Src,
	int fftH,
	int fftW,
	int dataH,
	int dataW,
	int kernelW,
	int kernelH,
	int kernelY,
	int kernelX
	){
		assert(d_Src != d_Dst);
		dim3 threads(32, 8);
		dim3 grid(iDivUp(fftW, threads.x), iDivUp(fftH, threads.y));

		SET_FLOAT_BASE;
		padDataClampToBorder_kernel<<<grid, threads>>>(
			d_Dst,
			d_Src,
			fftH,
			fftW,
			dataH,
			dataW,
			kernelH,
			kernelW,
			kernelY,
			kernelX
			);
		cutilCheckMsg("padDataClampToBorder_kernel<<<>>> execution failed\n");
}



////////////////////////////////////////////////////////////////////////////////
// Modulate Fourier image of padded data by Fourier image of padded kernel
// and normalize by FFT size
////////////////////////////////////////////////////////////////////////////////
void Utility::ConvolutionFFT2D::modulateAndNormalize(
	fComplex *d_Dst,
	fComplex *d_Src,
	int fftH,
	int fftW,
	int padding
	){
		assert( fftW % 2 == 0 );
		const int dataSize = fftH * (fftW / 2 + padding);

		modulateAndNormalize_kernel<<<iDivUp(dataSize, 256), 256>>>(
			d_Dst,
			d_Src,
			dataSize,
			1.0f / (float)(fftW * fftH)
			);
		cutilCheckMsg("modulateAndNormalize() execution failed\n");
}

////////////////////////////////////////////////////////////////////////////////
// Combined spPostprocess2D + modulateAndNormalize + spPreprocess2D*
////////////////////////////////////////////////////////////////////////////////
void Utility::ConvolutionFFT2D::spProcess2D(
	void *d_Dst,
	void *d_SrcA,
	void *d_SrcB,
	uint DY,
	uint DX,
	int dir
	){
		assert( DY % 2 == 0 );

#if(POWER_OF_TWO)
		uint log2DX, log2DY;
		uint factorizationRemX = factorRadix2(log2DX, DX);
		uint factorizationRemY = factorRadix2(log2DY, DY);
		assert( factorizationRemX == 1 && factorizationRemY == 1 );
#endif

		const uint threadCount = (DY / 2) * DX;
		const double phaseBase = dir * PI / (double)DX;

		SET_FCOMPLEX_BASE_A;
		SET_FCOMPLEX_BASE_B;
		spProcess2D_kernel<<<iDivUp(threadCount, BLOCKDIM), BLOCKDIM>>>(
			(fComplex *)d_Dst,
			(fComplex *)d_SrcA,
			(fComplex *)d_SrcB,
			DY, DX, threadCount,
			(float)phaseBase,
			0.5f / (float)(DY * DX)
			);
		cutilCheckMsg("spProcess2D_kernel<<<>>> execution failed\n");
}

void Utility::ConvolutionFFT2D::GaussianFilter( 
	float *kernel
	, siz_t kernel_size
	, float sigma
	)
{	
	int siz = ( kernel_size.w - 1) / 2;
	float std   = sigma;

	float sumh = 0.0f;
	float max = 0.0f;
	double temp = EPS * ( double) max;	

	int i , j, index;	

	float *X = ( float *) malloc ( sizeof ( float) * kernel_size.w*kernel_size.h);
	float *Y = ( float *) malloc ( sizeof ( float) * kernel_size.w*kernel_size.h);

	for ( i = 0;i < kernel_size.h;i++) {
		for ( j = 0;j < kernel_size.w;j++) {

			index = i*kernel_size.w + j;

			X[index] = ( j - siz) * 1.0f;
			Y[index] = ( i - siz) * 1.0f;					
		}
	}

	for ( i = 0;i < kernel_size.w*kernel_size.h;i++) {	

		kernel[i] = exp( -( ( ( X[i] * X[i]) + ( Y[i] * Y[i])) / ( 2 * std * std)));					

		if ( max < kernel[i]) {

			max = kernel[i];
		}
	}

	for ( i = 0;i < kernel_size.w*kernel_size.h;i++) {		

		if ( kernel[i] < temp){

			kernel[i] = 0;
		}
	}

	for ( i = 0;i < kernel_size.w*kernel_size.h;i++) {

		sumh += kernel[i];		
	}

	if ( sumh != 0)
	{
		for ( i = 0;i < kernel_size.w;i++) {

			kernel[i] /= sumh;		
		}	
	}

	free( X);
	free( Y);
}

int Utility::ConvolutionFFT2D::snapTransformSize(int dataSize){
	int hiBit;
	unsigned int lowPOT, hiPOT;

	dataSize = iAlignUp(dataSize, 16);

	for(hiBit = 31; hiBit >= 0; hiBit--)
		if(dataSize & (1U << hiBit)) break;

	lowPOT = 1U << hiBit;
	if(lowPOT == (unsigned int)dataSize)
		return dataSize;

	hiPOT = 1U << (hiBit + 1);
	if(hiPOT <= 1024)
		return hiPOT;
	else 
		return iAlignUp(dataSize, 512);
}

void Utility::ConvolutionFFT2D::Init()
{
	padded_size.h = snapTransformSize(im_size.h + kernel_size.h - 1);
	padded_size.w = snapTransformSize(im_size.w + kernel_size.w - 1);

	FFT_SIZE	  = padded_size.h *   padded_size.w		 * sizeof(float);
	SPECTRUM_SIZE = padded_size.h * ( padded_size.w / 2) * sizeof(fComplex);

	cutilSafeCall( cudaMalloc( (void **)&d_PaddedData,   FFT_SIZE) );
	cutilSafeCall( cudaMalloc( (void **)&d_PaddedKernel, FFT_SIZE) );

	cutilSafeCall( cudaMalloc( (void **)&d_DataSpectrum0,   SPECTRUM_SIZE) );
	cutilSafeCall( cudaMalloc( (void **)&d_KernelSpectrum0, SPECTRUM_SIZE) );

		cufftSafeCall( cufftPlan2d(&fftPlan, padded_size.h, padded_size.w / 2, CUFFT_C2C) );
}

void Utility::ConvolutionFFT2D::Convolve( 
	float *d_odata
	, float *d_idata
	, float *d_kernel						 
	, siz_t im_size
	, siz_t kernel_size									
	, point_t kernel_center
	)
{
	cutilSafeCall( cudaMemset( d_PaddedData,   0, FFT_SIZE) );
	cutilSafeCall( cudaMemset( d_PaddedKernel, 0, FFT_SIZE) );

	padDataClampToBorder(
		d_PaddedData
		, d_idata
		, padded_size.h
		, padded_size.w
		, im_size.h
		, im_size.w
		, kernel_size.h
		, kernel_size.w
		, kernel_center.y
		, kernel_center.x
		);

	padKernel(
		d_PaddedKernel
		, d_kernel
		, padded_size.h
		, padded_size.w
		, kernel_size.h
		, kernel_size.w
		, kernel_center.y
		, kernel_center.x
		);

	//CUFFT_INVERSE works just as well...
	const int FFT_DIR = CUFFT_FORWARD;

	//Not including kernel transformation into time measurement,
	//since convolution kernel is not changed very frequently	
	cufftSafeCall( cufftExecC2C(fftPlan, (cufftComplex *)d_PaddedKernel, (cufftComplex *)d_KernelSpectrum0, FFT_DIR) );	

	cutilSafeCall( cutilDeviceSynchronize() );

	cufftSafeCall( cufftExecC2C(fftPlan, (cufftComplex *)d_PaddedData, (cufftComplex *)d_DataSpectrum0,  FFT_DIR) );
	spProcess2D( d_DataSpectrum0, d_DataSpectrum0, d_KernelSpectrum0, padded_size.h, padded_size.w / 2,  FFT_DIR);
	cufftSafeCall( cufftExecC2C(fftPlan, (cufftComplex *)d_DataSpectrum0, (cufftComplex *)d_PaddedData, -FFT_DIR) );

	cutilSafeCall( cutilDeviceSynchronize() );

	dim3 threads(16, 16);
	dim3 grid(iDivUp(im_size.w, threads.x), iDivUp(im_size.h, threads.y));	

	StripOutPadding<<< grid, threads, 0 >>>( d_odata, d_PaddedData, im_size, padded_size);
}

void Utility::ConvolutionFFT2D::Clean()
{
	cufftSafeCall( cufftDestroy(fftPlan) );

	cutilSafeCall( cudaFree(d_KernelSpectrum0) );
	cutilSafeCall( cudaFree(d_DataSpectrum0) );

	cutilSafeCall( cudaFree(d_PaddedData) );
	cutilSafeCall( cudaFree(d_PaddedKernel) );
}

void Utility::ConvolutionFFT2D::convolutionClampToBorderCPU(
    float *h_Result,
    float *h_Data,
    float *h_Kernel,
    int dataH,
    int dataW,
    int kernelH,
    int kernelW,
    int kernelY,
    int kernelX
){
    for(int y = 0; y < dataH; y++)
        for(int x = 0; x < dataW; x++){
            double sum = 0;

            for(int ky = -(kernelH - kernelY - 1); ky <= kernelY; ky++)
                for(int kx = -(kernelW - kernelX - 1); kx <= kernelX; kx++){
                    int dy = y + ky;
                    int dx = x + kx;
                    if(dy < 0) dy = 0;
                    if(dx < 0) dx = 0;
                    if(dy >= dataH) dy = dataH - 1;
                    if(dx >= dataW) dx = dataW - 1;

                    sum += h_Data[dy * dataW + dx] * h_Kernel[(kernelY - ky) * kernelW + (kernelX - kx)];
                }

            h_Result[y * dataW + x] = (float)sum;
        }
}


////////////////////////////////////////////////////////////////////////////////
/// Position convolution kernel center at (0, 0) in the image
////////////////////////////////////////////////////////////////////////////////
__global__ void Utility::padKernel_kernel(
    float *d_Dst,
    float *d_Src,
    int fftH,
    int fftW,
    int kernelH,
    int kernelW,
    int kernelY,
    int kernelX
){
    const int y = blockDim.y * blockIdx.y + threadIdx.y;
    const int x = blockDim.x * blockIdx.x + threadIdx.x;

    if(y < kernelH && x < kernelW){
        int ky = y - kernelY; if(ky < 0) ky += fftH;
        int kx = x - kernelX; if(kx < 0) kx += fftW;
        d_Dst[ky * fftW + kx] = LOAD_FLOAT(y * kernelW + x);
    }
}



////////////////////////////////////////////////////////////////////////////////
// Prepare data for "pad to border" addressing mode
////////////////////////////////////////////////////////////////////////////////
__global__ void Utility::padDataClampToBorder_kernel(
    float *d_Dst,
    float *d_Src,
    int fftH,
    int fftW,
    int dataH,
    int dataW,
    int kernelH,
    int kernelW,
    int kernelY,
    int kernelX
){
    const int y = blockDim.y * blockIdx.y + threadIdx.y;
    const int x = blockDim.x * blockIdx.x + threadIdx.x;
    const int borderH = dataH + kernelY;
    const int borderW = dataW + kernelX;

    if(y < fftH && x < fftW){
        int dy, dx;

        if(y < dataH) dy = y;
        if(x < dataW) dx = x;
        if(y >= dataH && y < borderH) dy = dataH - 1;
        if(x >= dataW && x < borderW) dx = dataW - 1;
        if(y >= borderH) dy = 0;
        if(x >= borderW) dx = 0;

        d_Dst[y * fftW + x] = LOAD_FLOAT(dy * dataW + dx);
    }
}



////////////////////////////////////////////////////////////////////////////////
// Modulate Fourier image of padded data by Fourier image of padded kernel
// and normalize by FFT size
////////////////////////////////////////////////////////////////////////////////
inline __device__ void Utility::mulAndScale(fComplex& a, const fComplex& b, const float& c){
    fComplex t = {c * (a.x * b.x - a.y * b.y), c * (a.y * b.x + a.x * b.y)};
    a = t;
}

__global__ void Utility::modulateAndNormalize_kernel(
    fComplex *d_Dst,
    fComplex *d_Src,
    int dataSize,
    float c
){
    const int i = blockDim.x * blockIdx.x + threadIdx.x;
    if(i >= dataSize)
        return;

    fComplex a = d_Src[i];
    fComplex b = d_Dst[i];

    mulAndScale(a, b, c);

    d_Dst[i] = a;
}

////////////////////////////////////////////////////////////////////////////////
// 2D R2C / C2R post/preprocessing kernels
////////////////////////////////////////////////////////////////////////////////
inline __device__ void Utility::spPostprocessC2C(fComplex& D1, fComplex& D2, const fComplex& twiddle){
    float A1 = 0.5f * (D1.x + D2.x);
    float B1 = 0.5f * (D1.y - D2.y);
    float A2 = 0.5f * (D1.y + D2.y);
    float B2 = 0.5f * (D1.x - D2.x);

    D1.x = A1 + (A2 * twiddle.x + B2 * twiddle.y);
    D1.y = (A2 * twiddle.y - B2 * twiddle.x) + B1;
    D2.x = A1 - (A2 * twiddle.x + B2 * twiddle.y);
    D2.y = (A2 * twiddle.y - B2 * twiddle.x) - B1;
}

//Premultiply by 2 to account for 1.0 / (DZ * DY * DX) normalization
inline __device__ void Utility::spPreprocessC2C(fComplex& D1, fComplex& D2, const fComplex& twiddle){
    float A1 = /* 0.5f * */ (D1.x + D2.x);
    float B1 = /* 0.5f * */ (D1.y - D2.y);
    float A2 = /* 0.5f * */ (D1.y + D2.y);
    float B2 = /* 0.5f * */ (D1.x - D2.x);

    D1.x = A1 - (A2 * twiddle.x - B2 * twiddle.y);
    D1.y = (B2 * twiddle.x + A2 * twiddle.y) + B1;
    D2.x = A1 + (A2 * twiddle.x - B2 * twiddle.y);
    D2.y = (B2 * twiddle.x + A2 * twiddle.y) - B1;
}

inline __device__ void Utility::getTwiddle(fComplex& twiddle, float phase){
    __sincosf(phase, &twiddle.y, &twiddle.x);
}

inline __device__ uint Utility::mod(uint a, uint DA){
    //(DA - a) % DA, assuming a <= DA
    return a ? (DA - a) : a;
}

inline __device__ void Utility::udivmod(uint& dividend, uint divisor, uint& rem){
    #if(!POWER_OF_TWO)
        rem = dividend % divisor;
        dividend /= divisor;
    #else
        rem = dividend & (divisor - 1);
        dividend >>= (__ffs(divisor) - 1);
    #endif
}


////////////////////////////////////////////////////////////////////////////////
// Combined spPostprocess2D + modulateAndNormalize + spPreprocess2D*
////////////////////////////////////////////////////////////////////////////////
__global__ void Utility::spProcess2D_kernel(
    fComplex *d_Dst,
    fComplex *d_SrcA,
    fComplex *d_SrcB,
    uint DY,
    uint DX,
    uint threadCount,
    float phaseBase,
    float c
){
    const uint threadId = blockIdx.x * blockDim.x + threadIdx.x;
    if(threadId >= threadCount)
        return;

    uint x, y, i = threadId;
    udivmod(i, DX, x);
    udivmod(i, DY / 2, y);

    const uint offset = i * DY * DX;

    //Avoid overwrites in rows 0 and DY / 2 by different threads (left and right halves)
    //Otherwise correctness for in-place transformations is affected
    if( (y == 0) && (x > DX / 2) )
        return;

    fComplex twiddle;

    //Process y = [0 .. DY / 2 - 1] U [DY - (DY / 2) + 1 .. DY - 1]
    {
        const uint pos1 = offset +          y * DX +          x;
        const uint pos2 = offset + mod(y, DY) * DX + mod(x, DX);

        fComplex D1 = LOAD_FCOMPLEX_A(pos1);
        fComplex D2 = LOAD_FCOMPLEX_A(pos2);
        fComplex K1 = LOAD_FCOMPLEX_B(pos1);
        fComplex K2 = LOAD_FCOMPLEX_B(pos2);
        getTwiddle(twiddle, phaseBase * (float)x);

        spPostprocessC2C(D1, D2, twiddle);
        spPostprocessC2C(K1, K2, twiddle);
        mulAndScale(D1, K1, c);
        mulAndScale(D2, K2, c);
		spPreprocessC2C(D1, D2, twiddle);

        d_Dst[pos1] = D1;
        d_Dst[pos2] = D2;
    }

    if(y == 0){
        const uint pos1 = offset + (DY / 2) * DX +          x;
        const uint pos2 = offset + (DY / 2) * DX + mod(x, DX);

        fComplex D1 = LOAD_FCOMPLEX_A(pos1);
        fComplex D2 = LOAD_FCOMPLEX_A(pos2);
        fComplex K1 = LOAD_FCOMPLEX_B(pos1);
        fComplex K2 = LOAD_FCOMPLEX_B(pos2);

        spPostprocessC2C(D1, D2, twiddle);
        spPostprocessC2C(K1, K2, twiddle);
        mulAndScale(D1, K1, c);
        mulAndScale(D2, K2, c);
        spPreprocessC2C(D1, D2, twiddle);

        d_Dst[pos1] = D1;
        d_Dst[pos2] = D2;
    }
}


__global__ void Utility::StripOutPadding( 
								float *out
								, float *in
								, siz_t im_size
								, siz_t fft_size
								)
{
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

	if( x>=im_size.w || y>=im_size.h) return;

	out[x + y*im_size.w] = in[x + y*fft_size.w];
}