
// includes
#include "error.hpp"
//#include "UTY_Convolution.hpp"
//#include "DYN_GaussianRecursive.hpp"
#include "DYN_Retina.hpp"
/*
#define KERNEL_PHOTORECEPTOR 15 // kernel size for photoreceptor cells
#define KERNEL_HORIZENTAL 21 // kernel size for horizental cells
*/
void Dynamic::Retina::GaussianFilter( 
float *kernel
, siz_t kernel_size
, float sigma
)
{	
	int i , j, m;	

	float X, Y;

	int X0 = ( kernel_size.w - 1) / 2;
	int Y0 = ( kernel_size.h - 1) / 2;

	float sigmasquaredtwo = 2 * sigma*sigma;

	for ( i = 0 ; i < kernel_size.h ; ++i) {
		for ( j = 0 ; j < kernel_size.w ; ++j) {

			m = i*kernel_size.w + j;

			X = (float)( j - X0);
			Y = (float)( i - Y0);

			kernel[m] = exp( -(
			powf( X*X, 2.0f) / sigmasquaredtwo
			+ powf( Y*Y, 2.0f) / sigmasquaredtwo
			));				
		}
	}
}

void Dynamic::Retina::Rotate90(
float *out
, float *in 
, siz_t im_size
)
{
	for ( int i = 0;i < im_size.h;i++) {
		for ( int j = 0;j < im_size.w;j++) {

			out[i*im_size.w + j] = in[j*im_size.h + i];
		}
	}
}
/*
void Dynamic::Retina::LowPassFilter( 
float *out
, float *in
, float *kernel 
, siz_t im_size
, siz_t kernel_size
)
{
	Rotate90( sup, kernel , kernel_size);
	Rotate90( kernel, sup , kernel_size);
	
	CUDA_CHECK( cudaMemcpy( d_kernel, kernel, kernel_size.w*kernel_size.h * sizeof( float), cudaMemcpyHostToDevice));

	oConvolutionFFT2D.Convolve(
	out
	, in
	, d_kernel
	, im_size
	, kernel_size
	, point_t(kernel_size.w/2, kernel_size.h/2)
	);
}
*/
void Dynamic::Retina::RetinaFilter( 
float *out
, float *in
, siz_t im_size
, int beta
)
{
	/*******************************************************************************
	Photoreceptor Cells
	*******************************************************************************/
/*
	float sigma = 1.0f / ( 2.0f * _PI * 0.5f);

	GaussianFilter( h_photoreceptor, kernel_photoreceptor_, sigma);

	LowPassFilter( d_photoreceptor, in, h_photoreceptor, im_size, kernel_photoreceptor_);
*/
	oGaussianRecursive.Apply( d_photoreceptor, in, im_size, 1, 0.60f);

	/*******************************************************************************
	* Horizental Cells
	*******************************************************************************/
/*
	oGaussianRecursive.Apply( d_horizental, d_photoreceptor, im_size, 1, 0.80);	
	oGaussianRecursive.Apply( d_horizental, d_horizental, im_size, 1, 0.94);
*/

	oGaussianRecursive.Apply( d_horizental, d_photoreceptor, im_size, 1, 1.05f);

	/*******************************************************************************
	* BipolarCells
	*******************************************************************************/

	dim3 dimBlock( 16, 16, 1);
	dim3 dimGrid( im_size.w / dimBlock.x, im_size.h / dimBlock.y, 1);

	Bipolar<<< dimGrid, dimBlock, 0 >>>( out , d_photoreceptor , d_horizental , im_size , beta);
	CUDA_CHECK( cudaDeviceSynchronize());

	oGaussianRecursive.Apply( out, out, im_size, 1, 0.80f);	
}

__global__ void Dynamic::Bipolar( 
float *out
, float *Y 
, float *H
, siz_t im_size
, int beta
)
{
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
/*
	float tempON, tempOFF;

	tempON  = Y[y*im_size.w + x] - beta*H[y*im_size.w + x];
	tempOFF = beta * H[y*im_size.w + x] - Y[y*im_size.w + x];

	out[y*im_size.w + x] = ( ( tempON < 0.0f) ? 0.0f : tempON) - ( ( tempOFF < 0.0f) ? 0.0f : tempOFF);
*/

	//TODO::verify the rescaling
	out[y*im_size.w + x] = Y[y*im_size.w + x] - H[y*im_size.w + x];
	//out[y*im_size.w + x] = (Y[y*im_size.w + x] - H[y*im_size.w + x]) * 255.0f;
}

void Dynamic::Retina::Init() 
{
	CUDA_CHECK( cudaMalloc( ( void**)&d_photoreceptor, im_size_.w*im_size_.h * sizeof( float)));
	CUDA_CHECK( cudaMalloc( ( void**)&d_horizental	 , im_size_.w*im_size_.h * sizeof( float)));

	CUDA_CHECK( cudaMalloc( ( void**)&d_kernel, kernel_photoreceptor_.w*kernel_photoreceptor_.h * sizeof( float)));

	h_photoreceptor = ( float *) malloc( kernel_photoreceptor_.w*kernel_photoreceptor_.h * sizeof( float));

	sup = ( float *) malloc( kernel_photoreceptor_.w*kernel_photoreceptor_.h * sizeof( float));
}

void Dynamic::Retina::Clean() 
{
	d_photoreceptor = NULL; CUDA_CHECK( cudaFree( d_photoreceptor));
	d_horizental = NULL; CUDA_CHECK( cudaFree( d_horizental));

	d_kernel = NULL; CUDA_CHECK( cudaFree( d_kernel));

	free( h_photoreceptor);

	free( sup);
}

