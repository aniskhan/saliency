/*
* This sample demonstrates how 2D convolutions 
* with very large kernel sizes 
* can be efficiently implemented 
* using FFT transformations.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include <shrUtils.h>
//#include <shrQATest.h>

#include "cufft.h"

#include "error.hpp"

#include "UTY_Convolution.hpp"

//#define VERBOSE

texture<float, 2, cudaReadModeElementType> texKernel;
texture<float, 2, cudaReadModeElementType> texData;

/**
Data configuration
*/

int UTY_Convolution::calculateFFTsize( int dataSize){
	//Highest non-zero bit position of dataSize
	int hiBit;
	//Neares lower and higher powers of two numbers for dataSize
	unsigned int lowPOT, hiPOT;

	//Align data size to im multiple of half-warp
	//in order to have each line starting at properly aligned addresses
	//for coalesced global memory writes in padKernel() and padData()
	dataSize = iAlignUp( dataSize, 16);

	//Find highest non-zero bit
	for( hiBit = 31;hiBit >= 0;hiBit--)
		if( dataSize &( 1U << hiBit)) break;

	//No need to align, if already power of two
	lowPOT = 1U << hiBit;
	if( lowPOT == dataSize) return dataSize;

	//Align to im nearest higher power of two, if the size is small enough, 
	//else align only to im nearest higher multiple of 512, 
	//in order to save computation and memory bandwidth
	hiPOT = 1U <<( hiBit + 1);
	//if( hiPOT <= 1024)
	return hiPOT;
	//else 
	//	return iAlignUp( dataSize, 512);
}

/**
FFT convolution program
*/

void UTY_Convolution::UTY_convolve_2d( float *out, float *in, float *kernel, siz_t im_size, siz_t kernel_size)
{
	int FFT_W, FFT_H, PADDING_H, PADDING_W, KERNEL_X, KERNEL_Y, FFT_SIZE, KERNEL_SIZE, DATA_SIZE, CFFT_SIZE;

	PADDING_W = kernel_size.w - 1;
	PADDING_H = kernel_size.h - 1;

	KERNEL_X = kernel_size.w/2;
	KERNEL_Y = kernel_size.h/2;

	FFT_W = calculateFFTsize( im_size.w + PADDING_W);
	FFT_H = calculateFFTsize( im_size.h + PADDING_H);

	FFT_SIZE = FFT_W * FFT_H * sizeof( float);
	CFFT_SIZE = FFT_W * FFT_H * sizeof( complex_t);
	KERNEL_SIZE = kernel_size.w * kernel_size.h * sizeof( float);
	DATA_SIZE = im_size.w * im_size.h * sizeof( float);

	float *h_Result =( float *)malloc( FFT_SIZE);

	cudaArray *a_Kernel, *a_Data;
	cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc( 32, 0, 0, 0, cudaChannelFormatKindFloat);

	float *d_PaddedKernel, *d_PaddedData;
	complex_t *fft_PaddedKernel, *fft_PaddedData;

	cufftHandle FFTplan_R2C;
	cufftHandle FFTplan_C2R;

	cudaMallocArray( &a_Kernel, &channelDesc, kernel_size.w, kernel_size.h);
	cudaMallocArray( &a_Data, &channelDesc, im_size.w, im_size.h);
	cudaMalloc( ( void **)&d_PaddedKernel, 	FFT_SIZE);
	cudaMalloc( ( void **)&d_PaddedData, 	FFT_SIZE);
	cudaMalloc( ( void **)&fft_PaddedKernel, 	CFFT_SIZE);
	cudaMalloc( ( void **)&fft_PaddedData, 	CFFT_SIZE);

	cufftPlan2d( &FFTplan_C2R, FFT_H, FFT_W, CUFFT_C2R);
	cufftPlan2d( &FFTplan_R2C, FFT_H, FFT_W, CUFFT_R2C);

	cudaMemset( d_PaddedKernel, 0, FFT_SIZE);
	cudaMemset( d_PaddedData, 0, FFT_SIZE);

	cudaMemcpyToArray( a_Kernel, 0, 0, kernel, KERNEL_SIZE, cudaMemcpyHostToDevice);
	cudaMemcpyToArray( a_Data, 0, 0, in, DATA_SIZE, cudaMemcpyDeviceToDevice);

	cudaBindTextureToArray( texKernel, a_Kernel, channelDesc);
	cudaBindTextureToArray( texData, a_Data, channelDesc);

	//Block width should be im multiple of maximum coalesced write size 
	//for coalesced memory writes in padKernel() and padData()
	dim3 threadBlock( 16, 12);
	dim3 kernelBlockGrid( iDivUp( kernel_size.w, threadBlock.x), iDivUp( kernel_size.h, threadBlock.y));
	dim3 dataBlockGrid( iDivUp( FFT_W, threadBlock.x), iDivUp( FFT_H, threadBlock.y));
		
	padKernel<<<kernelBlockGrid, threadBlock>>>( 
		d_PaddedKernel, 
		FFT_W, 
		FFT_H, 
		kernel_size.w, 
		kernel_size.h, 
		KERNEL_X, 
		KERNEL_Y
		);
	CUDA_CHECK( cudaDeviceSynchronize());
		
	padData<<<dataBlockGrid, threadBlock>>>( 
		d_PaddedData, 
		FFT_W, 
		FFT_H, 
		im_size.w, 
		im_size.h, 
		kernel_size.w, 
		kernel_size.h, 
		KERNEL_X, 
		KERNEL_Y
		);
	CUDA_CHECK( cudaDeviceSynchronize());
		
	cufftExecR2C( FFTplan_R2C, ( cufftReal *)d_PaddedKernel, ( cufftComplex *)fft_PaddedKernel);
	cufftExecR2C( FFTplan_R2C, ( cufftReal *)d_PaddedData, ( cufftComplex *)fft_PaddedData);

	CUDA_CHECK( cudaDeviceSynchronize());
		
	modulateAndNormalize<<<16, 128>>>( 
		fft_PaddedData, 
		fft_PaddedKernel, 
		FFT_W * FFT_H
		);
	CUDA_CHECK( cudaDeviceSynchronize());
		
	cufftExecC2R( FFTplan_C2R, ( cufftComplex *)fft_PaddedData, ( cufftReal *)d_PaddedData);

	CUDA_CHECK( cudaDeviceSynchronize());

	/*******************************************************************************
	* StripOutPadding
	*/
	dim3 dimBlock( 16, 16, 1);	
	dim3 dimGrid( iDivUp(im_size.w, dimBlock.x), iDivUp(im_size.h, dimBlock.y), 1);

	StripOutPadding<<< dimGrid, dimBlock, 0 >>>( out, d_PaddedData, im_size, siz_t(FFT_W, FFT_H));	
CUDA_CHECK( cudaDeviceSynchronize());

	CUDA_CHECK( cudaUnbindTexture( texData));
	CUDA_CHECK( cudaUnbindTexture( texKernel));

	cufftDestroy( FFTplan_C2R);
	cufftDestroy( FFTplan_R2C);

	CUDA_CHECK( cudaFree( d_PaddedData));
	CUDA_CHECK( cudaFree( d_PaddedKernel));

	CUDA_CHECK( cudaFree( fft_PaddedData));
	CUDA_CHECK( cudaFree( fft_PaddedKernel));

	a_Data = NULL; CUDA_CHECK( cudaFreeArray( a_Data));
	a_Data = NULL; CUDA_CHECK( cudaFreeArray( a_Kernel));

	free( h_Result);
}

/**
Cyclically shift convolution kernel, so that the center is at( 0, 0)
*/

__global__ void padKernel( 
						  float *d_PaddedKernel, 
						  int fftW, 
						  int fftH, 
						  int kernelW, 
						  int kernelH, 
						  int kernelX, 
						  int kernelY
						  ){
							  const int x = IMUL( blockDim.x, blockIdx.x) + threadIdx.x;
							  const int y = IMUL( blockDim.y, blockIdx.y) + threadIdx.y;

							  if( x < kernelW && y < kernelH){
								  int kx = x - kernelX;if( kx < 0) kx += fftW;
								  int ky = y - kernelY;if( ky < 0) ky += fftH;
								  d_PaddedKernel[IMUL( ky, fftW) + kx] =
									  tex2D( texKernel, ( float)x + 0.5f, ( float)y + 0.5f);
							  }
}

/**
Copy input data array to the upper left corner and pad by border values
*/

__global__ void padData( 
						float *d_PaddedData, 
						int fftW, 
						int fftH, 
						int dataW, 
						int dataH, 
						int kernelW, 
						int kernelH, 
						int kernelX, 
						int kernelY
						){
							const int x = IMUL( blockDim.x, blockIdx.x) + threadIdx.x;
							const int y = IMUL( blockDim.y, blockIdx.y) + threadIdx.y;
							const int borderW = dataW + kernelX;
							const int borderH = dataH + kernelY;
							int dx;
							int dy;

							if( x < fftW && y < fftH){
								if( x < dataW) dx = x;
								if( y < dataH) dy = y;
								if( x >= dataW && x < borderW) dx = dataW - 1;
								if( y >= dataH && y < borderH) dy = dataH - 1;
								if( x >= borderW) dx = 0;
								if( y >= borderH) dy = 0;

								d_PaddedData[IMUL( y, fftW) + x] =
									tex2D( texData, ( float)dx + 0.5f, ( float)dy + 0.5f);
							}
}

/**
Modulate Fourier image of padded data by Fourier image of padded kernel
* and normalize by FFT size
*/
__device__ void complexMulAndScale( complex_t& im, complex_t b, float c){
	im[0]= c *( im[0] * b[0] - im[1] * b[1]);
	im[1]= c *( im[1] * b[0] + im[0] * b[1]);
}

__global__ void modulateAndNormalize( complex_t *fft_PaddedData, complex_t *fft_PaddedKernel, int dataN)
{
	const int     tid = IMUL( blockDim.x, blockIdx.x) + threadIdx.x;
	const int threadN = IMUL( blockDim.x, gridDim.x);
	const float     q = 1.0f /( float)dataN;

	for( int i = tid;i < dataN;i += threadN)
		complexMulAndScale( fft_PaddedData[i], fft_PaddedKernel[i], q);
}

/**
StripOutPadding Kernel
*/

__global__ void StripOutPadding( 
								float *out
								, float *in
								, siz_t im_size
								, siz_t fft_size
								)
{
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

	if( x<im_size.w && y<im_size.h) 
		out[x + y*im_size.w] = in[x + y*fft_size.w];
}