
// includes
#include "struct.hpp"
#include "DYN_GaussianRecursive.hpp"
#include "DYN_Modulation.hpp"
#include "DYN_Gabor.hpp"

void Dynamic::Gabor::Apply(
float *out
, float *in
, float *mod
, siz_t level_size
, siz_t im_size
)
{	
	dim3 dimb( 16, 16, 1);
	dim3 dimg(
	( level_size.w/dimb.x)+1*( level_size.w%dimb.x!=0)
	, ( level_size.h/dimb.y)+1*( level_size.h%dimb.y!=0)
	, 1
	);

	oModulation.Apply( out, in, mod, level_size, im_size, CUMD_MO);

	oGaussianRecursive.Apply( out, out, level_size, 2*N, s0);	

	oModulation.Apply( out, out, mod, level_size, im_size, CUMD_DEM);
}
