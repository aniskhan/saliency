#include "DYN_Modulation.hpp"
#include "error.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void Dynamic::Modulation::Init( 
float *mod
, siz_t im_size
, float freq
)
{
	dim3 dimb( 16, 16, 1);
	dim3 dimg( 
	( im_size.w/dimb.x)   + 1*( im_size.w%dimb.x!=0)
	, ( im_size.h/dimb.y) + 1*( im_size.h%dimb.y!=0)
	, 1
	);

	Dynamic::KernelInitModulation<<<dimg, dimb>>>( mod, im_size, freq);
	CUDA_CHECK( cudaDeviceSynchronize());	
}

void Dynamic::Modulation::Apply(
float *out
, float *in
, float *mod
, siz_t level_size
, siz_t im_size
, ModulationType_t type
)
{		
	dim3 dimb( 16, 16, 1);
	dim3 dimg( 
	( level_size.w/dimb.x)   + 1*( level_size.w%dimb.x!=0)
	, ( level_size.h/dimb.y) + 1*( level_size.h%dimb.y!=0)
	, 1
	);

	if( type == CUMD_MO)
	{
		Dynamic::KernelModulation<<<dimg, dimb>>>( out, in, mod, level_size, im_size);
		CUDA_CHECK( cudaDeviceSynchronize());
	}
	else if(type == CUMD_DEM)
	{
		Dynamic::KernelDemodulation<<<dimg, dimb>>>( out, in, mod, level_size, im_size);		
		CUDA_CHECK( cudaDeviceSynchronize());
	}	
}

__global__ void Dynamic::KernelInitModulation( 
float *mod
, siz_t im_size
, float freq
)
{
	int 
	x, y
	, m, size;
	
	float teta, temp;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;

	if ( x>=im_size.w || y>=im_size.h) return;

	m = x + y*im_size.w;

	size = im_size.w * im_size.h;

	for( unsigned int n=0 ; n<N ; n++)
	{
		teta = ((n+1.0f) * _PI) / N;
		temp = 2.0f*_PI*freq * ( (y+1)*cosf(teta) + (x+1)*sinf(teta));

		mod[m+ n*size]		= cosf( temp);		
		mod[m + (n+N)*size]	= sinf( temp);
	}
}

__global__ void Dynamic::KernelModulation( 
float *out
, float *in
, float *mod
, siz_t level_size
, siz_t im_size
)
{
	unsigned int 
	x, y
	, m, l
	, m_size, l_size;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;
	
	if( x>=level_size.w || y>=level_size.h) return;

	l = x + y*level_size.w;
	m = x + y*im_size.w;

	l_size = level_size.w * level_size.h;
	m_size = 	im_size.w * im_size.h;

	for ( unsigned int n=0 ; n<2*N ; n++)
	{
		out[l + n*l_size] = mod[m + n*m_size] * in[l];
	}
}

__global__ void Dynamic::KernelDemodulation(
float *out
, float *in
, float *mod
, siz_t level_size
, siz_t im_size
)
{
	int 
	x, y
	, m, l
	, m_size, l_size;
	
	float 
	dr, di
	, mr, mi
	, norm;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;
	
	if ( x>=level_size.w || y>=level_size.h) return;

	l = x + y*level_size.w;
	m = x + y*im_size.w;

	l_size = level_size.w * level_size.h;
	m_size = im_size.w * im_size.h;

	for( unsigned int n=0 ; n<N ; n++)
	{
		dr = in[l + 	n*l_size];
		di = in[l + (n+N)*l_size];

		mr = mod[m + 	 n*m_size];
		mi = mod[m + (n+N)*m_size];

		norm = sqrtf( powf( dr, 2.0f) + powf( di, 2.0f));

		if( norm==0.0f) 
		{
			out[l +	    n*l_size] = 0.0f;
			out[l + (n+N)*l_size] = 0.0f;
		}
		else {
			out[l +     n*l_size] = (  dr*mr + di*mi) / norm;
			out[l + (n+N)*l_size] = ( -dr*mi + di*mr) / norm;
		}
	}
}
