
#include "error.hpp"
#include "DYN_GaussianRecursive.hpp"

#define SIMPLIFIED

void Dynamic::GaussianRecursive::Apply( 
									   float *out
									   , float *in
									   , siz_t im_size									   
									   , int order
									   , float sigma
									   )
{

	float 
		q, d1r, d1i, d3, b
		, B, b0, b1, b2, b3;

	dim3 dimbh( 1, 16, 1);
	dim3 dimgh( order, ( im_size.w/dimbh.x) + 1*( im_size.w%dimbh.x!=0), 1);
	dim3 dimbv( 16, 1, 1);
	dim3 dimgv( ( im_size.h/dimbv.y) + 1*( im_size.h%dimbv.y!=0), order, 1);

	q   = ( sigma / 2.1234f) + 0.0467f;
	d1r = powf( 1.7387f, 1/q) * cos( 0.61861f/q);
	d1i = powf( 1.7387f, 1/q) * sin( 0.61861f/q);
	d3  = powf( 1.8654f, 1/q);
	b   = 1 / ( d3 * ( d1r*d1r + d1i*d1i));

	b1 = -b * ( d1r*d1r + d1i*d1i + 2*d3*d1r);
	b2 =  b * ( 2*d1r + d3);
	b3 = -b;
	B  = 1 + b1 + b2 + b3;
	b0 = -1;

	// out is output
	KernelGaussianRecursiveCausal<<<dimgh, dimbh>>>( out, in, im_size, B, b0, b1, b2, b3);
	CUDA_CHECK( cudaDeviceSynchronize());

	// out is I/O
	KernelGaussianRecursiveAntiCausal<<<dimgv, dimbv>>>( out, out, im_size, B, b0, b1, b2, b3);
	CUDA_CHECK( cudaDeviceSynchronize());
}

void Dynamic::GaussianRecursive::Apply( 
									   float *out
									   , float *in
									   , float *temp
									   , siz_t im_size                                                                         
									   , int order
									   , float sigma
									   )
{
	float q, d1r, d1i, d3, b;
	float B, b0, b1, b2, b3;

	dim3 dimbh( 1, 16, 1);  
	dim3 dimgh( order, iDivUp( im_size.w,dimbh.x), 1);
	dim3 dimbv( 16, 1, 1);
	dim3 dimgv( iDivUp( im_size.h,dimbv.y), order, 1);

	q=( sigma/2.1234f)+0.0467f;
	d1r=powf( 1.7387f, 1/q)*cos( 0.61861f/q);
	d1i=powf( 1.7387f, 1/q)*sin( 0.61861f/q);
	d3=powf( 1.8654f, 1/q);
	b=1/( d3*( d1r*d1r+d1i*d1i));

	b1=-b*( d1r*d1r+d1i*d1i+2*d3*d1r);
	b2=b*( 2*d1r+d3);
	b3=-b;
	B=1+b1+b2+b3;
	b0=-1;

	KernelGaussianRecursiveCausal<<<dimgh, dimbh>>>( temp, in, im_size, B, b0, b1, b2, b3);
	CUDA_CHECK( cudaDeviceSynchronize());

	KernelGaussianRecursiveAntiCausal<<<dimgv, dimbv>>>( out, temp, im_size, B, b0, b1, b2, b3);
	CUDA_CHECK( cudaDeviceSynchronize());
}

__global__ void Dynamic::KernelGaussianRecursiveCausal(
	float *odat
	, float *idat
	, siz_t im_size
	, float B
	, float b0
	, float b1
	, float b2
	, float b3
	) 
{
	#if defined SIMPLIFIED

	int 
		x, y
		, n
		, size = im_size.w*im_size.h;

	y = threadIdx.y + blockIdx.y*blockDim.y;

	if( y >= im_size.h) return;

	n = blockIdx.x;

	idat += ( im_size.w*y + n*size);
	odat += ( im_size.w*y + n*size);

	// forward pass

	float yp1, yp2, yp3, xc, yc;

	yp3 = B*( *idat);
	yp2 = B*( *(idat + 1)) + ( b1/b0) * yp3;
	yp1 = B*( *(idat + 2)) + ( b1*yp2 + b2*yp3) / b0;

	for( x=3 ; x<10 ; x++){

		yc = B*( *(idat + x)) + ( b1*yp1 + b2*yp2 + b3*yp3) / b0;

		yp3 = yp2; yp2 = yp1; yp1 = yc;
	}

	for( x=0; x<im_size.w ; x++){

		xc = *idat;

		yc = B*xc + ( b1*yp1 + b2*yp2 + b3*yp3) / b0;

		yp3 = yp2; yp2 = yp1; yp1 = yc;
		*odat = yc;
		idat ++; odat ++;
	}

	// reset pointer

	idat --; odat --;

	// reverse pass

	yp3 = B*( *idat);
	yp2 = B*( *(idat - 1)) + ( b1/b0) * yp3;
	yp1 = B*( *(idat - 2)) + ( b1*yp2 + b2*yp3) / b0;

	for( x=3 ; x<10 ; x++){

		yc = B*( *(idat - x)) + ( b1*yp1 + b2*yp2 + b3*yp3) / b0;

		yp3 = yp2; yp2 = yp1; yp1 = yc;
	}

	for( x = im_size.w-1; x >= 0; x--) {

		xc = *idat;

		yc = B*xc + ( b1*yp1 + b2*yp2 + b3*yp3) / b0;

		yp3 = yp2; yp2 = yp1; yp1 = yc;
		*odat = yc;
		idat --; odat --;
	}

#else
	int 
		x, y
		, n, m, l
		, size = im_size.w*im_size.h;

	float
		tampon[10]
	, *in, *out;

	x = 0;
	y = threadIdx.y + blockIdx.y*blockDim.y;

	if( y >= im_size.h) return;

	m = im_size.w*y;
	n = blockIdx.x;

	in  = &idat[n*size];
	out = &odat[n*size];

	// Causal filtering
	tampon[0] = B*in[m];
	tampon[1] = B*in[m+1] + ( b1/b0) * tampon[0];
	tampon[2] = B*in[m+2] + ( b1*tampon[1] + b2*tampon[0]) / b0;

	for( l=3 ; l<10 ; l++)
		tampon[l] = B*in[m+l] + ( b1*tampon[l-1] + b2*tampon[l-2] + b3*tampon[l-3]) / b0;

	out[m]   = B*in[m]   + ( b1*tampon[9] + b2*tampon[8] + b3*tampon[7]) / b0;
	out[m+1] = B*in[m+1] + ( b1*out[m]    + b2*tampon[9] + b3*tampon[8]) / b0;
	out[m+2] = B*in[m+2] + ( b1*out[m+1]  + b2*out[m]    + b3*tampon[9]) / b0;

	for( x+=3, m+=3 ; x<im_size.w ; x++, m++)
		out[m] = B*in[m] + ( b1*out[m-1] + b2*out[m-2] + b3*out[m-3]) / b0;

	x--; m--;

	// Anticausal filtering
	tampon[9] = B*out[m];
	tampon[8] = B*out[m-1] + ( b1/b0) * tampon[9];
	tampon[7] = B*out[m-2] + ( b1*tampon[8] + b2*tampon[9]) / b0;

	for ( l=3 ; l<10 ; l++)
		tampon[9-l] = B*out[m-l] + ( b1*tampon[10-l] + b2*tampon[11-l] + b3*tampon[12-l]) / b0;

	out[m]   = B*out[m]   + ( b1*tampon[0] + b2*tampon[1] + b3*tampon[2]) / b0;
	out[m-1] = B*out[m-1] + ( b1*out[m]    + b2*tampon[0] + b3*tampon[1]) / b0;
	out[m-2] = B*out[m-2] + ( b1*out[m-1]  + b2*out[m]    + b3*tampon[0]) / b0;

	for( x-=3, m-=3 ; x>=0 ; x--, m--)
		out[m] = B*out[m] + ( b1*out[m+1] + b2*out[m+2] + b3*out[m+3]) / b0;
#endif
}

__global__ void Dynamic::KernelGaussianRecursiveAntiCausal( 
	float *odat
	, float *idat
	, siz_t im_size
	, float B
	, float b0
	, float b1
	, float b2
	, float b3
	)
{
	#if defined SIMPLIFIED
	int 
		x, y
		, n
		, size = im_size.w*im_size.h;

	x = threadIdx.x + blockIdx.x*blockDim.x;

	if( x>=im_size.w) return;

	n = blockIdx.y;

	idat += ( x + n*size);
	odat += ( x + n*size);

	// forward pass

	float yp1, yp2, yp3, xc, yc;

	yp3 = B*( *idat);
	yp2 = B*( *(idat + im_size.w)) + ( b1/b0) * yp3;
	yp1 = B*( *(idat + im_size.w)) + ( b1*yp2 + b2*yp3) / b0;

	for( y=3 ; y<10 ; y++){

		yc = B*( *(idat + y*im_size.w)) + ( b1*yp1 + b2*yp2 + b3*yp3) / b0;

		yp3 = yp2; yp2 = yp1; yp1 = yc;
	}

	for( y=0; y<im_size.h ; y++){

		xc = *idat;

		yc = B*xc + ( b1*yp1 + b2*yp2 + b3*yp3) / b0;

		yp3 = yp2; yp2 = yp1; yp1 = yc;
		*odat = yc;
		idat += im_size.w; odat += im_size.w;
	}

	// reset pointer

	idat -= im_size.w; odat -= im_size.w;

	// reverse pass

	yp3 = B*( *idat);
	yp2 = B*( *(idat - im_size.w)) + ( b1/b0) * yp3;
	yp1 = B*( *(idat - im_size.w)) + ( b1*yp2 + b2*yp3) / b0;

	for( y=3 ; y<10 ; y++){

		yc = B*( *(idat - y*im_size.w)) + ( b1*yp1 + b2*yp2 + b3*yp3) / b0;

		yp3 = yp2; yp2 = yp1; yp1 = yc;
	}

	for( y = im_size.h-1; y >= 0; y--) {

		xc = *idat;

		yc = B*xc + ( b1*yp1 + b2*yp2 + b3*yp3) / b0;

		yp3 = yp2; yp2 = yp1; yp1 = yc;
		*odat = yc;
		idat -= im_size.w; odat -= im_size.w;
	}

#else
	int 
		x, y
		, m, n, l
		, size = im_size.w*im_size.h;

	float
		tampon[10]
	, *in, *out;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = 0;

	if( x>=im_size.w) return;

	m = x;
	n = blockIdx.y;

	in  = &idat[n*size];
	out = &odat[n*size];

	// Causal Filtering

	tampon[0] = B*in[m];
	tampon[1] = B*in[m + im_size.w]   + ( b1/b0) * tampon[0];
	tampon[2] = B*in[m + 2*im_size.w] + ( b1*tampon[1] + b2*tampon[0]) / b0;

	for( l=3 ; l<10 ; l++)
		tampon[l] = B*in[m + l*im_size.w] + ( b1*tampon[l-1] + b2*tampon[l-2] + b3*tampon[l-3]) / b0;

	out[m] 				 = B*in[m] 				 + ( b1*tampon[9] 		   + b2*tampon[8] + b3*tampon[7]) / b0;
	out[m + im_size.w]   = B*in[m + im_size.w]   + ( b1*out[m] 			   + b2*tampon[9] + b3*tampon[8]) / b0;
	out[m + 2*im_size.w] = B*in[m + 2*im_size.w] + ( b1*out[m + im_size.w] + b2*out[m]    + b3*tampon[9]) / b0;

	for( y+=3, m+=3*im_size.w ; y<im_size.h ; y++, m+=im_size.w)
		out[m] = B*in[m] + ( b1*out[m - im_size.w] + b2*out[m - 2*im_size.w] + b3*out[m - 3*im_size.w]) / b0;

	y--; m-=im_size.w;

	// Anticausal filtering 

	tampon[9] = B*out[m];
	tampon[8] = B*out[m - im_size.w]   + ( b1/b0) * tampon[9];
	tampon[7] = B*out[m - 2*im_size.w] + ( b1*tampon[8] + b2*tampon[9]) / b0;

	for( l=3 ; l<10 ; l++)
		tampon[9-l] = B*out[m - l*im_size.w] + ( b1*tampon[10-l] + b2*tampon[11-l] + b3*tampon[12-l]) / b0;

	out[m] 				 = B*out[m] 			+ ( b1*tampon[0] 		  + b2*tampon[1] + b3*tampon[2]) / b0;
	out[m - im_size.w] 	 = B*out[m - im_size.w] + ( b1*out[m] 			  + b2*tampon[0] + b3*tampon[1]) / b0;
	out[m - 2*im_size.w] = B*out[m-2*im_size.w] + ( b1*out[m - im_size.w] + b2*out[m] 	 + b3*tampon[0]) / b0;

	for( y-=3, m-=3*im_size.w ; y>=0 ; y--, m-=im_size.w)
		out[m] = B*out[m] + ( b1*out[m + im_size.w] + b2*out[m + 2*im_size.w] + b3*out[m + 3*im_size.w]) / b0;

#endif
}