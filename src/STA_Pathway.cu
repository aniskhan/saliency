
// includes
#include "cufft.h"
#include "STA_Pathway.hpp"

#include "UTY_Debug.hpp"

#include <assert.h>

void Static::Pathway::SetData(
							  float* out
							  , float* in
							  ) 
{
	CUDA_CHECK( cudaMemcpy( out, in, size_ * sizeof( float), cudaMemcpyHostToDevice));
}

void Static::Pathway::GetData( 
							  float* out
							  , float* in
							  ) 
{
	CUDA_CHECK( cudaMemcpy( out, in, size_*sizeof( float), cudaMemcpyDeviceToHost));
}

void Static::Pathway::GetData( 
							  complex_t* out
							  , complex_t* in
							  )
{
	CUDA_CHECK( cudaMemcpy( out, in, size_*sizeof( complex_t), cudaMemcpyDeviceToHost));
}

void Static::Pathway::Init()
{
	CUDA_CHECK( cudaMalloc( ( void**)&d_idata, 	size_ * sizeof( float)));
	CUDA_CHECK( cudaMalloc( ( void**)&d_idata, 	size_ * sizeof( float)));

	CUDA_CHECK( cudaMalloc( ( void**)&d_temp, 	size_ * sizeof( float)));
	CUDA_CHECK( cudaMalloc( ( void**)&d_temp_scaled,size_scaled_ * sizeof( float)));

	CUDA_CHECK( cudaMalloc( ( void**)&d_data_freq, 	size_scaled_ * sizeof( complex_t)));
	CUDA_CHECK( cudaMalloc( ( void**)&d_data_spat, 	size_scaled_ * sizeof( complex_t)));

	CUDA_CHECK( cudaMalloc( ( void**)&d_odata, 	size_scaled_ * sizeof( float)));

	CUDA_CHECK( cudaMalloc( ( void**)&d_maps, 	NO_OF_ORIENTS*NO_OF_BANDS * size_scaled_ * sizeof( float)));
	CUDA_CHECK( cudaMalloc( ( void**)&d_maps_freq,	NO_OF_ORIENTS*NO_OF_BANDS * size_scaled_ * sizeof( complex_t)));	
	CUDA_CHECK( cudaMalloc( ( void**)&d_maps_spat,	NO_OF_ORIENTS*NO_OF_BANDS * size_scaled_ * sizeof( complex_t)));
}

void Static::Pathway::Apply(
							float *out
							, float *in
							, siz_t im_size_scaled
							, siz_t im_size
							)
{	
	assert(  in != NULL);
	assert( out != NULL);

	int INPUT_SIZE, SCALED_SIZE;

	INPUT_SIZE  = im_size.w*im_size.h * sizeof( float);
	SCALED_SIZE = im_size_scaled.w * im_size_scaled.h *sizeof(float);

	CUDA_CHECK( cudaMemcpy( d_idata, in, INPUT_SIZE, cudaMemcpyHostToDevice));

	//oRetina.RetinaFilter( d_data, im_size, 0, 0, 0);

	oMask.Apply( d_data_spat, d_idata, im_size);

#if defined VERBOSE
	Utility::write_image_device( "tmp/1_mask.png", d_data_spat, im_size);
#endif

	oTransform.Apply( d_data_freq, d_data_spat, im_size, CUFFT_FORWARD);

#if defined VERBOSE
	Utility::write_image_device( "tmp/2_fft.png", d_data_freq, im_size);
#endif

	oGabor.Apply( d_maps_freq, d_data_freq, im_size);

#if defined VERBOSE
	Utility::write_image_device( "tmp/3_gabor.png", d_maps_freq, im_size);
#endif

	oTransform.Apply( d_maps_spat, d_maps_freq, im_size, CUFFT_INVERSE);

#if defined VERBOSE
	Utility::write_image_device( "tmp/4_ifft.png", d_maps_spat, im_size);
#endif

	oInteract.Apply( d_maps, d_maps_spat, im_size);

#if defined VERBOSE
	Utility::write_image_device( "tmp/5_inter.png", d_maps, im_size);
#endif

	oNormalize.Apply( d_idata, d_maps, im_size);

#if defined VERBOSE
	Utility::write_image_device( "tmp/6_fusion.png", d_idata, im_size);
#endif

	oMask.Apply( d_odata, d_idata, im_size);

#if defined VERBOSE
	Utility::write_image_device( "tmp/7_mask.png", d_odata, im_size);

#endif	
	/*
	CUDA_CHECK( cudaDeviceSynchronize());

	CUDA_CHECK( cudaMemcpy( d_idata, in, ORIGINAL_SIZE, cudaMemcpyHostToDevice));

	oGaussianRecursive.Apply( d_temp, d_idata, im_size, 1, 1.0f);

	printf( "%d * %d",  im_size.w,im_size.h );

	oImageTransform.Resize( d_temp_scaled, d_temp, im_size_scaled, im_size);
	*/

	CUDA_CHECK( cudaMemcpy( out, d_odata, SCALED_SIZE, cudaMemcpyDeviceToHost));
}

void Static::Pathway::Clean()
{	
	d_idata = NULL; CUDA_CHECK( cudaFree( d_idata));
	d_odata = NULL; CUDA_CHECK( cudaFree( d_odata));

	d_data_freq = NULL; CUDA_CHECK( cudaFree( d_data_freq));
	d_data_spat = NULL; CUDA_CHECK( cudaFree( d_data_spat));

	d_maps = NULL; CUDA_CHECK( cudaFree( d_maps));

	d_maps_freq = NULL; CUDA_CHECK( cudaFree( d_maps_freq));	
	d_maps_spat = NULL; CUDA_CHECK( cudaFree( d_maps_spat));

	//CUDA_CHECK( cudaThreadExit());
}
