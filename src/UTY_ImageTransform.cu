
// includes
#include "error.hpp"
#include "UTY_ImageTransform.hpp"

texture<float, 2, cudaReadModeElementType> texInput;

//Round a / b to nearest higher integer value
int iDivUpT(int a, int b){
	return (a % b != 0) ? (a / b + 1) : (a / b);
}

void Utility::ImageTransform::Init()
{
	///// Allocate, set up data structures
	int OUTPUT_W, OUTPUT_H, OUTPUT_SIZE, INPUT_W, INPUT_H, POINTS_SIZE;

	INPUT_W = im_size_in_.w;
	INPUT_H = im_size_in_.h;

	// number of points mode
	OUTPUT_W = im_size_out_.w;
	OUTPUT_H = im_size_out_.h;
	OUTPUT_SIZE = OUTPUT_W * OUTPUT_H * sizeof(float);
	POINTS_SIZE = 2*OUTPUT_SIZE;

	// we want N evenly spaced points from 0 to 1
	f_points = (float2 *)malloc(POINTS_SIZE);

	for ( int r=0; r<OUTPUT_H; r++ ) {
		for ( int c=0; c<OUTPUT_W; c++ ) {
			f_points[c + OUTPUT_W*r].x = (float) c * (INPUT_W-1) / (OUTPUT_W-1) + 0.5f;
			f_points[c + OUTPUT_W*r].y = (float) r * (INPUT_H-1) / (OUTPUT_H-1) + 0.5f;
		}
	}

	// Allocate, copy points data into a float2*
	CUDA_CHECK( cudaMalloc((void **)&d_points, POINTS_SIZE));
	CUDA_CHECK( cudaMemcpy(d_points, f_points, POINTS_SIZE, cudaMemcpyHostToDevice));
	
	// Allocate, copy input data into a 2D texture
	cudaChannelFormatDesc input_tex = cudaCreateChannelDesc<float>();
	CUDA_CHECK( cudaMallocArray(&d_input, &input_tex, INPUT_W, INPUT_H));

	texInput.filterMode = cudaFilterModeLinear;
	texInput.normalized = 0;

}

void Utility::ImageTransform::Clean()
{	
	CUDA_CHECK( cudaUnbindTexture(texInput));
	d_input = NULL; CUDA_CHECK( cudaFreeArray(d_input));
	d_points = NULL; CUDA_CHECK( cudaFree(d_points));

	free(f_points);
}

void Utility::ImageTransform::Resize(
float *out
, float *in
, siz_t im_size_out
, siz_t im_size_in										
)
{

	///// Allocate, set up data structures
	int OUTPUT_W, OUTPUT_H, OUTPUT_SIZE, INPUT_W, INPUT_H, INPUT_SIZE, POINTS_SIZE;

	INPUT_W = im_size_in_.w;
	INPUT_H = im_size_in_.h;
	INPUT_SIZE = INPUT_H * INPUT_W * sizeof(float);

	float2 *f_points;

	// number of points mode
	OUTPUT_W = im_size_out_.w;
	OUTPUT_H = im_size_out_.h;
	OUTPUT_SIZE = OUTPUT_W * OUTPUT_H * sizeof(float);
	POINTS_SIZE = 2*OUTPUT_SIZE;

	// we want N evenly spaced points from 0 to 1
	f_points = (float2 *)malloc(POINTS_SIZE);

	for ( int r=0; r<OUTPUT_H; r++ ) {
		for ( int c=0; c<OUTPUT_W; c++ ) {
			f_points[c + OUTPUT_W*r].x = (float) c * (INPUT_W-1) / (OUTPUT_W-1) + 0.5f;
			f_points[c + OUTPUT_W*r].y = (float) r * (INPUT_H-1) / (OUTPUT_H-1) + 0.5f;
		}
	}

	// Allocate, copy points data into a float2*
	//	float2 *d_points;
	//	CUDA_CHECK( cudaMalloc((void **)&d_points, POINTS_SIZE));
	CUDA_CHECK( cudaMemcpy(d_points, f_points, POINTS_SIZE, cudaMemcpyHostToDevice));
	
	// Allocate, copy input data into a 2D texture
	//	cudaArray *d_input;
	//	cudaChannelFormatDesc input_tex = cudaCreateChannelDesc<float>();

	//	CUDA_CHECK( cudaMallocArray(&d_input, &input_tex, INPUT_W, INPUT_H));

	CUDA_CHECK( cudaMemcpyToArray(d_input, 0, 0, in, INPUT_SIZE, cudaMemcpyDeviceToDevice));

	texInput.filterMode = cudaFilterModeLinear;
	texInput.normalized = 0;

	CUDA_CHECK( cudaBindTextureToArray(texInput, d_input));

	// Set up blocks, grid for parallel processing
	dim3 dimBlock(16, 12);
	dim3 dimGrid(iDivUpT(OUTPUT_W,dimBlock.x),iDivUpT(OUTPUT_H,dimBlock.y));

	// Run it
	KernelBilinearInterpolation<<<dimGrid, dimBlock>>>( out, d_points, OUTPUT_W*OUTPUT_H, OUTPUT_W);
	CUDA_CHECK( cudaDeviceSynchronize());

	CUDA_CHECK( cudaUnbindTexture(texInput));
	//	d_input = NULL; CUDA_CHECK( cudaFreeArray(d_input));
	//	CUDA_CHECK( cudaFree(d_points));

	free(f_points);
	/*

	///// Allocate, set up data structures
	int OUTPUT_W, OUTPUT_H, OUTPUT_SIZE, INPUT_W, INPUT_H, INPUT_SIZE, POINTS_SIZE;

	INPUT_W = im_size_in.w;
	INPUT_H = im_size_in.h;
	INPUT_SIZE = INPUT_H * INPUT_W * sizeof(float);

	// number of points mode
	OUTPUT_W = im_size_out.w;
	OUTPUT_H = im_size_out.h;
	OUTPUT_SIZE = OUTPUT_W * OUTPUT_H * sizeof(float);
	POINTS_SIZE = 2*OUTPUT_SIZE;

	printf("%d*%d : %d*%d\n",INPUT_W,INPUT_H,OUTPUT_W,OUTPUT_H);

	cudaChannelFormatDesc input_tex = cudaCreateChannelDesc<float>();
	CUDA_CHECK( cudaMallocArray(&d_input, &input_tex, INPUT_W, INPUT_H));
	CUDA_CHECK( cudaMemcpyToArray(d_input, 0, 0, in, INPUT_SIZE, cudaMemcpyDeviceToDevice));

	// Set up blocks, grid for parallel processing
	dim3 dimBlock(16, 12);
	dim3 dimGrid(iDivUpT(OUTPUT_W,dimBlock.x),iDivUpT(OUTPUT_H,dimBlock.y));

	// Run it
	KernelBilinearInterpolation<<<dimGrid, dimBlock>>>( out, d_points, OUTPUT_W*OUTPUT_H, OUTPUT_W);
	CUDA_CHECK( cudaDeviceSynchronize());
*/
}

void Utility::ImageTransform::BilinearInterpolation ( 
float *out
, float *in
, float2 *points
, siz_t im_size 
) 
{
	///// Allocate, set up data structures
	int OUTPUT_W, OUTPUT_H, INPUT_W, INPUT_H, INPUT_SIZE;

	INPUT_W = im_size.w;
	INPUT_H = im_size.h;
	INPUT_SIZE = INPUT_H * INPUT_W * sizeof(float);

	// number of points mode
	OUTPUT_W = im_size.w;
	OUTPUT_H = im_size.h;
/*
	// Allocate, copy input data into a 2D texture
	cudaArray *d_input;
	cudaChannelFormatDesc input_tex = cudaCreateChannelDesc<float>();

	CUDA_CHECK( cudaMallocArray(&d_input, &input_tex, INPUT_W, INPUT_H));
*/
	CUDA_CHECK( cudaMemcpyToArray(d_input, 0, 0, in, INPUT_SIZE, cudaMemcpyDeviceToDevice));

	texInput.filterMode = cudaFilterModeLinear;
	texInput.normalized = 0;

	CUDA_CHECK( cudaBindTextureToArray(texInput, d_input));

	// Set up blocks, grid for parallel processing
	dim3 dimBlock(16, 12);
	dim3 dimGrid(iDivUp(OUTPUT_W,dimBlock.x),iDivUp(OUTPUT_H,dimBlock.y));

	// Run it
	KernelBilinearInterpolation<<<dimGrid, dimBlock>>>( out, points, OUTPUT_W*OUTPUT_H, OUTPUT_W);
	CUDA_CHECK( cudaDeviceSynchronize());
/*
	CUDA_CHECK( cudaUnbindTexture(texInput));
	d_input = NULL; CUDA_CHECK( cudaFreeArray(d_input));
*/
}

__global__ void Utility::KernelBilinearInterpolation( 
float *out
, float2 *points
, int out_numel
, int out_width
)
{
	const int x = blockDim.x*blockIdx.x + threadIdx.x;
	const int y = blockDim.y*blockIdx.y + threadIdx.y;

	const int loc = y*out_width + x;

	if (loc < out_numel) { out[loc] = tex2D(texInput, points[loc].x, points[loc].y); }
}
