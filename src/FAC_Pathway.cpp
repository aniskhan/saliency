
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/gpu/gpu.hpp"

#include "DYN_Retina.hpp"
#include "FAC_Pathway.hpp"

#if defined VERBOSE
#include "UTY_Debug.hpp"
#endif

#include <iostream>
#include <iomanip>
#include <cmath>

template<class T>
void convertAndResize(const T& src, T& gray, T& resized, double scale)
{
	if( src.channels() == 3)
	{
		cvtColor( src, gray, CV_BGR2GRAY );
	}
	else
	{
		gray = src;
	}

	cv::Size sz(cvRound(gray.cols * scale), cvRound(gray.rows * scale));

	if( scale != 1)
	{
		cv::gpu::resize(gray, resized, sz);
	}
	else
	{
		resized = gray;
	}
}

void Face::Pathway::PostProcess(
								const std::vector<cv::Rect> faces
								, std::vector<int> &weights
								, siz_t im_size
								)
{
	std::vector<float> eccentricity;
	std::vector<float> area;

	unsigned int cx = im_size.w / 2;
	unsigned int cy = im_size.h / 2;

	for( unsigned int i = 0; i < faces.size(); ++i)
	{
		unsigned int r1 = faces[i].width/2;
		unsigned int r2 = faces[i].height/2;

		unsigned int fx = faces[i].x + r1;
		unsigned int fy = faces[i].y + r2;

		unsigned int a = abs( (int) (fx-cx));
		unsigned int b = abs( (int) (fy-cy));

		float ang = atanf( (float) (b/a));

		float r = r1*r2 / sqrt(powf(r2*cos(ang),2)+powf(r1*sin(ang),2));

		float E = fabs(r - sqrt(powf( (float) ( fx-cx),2) + powf( (float) ( fy-cy),2)));
		float A = _PI * r1*r2;

		eccentricity.push_back( E);
		area.push_back( A);
	}

	/**************************************************************************/

	float minE = std::numeric_limits<float>::max();
	float maxE = std::numeric_limits<float>::min();
	float maxA = std::numeric_limits<float>::min();

	for( unsigned int i = 0; i < faces.size(); ++i)
	{
		if( minE>eccentricity[i]) minE = eccentricity[i];
		if( maxE<eccentricity[i]) maxE = eccentricity[i];
		if( maxA<area[i]) maxA = area[i];
	}

	/**************************************************************************/

	unsigned int idx_one, idx_two;

	float max_one, max_two;

	if(eccentricity[0] > eccentricity[1]) {
		max_two = eccentricity[1]; idx_two = 1;
		max_one = eccentricity[0]; idx_one = 0;
	} else {
		max_two = eccentricity[0]; idx_two = 0;
		max_one = eccentricity[1]; idx_one = 1;
	}

	for( unsigned int i = 2; i < faces.size(); i++){
		// use >= n not just > as max and second_max can hav same value. Ex:{1,2,3,3}   
		if(eccentricity[i] >= max_one){  
			max_two=max_one;  idx_two = idx_one;
			max_one=eccentricity[i]; idx_one = i;       
		}
		else if(eccentricity[i] > max_two){
			max_two=eccentricity[i]; idx_two = i;
		}
	}

	/**************************************************************************/

	unsigned int r11 = faces[idx_one].width / 2;
	unsigned int r12 = faces[idx_one].height / 2;

	unsigned int r21 = faces[idx_two].width / 2;
	unsigned int r22 = faces[idx_two].height / 2;

	unsigned int fx1 = faces[idx_one].x + r11;
	unsigned int fy1 = faces[idx_one].y + r12;

	unsigned int fx2 = faces[idx_two].x + r21;
	unsigned int fy2 = faces[idx_two].y + r22;

	float a = fabs((float)(fx1-fx2));
	float b = fabs((float)(fy1-fy2));

	float ang1 = atan2(b, a) * 180 / _PI;
	float ang2 = atan2(a, b) * 180 / _PI;

	float d1 = sqrt( 1 / (
		powf(sin(ang1)/r11,2) + 
		powf(cos(ang1)/r12,2)
		));

	float d2 = sqrt( 1 / (
		powf(sin(ang2)/r21,2) + 
		powf(cos(ang2)/r22,2)
		));

	float closeness = sqrt( 
		powf((float)(fx1-fx2),2) + 
		powf((float)(fy1-fy2),2)
		) - (d1+d2);

	if( closeness<0.0f) closeness=0.0f;

	/**************************************************************************/

	float e1 = eccentricity[idx_one] / maxE;
	float e2 = eccentricity[idx_two] / maxE;

	float a1 = area[idx_one] / maxA;
	float a2 = area[idx_two] / maxA;

	float f3 = (closeness - minE*0.5f) / maxE;
	if( f3<0) f3 = 0;

	float k = 6;

	float we1 = 2 * 1 / (1 + exp(-(-e1*k)));
	float we2 = 2 * 1 / (1 + exp(-(-e2*k)));

	float wa1 = 1 / (1 + exp(-a1*k));
	float wa2 = 1 / (1 + exp(-a2*k));

	float s = 1 / (1 + exp(-f3*k));
	float wc1 = s;
	float wc2 = 2*s*(1-s);

	float w;
	for( unsigned int i = 0; i < faces.size(); i++){
		if( i==idx_one) {

			w = (we1 + wa1 + wc1) / 2.5f;
			if( w<0.0f) w = 0.0f;

			weights[i] *= (int) w;
		}
		else if( i==idx_two) {

			w = (we2 + wa2 + wc2) / 2.5f;
			if( w<0.0f) w = 0.0f;

			weights[i] *= (int) w;
		}
		else {

			w = (we2 + wa2 + wc2) / 2.5f;
			if( w<0.0f) w = 0.0f;

			weights[i] *= (int)( 0.5f*w);
		}
	}
}

void Face::Pathway::GetFaceMap( 
							   float *out
							   , const std::vector<cv::Rect> faces
							   , const std::vector<int> weights
							   )
{
	unsigned int m;

	int X0, Y0;

	float 
		X, Y
		, a, b, c
		, sigma = 5;

	for( unsigned int i = 0; i < im_size_.w*im_size_.h; ++i)
		out[i] = 0.0f;

	for( unsigned int i = 0; i < faces.size(); ++i)
	{
		X0 = ( faces[i].width  - 1) / 2;
		Y0 = ( faces[i].height - 1) / 2;

		a = 1.0f / ( sigma*faces[i].width);
		b = 0.0f;
		c = 1.0f / ( sigma*faces[i].height);

		for( int y = 0; y < faces[i].height; ++y) {
			for( int x = 0; x < faces[i].width; ++x) {

				m = ( faces[i].y + y)*im_size_.w + ( faces[i].x + x);

				X = (float)( x - X0);
				Y = (float)( y - Y0);

				out[m] = weights[i] * exp( - (
					a * powf( X, 2.0f)
					+ b * X*Y
					+ c * powf( Y, 2.0f)
					));
			}
		}
	}
}

void Face::Pathway::GetFaceMap( 
							   float *out
							   , const std::vector<cv::Rect> faces
							   , const std::vector<int> weights
							   , siz_t im_size
							   )
{
	float rbegin  = -(im_size.h / 2.0f);
	float cbegin  = -(im_size.w / 2.0f);

	for( unsigned int i = 0; i < im_size_.w*im_size_.h; ++i)
		out[i] = 0.0f;

	for( unsigned int i = 0; i < faces.size(); ++i)
	{
		float yc = faces[i].y + faces[i].height/2.0f - im_size.h/2.0f;
		float xc = faces[i].x + faces[i].width /2.0f - im_size.w/2.0f;

		unsigned int sigmay = faces[i].height;
		unsigned int sigmax = faces[i].width;		

		float theta = 0.0f;
		float offset = 0.0f;

		for( unsigned int r=1; r<im_size_.h; ++r)
		{
			for( unsigned int c=1; c<im_size_.w; ++c)
			{
				float y = rbegin + r;
				float x = cbegin + c;

				theta   = (theta/180)*_PI;
				float xm      = (y-yc)*sin(theta) + (x-xc)*cos(theta);
				float ym      = (y-yc)*cos(theta) - (x-xc)*sin(theta);			
				float u       = powf(ym/sigmay,2) + powf(xm/sigmax,2);
				out[c + r*im_size.w] = offset + weights[i]*exp(-u/2);
			}
		}
	}
}

void Face::Pathway::Init()
{
	/*
	if( !cascade_gpu_frontal.load(cascadeName_))
	{
	std::cout << "ERROR: Could not load cascade classifier \"" << cascadeName_ << "\"" << std::endl;
	}
	*/

	/**************************************************************************/

	/*
	if( !cascade_gpu_profile.load("E:\ReleaseC\data\input\haarcascade_profileface.xml"))
	{
	std::cout << "ERROR: Could not load cascade classifier \"" << cascadeName_ << "\"" << std::endl;
	}
	*/

	/**************************************************************************/

	h_idata = (float*) malloc( size_ * sizeof(float));
	h_odata = (float*) malloc( size_ * sizeof(float));

	CUDA_CHECK( cudaMalloc( ( void**)&d_data, size_ * sizeof(float)));
}

void Face::Pathway::Apply(			
						  std::vector<cv::Rect> & faces
						  , std::vector<int> & weights
						  , const cv::Mat & im
						  )
{
	bool findLargestObject = false;
	bool filterRects = false;

	int detections_num;

	/**************************************************************************/

	if( !cascade_gpu_frontal.load(cascadeName_))
	{
		std::cout << "ERROR: Could not load cascade classifier \"" << cascadeName_ << "\"" << std::endl;
	}

	/**************************************************************************/

	for( unsigned int i=0 ; i<size_ ; ++i) 
	{
		h_idata[i] = im.data[i];
	}

	CUDA_CHECK( cudaMemcpy( d_data, h_idata, size_ * sizeof(float), cudaMemcpyHostToDevice));

	oRetina.RetinaFilter( d_data, d_data, im_size_, 1);

	CUDA_CHECK( cudaMemcpy( h_odata, d_data, size_ * sizeof(float), cudaMemcpyDeviceToHost));

#if defined VERBOSE
	Utility::write_image_device_v1( "tmp/1_f_ret.png", d_data, im_size_);
#endif

	float mn = std::numeric_limits<float>::max();
	float mx = std::numeric_limits<float>::min();
	for( unsigned int i=0 ; i<size_ ; ++i) 
	{
		h_odata[i] = h_odata[i] + h_idata[i];

		if( h_odata[i]>mx)
			mx = h_odata[i];

		if( h_odata[i]<mn)
			mn = h_odata[i];
	}

	for( unsigned int i=0 ; i<size_ ; ++i) 
		im.data[i] = ( char)( unsigned char)( (h_odata[i]-mn) / (mx-mn) * 255.0f);

	/**************************************************************************/

	frame_gpu.upload(im);

	convertAndResize(frame_gpu, gray_gpu, resized_gpu, scale_);

	// cascade_gpu.visualizeInPlace = true;
	// cascade_gpu.findLargestObject = findLargestObject;

	detections_num = cascade_gpu_frontal.detectMultiScale(resized_gpu, facesBuf_gpu, 1.2, 0);
	//                                                         ( filterRects || findLargestObject) ? 4 : 0);
	facesBuf_gpu.colRange(0, detections_num).download(faces_downloaded);

	//resized_gpu.download(out);

	facesBuf_cpu = faces_downloaded.ptr<cv::Rect>();

	for( int i = 0 ; i<detections_num ; ++i)
		faces.push_back( facesBuf_cpu[i]);

	/**************************************************************************/

	/*
	detections_num = cascade_gpu_profile.detectMultiScale(resized_gpu, facesBuf_gpu, 1.2, 0);
	//                                                         ( filterRects || findLargestObject) ? 4 : 0);
	facesBuf_gpu.colRange(0, detections_num).download(faces_downloaded);

	//resized_gpu.download(out);

	facesBuf_cpu = faces_downloaded.ptr<cv::Rect>();

	for( unsigned int i = 0 ; i<detections_num ; ++i)
	faces.push_back( facesBuf_cpu[i]);
	*/

	/**************************************************************************/

	const double GROUP_EPS = 0.2;

	groupRectangles(faces, 2, GROUP_EPS, &weights, 0);

	/**************************************************************************/

	cascade_gpu_frontal.release();	
}

void Face::Pathway::Clean() 
{
	facesBuf_gpu.release();
	frame_gpu.release();
	resized_gpu.release();
	gray_gpu.release();

	faces_downloaded.release();

	/**************************************************************************/

	d_data = NULL; CUDA_CHECK( cudaFree( d_data));

	free(h_idata);
	free(h_odata);

	CUDA_CHECK( cudaThreadExit());
}
