
// includes
#include "error.hpp"
#include "DYN_MedianFilter.hpp"

// defines
#define BLOCK_W 8
#define BLOCK_H 8

/// <summary>Applies the median filter.</summary>
/// <param name="out">Destination image</param>
/// <param name="in">Source image</param>
/// <param name="level_size">Image size</param>
/// <returns>Returns the smoothed image.</returns>
void Dynamic::MedianFilter::Apply( 
	float *out
	, float* in
	, siz_t level_size	
	) 
{
	dim3 dimb ( BLOCK_W, BLOCK_H, 1);
	dim3 dimg ( 
		level_size.w / BLOCK_W + 1*( level_size.w%BLOCK_W!=0)
		, level_size.h / BLOCK_H + 1*( level_size.h%BLOCK_H!=0)
		, 1
		);

	Dynamic::KernelMedianFilter<<<dimg, dimb>>>( out, in, level_size);
	CUDA_CHECK( cudaDeviceSynchronize());
}

/// <summary>GPU kernel to apply the median filter.</summary>
/// <param name="out">Destination image</param>
/// <param name="in">Source image</param>
/// <param name="level_size">Image size</param>
/// <returns>Returns the smoothed image.</returns>
__global__ void Dynamic::KernelMedianFilter( 
	float *out
	, float* in
	, siz_t level_size	
	) 
{
	__shared__ float window[BLOCK_W*BLOCK_H][9];

	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

	unsigned int tid = threadIdx.y*blockDim.y + threadIdx.x;

	if( x>=level_size.w || y>=level_size.h) return;

	window[tid][0] = ( y == 0				||	x == 0 				) ? 0.0f :	in[(y-1)*level_size.w + x-1];
	window[tid][1] = ( y == 0										) ?	0.0f :	in[(y-1)*level_size.w + x  ];
	window[tid][2] = ( y == 0				||	x == level_size.w-1	) ? 0.0f :	in[(y-1)*level_size.w + x+1];
	window[tid][3] = ( 							x == 0				) ? 0.0f :	in[	y	*level_size.w + x-1];
	window[tid][4] =															in[	y	*level_size.w + x  ];
	window[tid][5] = ( 							x == level_size.w-1	) ? 0.0f :	in[	y	*level_size.w + x+1];
	window[tid][6] = ( y == level_size.h-1	||	x == 0				) ? 0.0f :	in[(y+1)*level_size.w + x-1];
	window[tid][7] = ( y == level_size.h-1							) ? 0.0f :	in[(y+1)*level_size.w + x  ];
	window[tid][8] = ( y == level_size.h-1	||	x == level_size.w-1	) ? 0.0f :	in[(y+1)*level_size.w + x+1];

	syncthreads();

	// Order elements ( only half of them)
	for ( unsigned int j = 0;j < 5;++j)
	{
		// Find position of minimum element
		int min = j;
		for ( unsigned int l = j + 1;l < 9;++l)
			if ( window[tid][l] < window[tid][min])
				min = l;

		// Put found minimum element in its place
		const float temp	= window[tid][j];
		window[tid][j]		= window[tid][min];
		window[tid][min]	= temp;

		syncthreads();
	}

	out[y*level_size.w + x] = window[tid][4];
}
