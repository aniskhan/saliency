// includes
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "UTY_HelperProcs.hpp"

using namespace std;

float Utility::Mean(
float *im
, siz_t im_size
)
{
	float sum = 0.0f;
	
	for( unsigned int i = 1; i < im_size.w*im_size.h; i++)
	sum += im[i];

	return( sum  / (float)( im_size.w*im_size.h));
}

float Utility::Mean(
std::vector<int> values
)
{
	if( values.empty()) return 0;
	if( values.size()==1) return values[0];

	float sum = 0.0f;

	for( unsigned int i = 1; i < values.size(); i++)
	sum += values[i];

	return( sum / (float) values.size());
}

float Utility::StandardDeviation(
float *im
, siz_t im_size
)
{
	unsigned int i;
	float avg, sum01 = 0.0f, sum02 = 0.0f;

	for( i=1;i<im_size.w*im_size.h;i++)
	{
		sum01 += im[i];
		sum02 +=( im[i]*im[i]);
	}

	return( sqrt( sum02/( im_size.w*im_size.h) - powf( sum01/( im_size.w*im_size.h), 2)));
}

float Utility::Skewness(
float *im
, siz_t im_size
, float mu
, float std
)
{
	unsigned int i;
	float sum = 0.0f;

	for( i=1;i<im_size.w*im_size.h;i++)
	sum +=( im[i] - mu)*( im[i] - mu)*( im[i] - mu);

	return( ( float)( sum/( im_size.w*im_size.h)) / powf( std, 3));
}

float Utility::Min(
float *im
, siz_t im_size
)
{
	unsigned int i;
	float min = im[0];

	for( i=1;i<im_size.w*im_size.h;i++)
	if( im[i]< min)
	min = im[i];

	return min;
}

float Utility::Max(
float *im
, siz_t im_size
)
{
	unsigned int i;
	float max = im[0];

	for( i=1;i<im_size.w*im_size.h;i++)
	if( im[i]> max)
	max = im[i];

	return max;
}

void Utility::Fusion(
float *out
, float *im_static
, float *im_dynamic
, siz_t im_size				
)
{	
	float max_sta, max_dyn, min_sta, min_dyn, skw_dyn;

	min_dyn = Utility::Min( im_dynamic, im_size);
	max_dyn = Utility::Max( im_dynamic, im_size);

	min_sta = Utility::Min( im_static, im_size);
	max_sta = Utility::Max( im_static, im_size);

	skw_dyn = Utility::Skewness( im_dynamic, im_size, Utility::Mean( im_dynamic, im_size), Utility::StandardDeviation( im_dynamic, im_size));

	skw_dyn = ( skw_dyn < 0.0f) ? 0.0f : skw_dyn;

	for( unsigned int i = 0; i < im_size.w*im_size.h; i++)
	out[i] = max_sta*im_static[i] + skw_dyn*im_dynamic[i] + max_sta*skw_dyn*im_static[i]*im_dynamic[i];
}

void Utility::Fusion(
float *out
, float *im_static
, float *im_dynamic
, float *im_face
, std::vector<int> weights
, siz_t im_size			
)
{	
	float max_sta, skw_dyn, mu_fac;

	max_sta = Utility::Max( im_static, im_size);

	skw_dyn = Utility::Skewness( im_dynamic, im_size, Utility::Mean( im_dynamic, im_size), Utility::StandardDeviation( im_dynamic, im_size));
	skw_dyn = ( skw_dyn < 0.0f) ? 0.0f : skw_dyn;

	mu_fac  = Utility::Mean( weights);

	for( unsigned int i = 0; i < im_size.w*im_size.h; i++)
	out[i] = max_sta*im_static[i] 
	+ skw_dyn*im_dynamic[i]
	+  mu_fac*im_face[i]
	+ max_sta*skw_dyn * im_static[i] *im_dynamic[i]
	+ max_sta*mu_fac  * im_static[i] *im_face[i]
	+ skw_dyn*mu_fac  * im_dynamic[i]*im_face[i];
}

void Utility::Fusion(
					 float *out
					 , float *im_static
					 , float *im_dynamic
					 , float *im_face
					 , std::vector<int> weights
					 , siz_t im_size
					 , float &m01
					 , float &m02
					 , float &m03
					 )
{
	float m11 = Utility::Max( im_static, im_size);

	float m12 = Utility::Skewness( im_dynamic, im_size, Utility::Mean( im_dynamic, im_size), Utility::StandardDeviation( im_dynamic, im_size));
	m12 = ( m12 < 0.0f) ? 0.0f : m12;

	float m13  = Utility::Mean( weights);

	float sum = (m11 + m12 + m13);

	float m21 = m11 / sum;
	float m22 = m12 / sum;
	float m23 = m13 / sum;

	float m31 = m01*m21 + m01*m22 + m01*m23 + m02*m21 + m03*m21;
	float m32 = m02*m22 + m02*m21 + m02*m23 + m01*m22 + m03*m22;
	float m33 = m03*m23 + m03*m21 + m03*m22 + m01*m23 + m02*m23;

	sum = (m31 + m32 + m33);

	m31 = m31 / sum;
	m32 = m32 / sum;
	m33 = m33 / sum;	

	for( unsigned int i = 0; i < im_size.w*im_size.h; i++)
		out[i] = 
		m31 * im_static[i] + 
		m32 * im_dynamic[i] + 
		m33 * im_face[i] + 
		m31*m32 * im_static[i]*im_dynamic[i] + 
		m32*m33 * im_dynamic[i]*im_face[i] + 
		m31*m33 * im_static[i]*im_face[i];
}

void Utility::BilinearInterpolation(
float *out
, float *in
, float *vx
, float *vy
, siz_t im_size
)
{		
	float fraction_x, fraction_y, one_minus_x, one_minus_y;
	int ceil_x, ceil_y, floor_x, floor_y;

	float pix[4];

	for( int y = 0;y < im_size.h;++y) {
		for( int x = 0;x < im_size.w;++x) {

			if( vx[y*im_size.w+x]<0.0f || vx[y*im_size.w+x]>im_size.w-1 || 
					vy[y*im_size.w+x]<0.0f || vy[y*im_size.w+x]>im_size.h-1) {

				out[y*im_size.w+x] = 0.0f;
				continue;
			}

			floor_x =( int)floor( vx[y*im_size.w+x]);			
			floor_y =( int)floor( vy[y*im_size.w+x]);

			if( floor_x < 0) floor_x = 0;
			if( floor_y < 0) floor_y = 0;

			ceil_x = floor_x + 1;
			if( ceil_x >= im_size.w) ceil_x = floor_x;

			ceil_y = floor_y + 1;
			if( ceil_y >= im_size.h) ceil_y = floor_y;

			fraction_x = vx[y*im_size.w+x] - ( float)floor_x;
			fraction_y = vy[y*im_size.w+x] - ( float)floor_y;

			one_minus_x = 1.0f - fraction_x;
			one_minus_y = 1.0f - fraction_y;

			pix[0] = in[floor_y*im_size.w + floor_x];
			pix[1] = in[floor_y*im_size.w + ceil_x];
			pix[2] = in[ ceil_y*im_size.w + floor_x];
			pix[3] = in[ ceil_y*im_size.w + ceil_x];

			out[y*im_size.w + x] = one_minus_y * 
			( one_minus_x * pix[0] + fraction_x * pix[1]) + fraction_y *( one_minus_x * pix[2] + fraction_x * pix[3]);
		}
	}
}

int Utility::WriteFile(
char* filename
, float *im
, siz_t im_size
)
{
	FILE *file;
	file = fopen(filename,"w+");
	for(unsigned int i=0;i<im_size.w*im_size.h;++i)
	fprintf(file,"%.2f ", im[i]);
	fclose(file); /*done!*/
	return 0;
}

