
// includes
#include "error.hpp"
#include "DYN_MotionCompensation.hpp"

// defines

#define DATA_LINE_LEN 14

/**
* Constant memory for orientations and frequencies
*/
__constant__ float d_compensation_vars[DATA_LINE_LEN];

/*
Print results.
*/

void printresults(FILE *output, unsigned long i1, unsigned long i2,
				  CMotion2DModel model, double support_size,
				  unsigned nbsubsample, double multfactor)
{
	double row, col;
	double parameters[CMotion2DModel::MDL_NMAX_COEF];
	double var_light;

	CMotion2DModel _model;
	// Transform the model at the highest image resolution
	_model = model.getModelLevel(-nbsubsample);
	_model.getOrigin(row, col);
	_model.getParameters(parameters);

	if (output == stdout) {
		// Print the motion estimation results
		fprintf(output, "\t2D estimated model between images %lu and %lu:\n\t",
			i1, i2);

		fprintf(output, "origin: %f %f\n\t", row, col);

		string s = _model.idToString();
		fprintf(output, "motion model: %s\n", s.c_str());
		fprintf(output, "\tx: %f | %f %f | %f %f %f \n",
			parameters[0], parameters[2], parameters[3],
			parameters[6], parameters[7], parameters[8]);
		fprintf(output, "\ty: %f | %f %f | %f %f %f \n",
			parameters[1], parameters[4], parameters[5],
			parameters[9], parameters[10], parameters[11]);

		if (_model.getVarLight(var_light))
			fprintf(output, "\tglobal illumination: %f \n", var_light);

		if (support_size != -1.)
			fprintf(output, "\tsupport size: %f \n", support_size);
	}
	else {
		fprintf(output, "%lu %f %f ", i1, row, col);
		for (int i = 0; i < CMotion2DModel::MDL_NMAX_COEF; i++)
			fprintf(output, "%f ", parameters[i] * multfactor);
		if (!_model.getVarLight(var_light)) var_light = 0;
		fprintf(output, "%f ", var_light);
		if (support_size != -1.)
			fprintf(output, "%f ", support_size);
		fprintf(output, "\n");
	}
	fflush(output);
}

std::vector<float> Dynamic::MotionCompensation::FetchCompensationVariables( unsigned char* im1, unsigned char* im2)
{       
	for( unsigned int i=0 ; i<size_ ; ++i)
	{		
		sup[i] = 234;
	}

	// Fill the pyramid 1 whith image 1
	pyramid1.build(im1);

	// Fill the pyramid 2 with image 2
	pyramid2.build(im2);

	// 2D Parametric motion estimation

	bool state;

	//#define INIT_ESTIMATOR_WITH_MODEL
#ifdef INIT_ESTIMATOR_WITH_MODEL
	double parameters[CMotion2DModel::MDL_NMAX_COEF];

	for (int i=0; i < CMotion2DModel::MDL_NMAX_COEF; i ++)
		parameters[i] = 0.0;
	//       parameters[0] = -4.958804;
	//       parameters[1] = -0.243949;
	//       parameters[2] = 0.002562;
	//       parameters[3] = -0.023941;
	//       parameters[4] = 0.002625;
	//       parameters[5] = 0.008823;
	parameters[0] = 30.;
	parameters[1] = -23;
	model.setParameters(parameters);
	state = estimator.estimate(pyramid1, pyramid2, sup, label, model,
		useModelAsInitialization);

#else
	state = estimator.estimate(pyramid1, pyramid2, sup,
		label, model, useModelAsInitialization);
#endif

	if (state == false){
		cout << "\nError during motion estimation. " << endl
			<< "Possible reason: not enought gradient." << endl
			<< "The model is set to zero..." << endl << endl;
	}       

	// Exchange pyramid 1 and 2
	//        pyramid1.exchange(pyramid2);


	double row, col;
	double parameters[CMotion2DModel::MDL_NMAX_COEF];

	CMotion2DModel _model;
	// Transform the model at the highest image resolution
	_model = model.getModelLevel(-nbsubsample);
	_model.getOrigin(row, col);
	_model.getParameters(parameters);

	std::vector<float> compensation_vars;

	compensation_vars.push_back( row);
	compensation_vars.push_back( col);

	for (int i = 0; i < CMotion2DModel::MDL_NMAX_COEF; i++)
		compensation_vars.push_back( parameters[i] * multfactor);

	/*
	if (!_model.getVarLight(var_light)) compensation_vars[id++] = var_light;

	double support_size;
	if (! estimator.getSupportSize(support_size)) support_size = -1.;
	*/

/*
	// Print results
	double support_size;
	if (! estimator.getSupportSize(support_size)) support_size = -1.;

	printresults(stdout, 0, 1, model,
		support_size, nbsubsample, multfactor);
*/
	return compensation_vars;
}

__global__ void Dynamic::KernelCompensate(
	float2 *points
	, siz_t im_size
	)
{
	const int x = blockDim.x*blockIdx.x + threadIdx.x;
	const int y = blockDim.y*blockIdx.y + threadIdx.y;

	const int loc1 = y*im_size.w + x;        

	float t1, t2;

	if( loc1 < im_size.w*im_size.h) 
	{
		t1 = ( float)( x - d_compensation_vars[1]);
		t2 = ( float)( y - d_compensation_vars[0]);

		points[loc1].x = 
			d_compensation_vars[2] + 
			d_compensation_vars[4]*t1 + 
			d_compensation_vars[5]*t2 + 
			d_compensation_vars[8]*( t1*t1) + 
			d_compensation_vars[9]*( t1*t2) + 
			d_compensation_vars[10]*( t2*t2) + x;

		points[loc1].y = 
			d_compensation_vars[3] + 
			d_compensation_vars[6]*t1 + 
			d_compensation_vars[7]*t2 + 
			d_compensation_vars[11]*( t1*t1) + 
			d_compensation_vars[12]*( t1*t2) + 
			d_compensation_vars[13]*( t2*t2) + y;
	}
}


std::vector<float> Dynamic::MotionCompensation::FetchCompensationVariables(     
	const std::string filename
	, unsigned int frame_no
	)
{
	int n;
	float tmp;

	FILE *file = fopen ( (char*)filename.c_str(), "r");

	std::vector<float> compensation_vars;

	if ( file != NULL){
		char line[1000];

		while( fgets( line, sizeof line, file) != NULL)
		{
			n = -1;
			sscanf( &line[0], "%d", &n);

			if ( n==frame_no) 
			{
				char *p;

				p = strtok ( line, " ");

				if ( p != NULL) {

					sscanf( p, "%d", &n);

					for ( unsigned int i=0;i<DATA_LINE_LEN;i++) {

						p = strtok ( NULL, " ");                                                
						sscanf( p, "%f ", &tmp);

						compensation_vars.push_back( tmp);
					}

					break;
				}
			}
		}
		fclose( file);
	}
	else 
	{
		perror( (char*)filename.c_str());
	}

	return compensation_vars;
}


void Dynamic::MotionCompensation::Apply(
										float *out
										, float *in
										, const std::vector<float> compensation_vars
										, siz_t im_size										
										)
{
	Dynamic::MotionCompensation::Compensate( d_points, compensation_vars, im_size);

	oImageTransform.BilinearInterpolation( out, in, d_points, im_size);	
}

void Dynamic::MotionCompensation::Compensate(  
	float2 *points
	, const std::vector<float> h_compensation_vars
	, siz_t im_size
	) 
{
	// Set up blocks, grid for parallel processing
	dim3 dimBlock( 16, 12);
	dim3 dimGrid( iDivUp( im_size.w,dimBlock.x), iDivUp(im_size.h,dimBlock.y));

	CUDA_CHECK( cudaMemcpyToSymbol( d_compensation_vars, &h_compensation_vars[0], h_compensation_vars.size()*sizeof( float)));

	// Run it
	KernelCompensate<<<dimGrid, dimBlock>>>( points, im_size);
	CUDA_CHECK( cudaDeviceSynchronize());
}
/*
__global__ void Dynamic::KernelCompensate(
float2 *points
, const std::vector<float> compensation_vars
, siz_t im_size
)
{
const int x = blockDim.x*blockIdx.x + threadIdx.x;
const int y = blockDim.y*blockIdx.y + threadIdx.y;

const int loc1 = y*im_size.w + x;        

float t1, t2;

if( loc1 < im_size.w*im_size.h) 
{
t1 = ( float)( x - params[1]);
t2 = ( float)( y - params[0]);

points[loc1].x = 
params[2] + 
params[4]*t1 + 
params[5]*t2 + 
params[8]*( t1*t1) + 
params[9]*( t1*t2) + 
params[10]*( t2*t2) + x;

points[loc1].y = 
params[3] + 
params[6]*t1 + 
params[7]*t2 + 
params[11]*( t1*t1) + 
params[12]*( t1*t2) + 
params[13]*( t2*t2) + y;
}
}
*/
void Dynamic::MotionCompensation::Init()
{
	bool ierr = true;               // Image IO return value

	sup = (unsigned char *) malloc( size_*sizeof(unsigned char));

	CUDA_CHECK( cudaMalloc( (void **)&d_points, 2*size_ * sizeof(float)));

	// Initialisation of the 2D parametric motion model to estimate
	model.reset();  // Set all the parameters to zero
	model.setIdModel(model_id); // Set the model id
	model.setVarLight(var_light); // Set the illumination variation estimation

	// Initialize the model origin
	if ( ! model_orig_fixed) {
		// Origin fixed at the middle of the image
		model_row_orig = im_size_.h / 2.0;
		model_col_orig = im_size_.w / 2.0;
	}

	model.setOrigin(model_row_orig, model_col_orig); // Set the origin

	// Initialize the motion estimator
	// Set the pyramid start estimation level
	ierr = estimator.setFirstEstimationLevel(pyr_nlevels - 1);
	if (ierr == false) {
		exit(-1);
	}
	// Set the pyramid stop estimation level
	ierr = estimator.setLastEstimationLevel(pyr_stop_level);
	if (ierr == false) {
		exit(-1);
	}
	estimator.setRobustEstimator(true);

	if (compute_covariance)
		estimator.computeCovarianceMatrix(true);

	// Pyramids allocation for two successive images
	pyramid1.allocate(im_size_.h, im_size_.w, pyr_nlevels);
	pyramid2.allocate(im_size_.h, im_size_.w, pyr_nlevels);

}

void Dynamic::MotionCompensation::Clean()
{
	d_points = NULL; CUDA_CHECK( cudaFree( d_points));

	// Destruction des pyramides
	pyramid1.destroy();
	pyramid2.destroy();

	free( sup);
}
