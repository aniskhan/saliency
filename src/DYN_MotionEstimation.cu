
// includes
#include "error.hpp"
#include "DYN_MotionEstimation.hpp"

#define SIMPLIFIED

void Dynamic::MotionEstimation::Apply( 
float *vx
, float *vy
, float *gx
, float *gy
, float *gt
, siz_t level_size
, bool is_top_level
) 
{
	dim3 dimb( 16, 16, 1);
	dim3 dimg( 
	( level_size.w/dimb.x)   + 1*( level_size.w%dimb.x!=0)
	, ( level_size.h/dimb.y) + 1*( level_size.h%dimb.y!=0)
	, 1
	);

	Dynamic::KernelMotionEstimation<<<dimg, dimb>>>( vx, vy, gx, gy, gt, level_size, is_top_level);
	CUDA_CHECK( cudaDeviceSynchronize());
}
/*
__global__ void Dynamic::KernelMotionEstimation ( 
float *vx
, float *vy
, float *gx
, float *gy
, float *gt	
, siz_t level_size
, int addtype
)
{
	const int L = N * ( 2*N - 1);
	const float C2 = C*C;

	int 
	x, y
	, n, m, l, o
	, it
	, size = level_size.w*level_size.h;

	float 
	eqx[2*N], eqy[2*N], eqt[2*N]
	, dx, dy, ri, wi
	, Mx, My, W = 0.0f
	, diff, mxp, myp;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;
	
	if( x>=level_size.w || y>=level_size.h) return;
	m = x + y*level_size.w;

	for( n=0 ; n<2*N ; n++)
	{
		o = m + n*size;

		eqx[n] = gx[o];
		eqy[n] = gy[o];
		eqt[n] = gt[o];
	}

	Mx = 0.0f;
	My = 0.0f;
	W  = 0.0f;

	for( n=0 ; n<2*N ; n++)
	{
		for( o=0 ; o<n ; o++) 
		{
			diff = eqx[o]*eqy[n] - eqy[o]*eqx[n];

			if ( diff==0.0f) 
			{
				dx = 0.0f;
				dy = 0.0f;
			}
			else
			{
				dx = ( eqy[o]*eqt[n] - eqt[o]*eqy[n]) / diff;
				dy = ( eqt[o]*eqx[n] - eqx[o]*eqt[n]) / diff;
			}
	
			if( Mmax<=0.0f || ( fabsf( dx)<=Mmax && fabsf( dy)<=Mmax)) 
			{
			
				Mx += dx;
				My += dy;
				W  += 1.0f;
			}
		}
	}
	
	if( W!=0.0f)
	{
		Mx /= W;
		My /= W;
	}
	
	for( it=0;it<Pmax;it++)
	{
		mxp = Mx;
		myp = My;
		
		Mx = 0.0f;
		My = 0.0f;
		W  = 0.0f;

		for( n=0 ; n<2*N ; n++)
		{
			for( o=0 ; o<n ; o++) 
			{
				diff = eqx[o]*eqy[n] - eqy[o]*eqx[n];

				if ( diff==0.0f) 
				{
					dx = 0.0f;
					dy = 0.0f;
				}
				else
				{
					dx = ( eqy[o]*eqt[n] - eqt[o]*eqy[n]) / diff;
					dy = ( eqt[o]*eqx[n] - eqx[o]*eqt[n]) / diff;
	
					ri = sqrtf( powf( dx-mxp, 2.0f) + powf( dy-myp, 2.0f));
			
					if( fabsf( ri)<=C)
					{
						wi  = ( ri*ri - C2) / C2;
						wi *= wi;

						Mx += wi*dx;
						My += wi*dy;
						W  += wi;
					}
				}

			}
		}

		if ( W!=0.0f) 
		{
			Mx /= W;
			My /= W;
		}

		diff = sqrtf( powf( Mx-mxp, 2.0f) + powf( My-myp, 2.0f)) / sqrtf( powf( mxp, 2.0f) + powf( myp, 2.0f));
		
		if( diff<=Dmin) break;
	}

	if( addtype==0)
	{
		vx[m] = Mx;
		vy[m] = My;
	}
	else 
	{
		vx[m] += Mx;
		vy[m] += My;
	}
}
*/
/*
__global__ void Dynamic::KernelMotionEstimation ( 
float *vx
, float *vy
, float *gx
, float *gy
, float *gt	
, siz_t level_size
, bool is_top_level
)
{
	const int L = N * ( 2*N - 1);
	const float C2 = C*C;

	int 
	x, y
	, n, m, l, o
	, it
	, size = level_size.w*level_size.h;

	float 
	eqx[2*N], eqy[2*N], eqt[2*N]
	, dx[N*( 2*N-1)], dy[N*( 2*N-1)], ri[N*( 2*N-1)], wi[N*( 2*N-1)]
	, Mx, My, W = 0.0f
	, diff, mxp, myp;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;
	
	if( x>=level_size.w || y>=level_size.h) return;
	m = x + y*level_size.w;

	for( n=0 ; n<2*N ; n++)
	{
		o = m + n*size;

		eqx[n] = gx[o];
		eqy[n] = gy[o];
		eqt[n] = gt[o];
	}

	for( l=0, n=0 ; n<2*N ; n++)
	{
		for( o=0 ; o<n ; o++, l++) 
		{
			diff = eqx[o]*eqy[n] - eqy[o]*eqx[n];

			if ( diff==0.0f) 
			{
				dx[l] = 0.0f;
				dy[l] = 0.0f;
				wi[l] = 0.0f;
			}
			else
			{
				wi[l] = 1.0f;
				dx[l] = ( eqy[o]*eqt[n] - eqt[o]*eqy[n]) / diff;
				dy[l] = ( eqt[o]*eqx[n] - eqx[o]*eqt[n]) / diff;
			}
		}
	}

	Mx = 0.0f;
	My = 0.0f;
	W  = 0.0f;
	
	for( l=0 ; l<L ; l++)
	{
		if( Mmax>0.0f && ( fabsf( dx[l])>Mmax || fabsf( dy[l])>Mmax)) 
		wi[l] = 0.0f;
		else
		wi[l] = 1.0f;
		
		Mx += wi[l]*dx[l];
		My += wi[l]*dy[l];
		W  += wi[l];
	}
	
	if( W!=0.0f)
	{
		Mx /= W;
		My /= W;
	}
	
	for( it=0;it<Pmax;it++) 
	{
		for( l=0;l<L;l++)
		{
//			ri[l] = sqrtf( powf( dx[l]-Mx, 2.0f) + powf( dy[l]-My, 2.0f));
			ri[l] = fabsf( dx[l]-Mx) + fabsf( dy[l]-My);
			
			if( fabsf( ri[l])>C || wi[l]==0.0f)
			wi[l] = 0.0f;
			else 
			{
				wi[l]  = ( ri[l]*ri[l] - C2) / C2;
				wi[l] *= wi[l];
			}
		}

		mxp = Mx;
		myp = My;
		
		Mx = 0.0f;
		My = 0.0f;
		W  = 0.0f;
		
		for( l=0 ; l<L ; l++)
		{
			Mx += wi[l]*dx[l];
			My += wi[l]*dy[l];
			W  += wi[l];
		}
		if ( W!=0.0f) 
		{
			Mx /= W;
			My /= W;
		}

		diff = ( fabsf( Mx-mxp) + fabsf( My-myp)) / ( mxp + myp);
//		diff = ( powf( Mx-mxp, 2.0f) + powf( My-myp, 2.0f)) / ( powf( mxp, 2.0f) + powf( myp, 2.0f));
//		diff = sqrtf( powf( Mx-mxp, 2.0f) + powf( My-myp, 2.0f)) / sqrtf( powf( mxp, 2.0f) + powf( myp, 2.0f));
		
		if( diff<=Dmin) break;
	}

	if( is_top_level)
	{
		vx[m] = Mx;
		vy[m] = My;
	}
	else 
	{
		vx[m] += Mx;
		vy[m] += My;
	}
}
*/

__global__ void Dynamic::KernelMotionEstimation ( 
	float *vx
	, float *vy	
	, float *gx
	, float *gy
	, float *gt	
	, siz_t level_size
	, bool is_top_level
	)
{
	const float C2 = C*C;

	int 
		x, y
		, n, m, l, o
		, it
		, size = level_size.w*level_size.h;

	float 
		eqx[2*N], eqy[2*N], eqt[2*N];

	float 
		dx, dy, ri, wi
		, Mx, My, W = 0.0f
		, diff, mxp, myp
		, tmp;

	x = threadIdx.x + blockIdx.x*blockDim.x;
	y = threadIdx.y + blockIdx.y*blockDim.y;

	if( x>=level_size.w || y>=level_size.h) return;
	m = x + y*level_size.w;

	for( n=0 ; n<2*N ; n++)
	{
		o = m + n*size;

		eqx[n] = gx[o];
		eqy[n] = gy[o];
		eqt[n] = gt[o];
	}

	Mx = 0.0f; My = 0.0f; W  = 0.0f;

	for( l=0, n=0 ; n<2*N ; n++)
	{
		for( o=0 ; o<n ; o++, l++) 
		{
			diff = eqx[o]*eqy[n] - eqy[o]*eqx[n];

			if ( diff==0.0f) 
			{
				dx = 0.0f;
				dy = 0.0f;
				//wi = 0.0f;
			}
			else
			{
				//wi = 1.0f;
				dx = ( eqy[o]*eqt[n] - eqt[o]*eqy[n]) / diff;
				dy = ( eqt[o]*eqx[n] - eqx[o]*eqt[n]) / diff;
			}

			if( !( Mmax>0.0f && ( fabsf( dx)>Mmax || fabsf( dy)>Mmax)))
			{
				Mx += dx;
				My += dy;
				W  ++;
			}
		}
	}

	if( W!=0.0f)
	{
		Mx /= W;
		My /= W;
	}

	for( it=0 ; it<Pmax ; it++) 
	{
		mxp = Mx; myp = My;

		Mx = 0.0f; My = 0.0f; W  = 0.0f;

		for( l=0, n=0 ; n<2*N ; n++)
		{
			for( o=0 ; o<n ; o++, l++) 
			{
				diff = eqx[o]*eqy[n] - eqy[o]*eqx[n];

				tmp = 1.0f;
				if ( diff==0.0f) { tmp = 0.0f; diff=1.0f; }

				wi = tmp;
				dx = ( eqy[o]*eqt[n] - eqt[o]*eqy[n]) * tmp / diff;
				dy = ( eqt[o]*eqx[n] - eqx[o]*eqt[n]) * tmp / diff;

#if defined SIMPLIFIED				
				ri = fabsf( dx-mxp) + fabsf( dy-myp); // simple taxicab distance
#else
				ri = sqrtf( powf( dx-mxp, 2.0f) + powf( dy-myp, 2.0f));
#endif

				ri *= wi; 
				tmp = 1.0f;

#if defined SIMPLIFIED
				if( ri>C) tmp = 0.0f; // ri already absolute
#else
				if( fabsf( ri)>C) tmp = 0.0f;
#endif

				wi  = ( ( ri*ri - C2) / C2) * tmp;
				wi *= wi;

				Mx += wi*dx;
				My += wi*dy;
				W  += wi;
			}
		}

		if ( W==0.0f) W = 1.0f;

		Mx /= W;
		My /= W;

#if defined SIMPLIFIED
		
		// alternate form as all variables are positive
		//diff = sqrtf( ( powf( Mx-mxp, 2.0f) + powf( My-myp, 2.0f)) / ( powf( mxp, 2.0f) + powf( myp, 2.0f)));

		// taxicab distance / 2-norm
		diff = ( fabsf( Mx-mxp) + fabsf( My-myp)) / ( fabsf(mxp) + fabsf(myp));

		// similar to taxicab distance / 2-norm with better accuracy
		// diff = ( powf( Mx-mxp, 2.0f) + powf( My-myp, 2.0f)) / ( powf( mxp, 2.0f) + powf( myp, 2.0f));

		// similar to taxicab distance / 2-norm with best accuracy
		// diff = ( fabsf( Mx -mxp) + fabsf( My-myp)) / sqrtf( powf( mxp, 2.0f) + powf( myp, 2.0f));
#else
		diff = sqrtf( powf( Mx-mxp, 2.0f) + powf( My-myp, 2.0f)) / sqrtf( powf( mxp, 2.0f) + powf( myp, 2.0f));		
#endif

		if( diff<=Dmin) break;
	}

	if( is_top_level)
	{
		vx[m] = Mx;
		vy[m] = My;		
	}
	else 
	{
		vx[m] += Mx;
		vy[m] += My;
	}
}
