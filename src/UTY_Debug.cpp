#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <fstream>
#include <cufft.h>
#include "error.hpp"
#include "GPUDefines.h"
#include "UTY_Debug.hpp"

void Utility::save_txt( char *filename, float *data, siz_t im_size)
{ 
	std::fstream file_op(filename,std::ios::out);

	for( unsigned int i=0;i<im_size.w*im_size.h;++i) {
		file_op<<data[i]<<"\t";
	}

	file_op.close();
}


void Utility::save_txt_device( char *filename, float *data, siz_t im_size)
{ 
	std::fstream file_op(filename,std::ios::out);

	float *tmp = (float*)malloc(im_size.w*im_size.h * sizeof(float));

	CUDA_CHECK( cudaMemcpy( tmp, data, im_size.w*im_size.h * sizeof( float), cudaMemcpyDeviceToHost));

	for( unsigned int i=0;i<im_size.w*im_size.h;++i) {
		file_op<<tmp[i]<<"\t";
	}

	free(tmp);

	file_op.close();
}


void Utility::write_image( char *filename, float *data, siz_t im_size)
{
	cv::Mat	im;

	std::vector<int> params;

	params.push_back( CV_IMWRITE_PNG_COMPRESSION);
	params.push_back( 100);

	im = cvCreateMat( im_size.h, im_size.w, CV_8UC1);

	float *tmp = (float*)malloc(im_size.w*im_size.h * sizeof(float));

	//float min_s;
	//min_s = data[0];
	
	//for( unsigned int i=0;i<im.rows*im.cols;++i) {
	//	if( data[i] < min_s)
	//		min_s = data[i];
	//}

	//for( unsigned int i=0;i<im.rows*im.cols;++i) {
	//	//tmp[i] = data[i] - min_s;
	//	tmp[i] = abs(data[i]);
	//}

	float max_s;
	max_s = data[0];

	for( unsigned int i=0;i<im.rows*im.cols;++i) {
		if( data[i] > max_s)
			max_s = data[i];

		//tmp[i] = ( tmp[i] < 0) ? 0 : data[i];
	}

	for( unsigned int i=0;i<im.rows*im.cols;++i) {		
		im.data[i] =( char)( unsigned char)( data[i]/max_s * 255.0f);
	}

	cv::imwrite( filename, im, params);
}


void Utility::write_image_device( char *filename, float *data, siz_t im_size)
{
	cv::Mat	im;

	std::vector<int> params;

	params.push_back( CV_IMWRITE_PXM_BINARY);
	params.push_back( 100);

	im = cvCreateMat( im_size.h, im_size.w, CV_8UC1);

	float *tmp = (float*)malloc(im_size.w*im_size.h * sizeof(float));

	CUDA_CHECK( cudaMemcpy( tmp, data, im_size.w*im_size.h * sizeof( float), cudaMemcpyDeviceToHost));

	//float min_s;
	//min_s = tmp[0];
	//for( unsigned int i=0;i<im.rows*im.cols;++i) {
	//	if( tmp[i] < min_s)
	//		min_s = tmp[i];
	//}

	//for( unsigned int i=0;i<im.rows*im.cols;++i) {
	//	//tmp[i] += min_s;
	//	tmp[i] = abs(tmp[i]);
	//}

	float max_s;
	max_s = tmp[0];
	for( unsigned int i=0;i<im.rows*im.cols;++i) {
		if( tmp[i] > max_s)
			max_s = tmp[i];

		//if( tmp[i] < 0)
		//	tmp[i] = 0;
	}

	for( unsigned int i=0;i<im.rows*im.cols;++i) {
		im.data[i] =( char)( unsigned char)( tmp[i]/max_s * 255.0f);
	}

	cv::imwrite( filename, im, params);

	free(tmp);
}

void Utility::write_image_device_v1( char *filename, float *data, siz_t im_size)
{
	cv::Mat	im;

	std::vector<int> params;

	params.push_back( CV_IMWRITE_PXM_BINARY);
	params.push_back( 100);

	im = cvCreateMat( im_size.h, im_size.w, CV_8UC1);

	float *tmp = (float*)malloc(im_size.w*im_size.h * sizeof(float));

	CUDA_CHECK( cudaMemcpy( tmp, data, im_size.w*im_size.h * sizeof( float), cudaMemcpyDeviceToHost));

	//float min_s;
	//min_s = tmp[0];
	//for( unsigned int i=0;i<im.rows*im.cols;++i) {
	//	if( tmp[i] < min_s)
	//		min_s = tmp[i];
	//}

	//for( unsigned int i=0;i<im.rows*im.cols;++i) {
	//	//tmp[i] += min_s;
	//	tmp[i] = abs(tmp[i]);
	//}

	float max_s, min_s;
	max_s = tmp[0];
	min_s = tmp[0];
	for( unsigned int i=0;i<im.rows*im.cols;++i) {
		if( tmp[i] > max_s)
			max_s = tmp[i];

		if( tmp[i] < min_s)
			min_s = tmp[i];
	}

	for( unsigned int i=0;i<im.rows*im.cols;++i) {
		im.data[i] =( char)( unsigned char)( (tmp[i] - min_s) / (max_s - min_s) * 255.0f);
	}

	cv::imwrite( filename, im, params);

	free(tmp);
}

void Utility::write_image_device( char *filename, complex_t *data, siz_t im_size)
{
	cv::Mat	im;

	std::vector<int> params;

	params.push_back( CV_IMWRITE_PXM_BINARY);
	params.push_back( 100);

	im = cvCreateMat( im_size.h, im_size.w, CV_8UC1);
	float max_s;
	
	complex_t *tmp = (complex_t*)malloc(im_size.w*im_size.h * sizeof(complex_t));

	CUDA_CHECK( cudaMemcpy( tmp, data, im_size.w*im_size.h * sizeof( complex_t), cudaMemcpyDeviceToHost));

	float *res = (float*)malloc(im_size.w*im_size.h * sizeof(float));

	max_s = tmp[0][0];
	for( unsigned int i=0;i<im.rows*im.cols;++i) {
		res[i] = sqrt(tmp[i][0]*tmp[i][0] + tmp[i][1]*tmp[i][1]);
		if( res[i] > max_s)
			max_s = res[i];
	}

	for( unsigned int i=0;i<im.rows*im.cols;++i) {
		im.data[i] =( char)( unsigned char)( res[i]/max_s * 255.0f);
	}

	cv::imwrite( filename, im, params);

	free(tmp);
}

void Utility::write_image_device( char *filename, cufftComplex *data, siz_t im_size)
{
	cv::Mat	im;

	std::vector<int> params;

	params.push_back( CV_IMWRITE_PXM_BINARY);
	params.push_back( 100);

	im = cvCreateMat( im_size.h, im_size.w, CV_8UC1);
	float max_s;
	
	cufftComplex *tmp = (cufftComplex*)malloc(im_size.w*im_size.h * sizeof(cufftComplex));

	CUDA_CHECK( cudaMemcpy( tmp, data, im_size.w*im_size.h * sizeof( cufftComplex), cudaMemcpyDeviceToHost));

	float *res = (float*)malloc(im_size.w*im_size.h * sizeof(float));

	max_s = tmp[0].x;
	for( unsigned int i=0;i<im.rows*im.cols;++i) {
		res[i] = sqrt(tmp[i].x*tmp[i].x + tmp[i].y*tmp[i].y);
		if( res[i] > max_s)
			max_s = res[i];
	}

	for( unsigned int i=0;i<im.rows*im.cols;++i) {
		im.data[i] =( char)( unsigned char)( res[i]/max_s * 255.0f);
	}

	cv::imwrite( filename, im, params);

	free(tmp);
}

