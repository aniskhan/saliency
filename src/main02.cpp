/// <summary>
/// change 11:33
/// Four threads for a get and three compute operations.
/// Fusion thread is the master, invokes the get frame thread
/// Then invokes the other two compute threads, and waits for them to finish
/// Finally performs the fusion into final saliency map
/// </summary>
///**********************************************************************************
///********FLAGS(is_completed,is_input_ready,is_static_ready,is_dynamic_ready)*******
///**********************************************************************************
///************************┌▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┐************************************
///************************|	compute_fusion	|************************************
///************************|	(0111)		|************************************
///************************└▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┘************************************
///************************▲║********▲║********║▲************************************
///************************║║*(0100)*║║*(0100)*║║************************************
///*****************(0110)*║║*(1000)*║║********║║*(0101)*****************************
///************************║▼********║║********▼║************************************
///************┌▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┐***║║***┌▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┐************************
///************| compute_static  |***║║***| compute_dynamic |************************
///************|	 (0010)	 |***║║***|	(0001)	    |************************
///************└▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┘***║║***└▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┘************************
///**********************************║║**********************************************
///**********************************║║*(0000)***************************************
///***************************(0100)*║║*(1000)***************************************
///**********************************║▼**********************************************
///************************┌▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┐**************************************
///************************|	get_frame     |**************************************
///************************|	(0100)	      |**************************************
///************************└▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬┘**************************************
///**********************************************************************************

// Utilities and system includes
#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>
#include <ctime>
#include <stdlib.h>
#include <assert.h>
#include <fstream>
#include <cmath>

#include <shrUtils.h>
#include <shrQATest.h>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "cutil_inline.h"

#include "inc/error.hpp"

#include "STA_Pathway.hpp"
#include "DYN_Pathway.hpp"

#include <UTY_HelperProcs.cpp>

boost::mutex g_mutex;	// mutex to synchronize access to buffer

static int frame_idx = 0;
static int max_images = 50;
static int queue_len = 13;
static float scale = 1.0f;
static int queue_idx = 0;
static bool queue_full = false;

void temporal_filtering( float *out, float** in, siz_t scaled_im_size);

bool is_completed = false;

bool is_input_ready 	= true;	// input buffer state
bool is_static_ready 	= true;	// static buffer state
bool is_dynamic_ready 	= true;	// dynamic buffer state

void GetFrameThread();
void ComputeStaticThread();
void ComputeDynamicThread();
void ComputeFusionThread();

/// <summary>
/// Variables declarations.
/// </summary>
float *h_idata
, *h_odata_s, *h_odata_d;

static int in_nbpixels, out_nbpixels
,in_nbpixels_cropped, out_nbpixels_cropped;

static siz_t in_size(720,576), out_size
, in_size_cropped, out_size_cropped;

static unsigned int in_offset_w = 0;
static unsigned int in_offset_h = 0;

static unsigned int out_offset_w = 0;
static unsigned int out_offset_h = 0;

cv::Mat frame, gray;
cv::VideoCapture capture; // open the default camera

std::vector<std::string> strs;
std::string video_name, full_video_name;

/// <summary>
/// Main program entry point.
/// <param name="argc">Argument count.</param>
/// <param name="argv">Arguments.</param>
/// <returns>TODO:: Returns status of program.</returns>
/// </summary>
int main(int argc, char** argv)
{
	cv::namedWindow( "Input",  CV_WINDOW_AUTOSIZE );
//	cv::namedWindow( "Static", CV_WINDOW_AUTOSIZE );
//	cv::namedWindow( "Dynamic",CV_WINDOW_AUTOSIZE );
	cv::namedWindow( "Fusion", CV_WINDOW_AUTOSIZE );

if (argc == 2) {
	full_video_name = argv[1];

	boost::split(strs, full_video_name, boost::is_any_of("./"));

	video_name = strs[4];

	capture.open(full_video_name); // open the default camera
}
else {
	capture.open("../Clip.avi"); // open the default camera
}

	if(!capture.isOpened()) { // check if we succeeded
		printf( "Unable to read input video." );
		return -1;
	}

	capture >> frame; // get a new frame from camera

	cv::cvtColor(frame, gray, CV_RGB2GRAY);

	in_size.h = gray.rows;
	in_size.w = gray.cols;

	////////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Input images are rescaled, if the size dimensions are not a multiple of 16.
	/// The reason is multiple of 16 being the ideal size of data access and processing on GPUs.
	/// Also, cufft library requires the data to be a multiple of 16.
	/// </summary>
	out_size.w = in_size.w * scale;
	out_size.h = in_size.h * scale;

	in_size_cropped.w = (int) ( floor(in_size.w/16.0f) * 16.0f);
	in_size_cropped.h = (int) ( floor(in_size.h/16.0f) * 16.0f);

	in_offset_w = (in_size.w - in_size_cropped.w) * scale;
	in_offset_h = (in_size.h - in_size_cropped.h) * scale;

	out_size_cropped.w = ( int)( in_size_cropped.w * scale);
	out_size_cropped.h = ( int)( in_size_cropped.h * scale);

	out_offset_w = (int)( ( out_size.w - out_size_cropped.w) * scale);
	out_offset_h = (int)( ( out_size.h - out_size_cropped.h) * scale);

	in_nbpixels  =  in_size.w *  in_size.h;
	out_nbpixels = out_size.w * out_size.h;

	in_nbpixels_cropped  =  in_size_cropped.w *  in_size_cropped.h;
	out_nbpixels_cropped = out_size_cropped.w * out_size_cropped.h;

	////////////////////////////////////////////////////////////////////////////

	h_idata	  = ( float *)malloc(  in_nbpixels_cropped * sizeof( float));
	h_odata_s = ( float *)malloc( out_nbpixels_cropped * sizeof( float));
	h_odata_d = ( float *)malloc( out_nbpixels_cropped * sizeof( float));
	
	boost::thread get_frame_thread( &GetFrameThread ); // create a get input frame thread
	boost::thread compute_static_thread( &ComputeStaticThread ); // start static pass thread
	boost::thread compute_dynamic_thread( &ComputeDynamicThread ); // start dynamic pass thread
	boost::thread compute_fusion_thread( &ComputeFusionThread ); // start fusion thread

	get_frame_thread.join(); // wait for timer_thread to finish
	compute_static_thread.join();
	compute_dynamic_thread.join();
	compute_fusion_thread.join();

	free( h_idata);
	free( h_odata_s);
	free( h_odata_d);	

//	cutilExit( argc, argv);
}

void GetFrameThread() // get input frame
{
	cv::Mat im = cv::Mat( in_size.h, in_size.w, CV_8UC1 );

// START write

	const bool askOutputType = false;

	int ex = static_cast<int>(capture.get(CV_CAP_PROP_FOURCC));     // Get Codec Type- Int form
/*
	const std::string NAME = "output.avi";

	cv::Size S = cv::Size((int) out_size_cropped.w, (int)out_size_cropped.h);    //Acquire input size

	cv::VideoWriter outputVideo;	// Open the output

	if (askOutputType)
		outputVideo.open(NAME, ex=-1, capture.get(CV_CAP_PROP_FPS), S, true);
	else   
        	outputVideo.open(NAME, ex,    capture.get(CV_CAP_PROP_FPS), S, true);

	std::cout  << "Ex : " << ex  << std::endl;
	std::cout  << "Name : " << NAME << std::endl;
	std::cout  << "FPS : "  << (int)capture.get(CV_CAP_PROP_FPS) << std::endl;
	std::cout  << "Size : " << (int)out_size_cropped.h << std::endl;

	if (!outputVideo.isOpened())
	{
        	std::cout  << "Could not open the output video for write: " << std::endl;
        	//return -1;
    	}
	
// END write
*/

	while(1) {

		while( is_input_ready && !is_completed);

		if( is_completed) break;

//		printf ("T1::Frame = %d\n", frame_idx);

		// READ new frame from camera
		capture >> frame;
		cv::cvtColor(frame,im,CV_RGB2GRAY);

		if (!im.empty()) {
//		if (!im.empty() && frame_idx<max_images) {

//			outputVideo << frame;

			for( int j=0;j<in_size_cropped.h;++j) {
				for( int i=0;i<in_size_cropped.w;++i) {
					h_idata[i + j*in_size_cropped.w] =  im.data[(i+in_offset_w) + (j+in_offset_h)*in_size.w];					
				}
			}

			cv::imshow("Input", im);
			char c = cv::waitKey(30);

			if( c == 27 ) break;

			{
				// get exclusive ownership of mutex (wait for light to tuen green)
				boost::mutex::scoped_lock lock_it( g_mutex ) ;
				// ok, now we have exclusive access to the light

				is_input_ready = true;

				// destructor for lock_it will release the mutex
			}
		}			
		else {
			// get exclusive ownership of mutex (wait for light to tuen green)
			boost::mutex::scoped_lock lock_it( g_mutex ) ;
			// ok, now we have exclusive access to the light

			is_completed = true;
			//is_input_ready = true;
			// destructor for lock_it will release the mutex
		}
	}

	printf ("T1::Ended\n");

	im.release();
}


void ComputeStaticThread() // get input frame
{
	cudaSetDevice(1);	

	/// <summary>
	/// Get device properties.
	/// </summary>
	int dev_id;
	cudaDeviceProp dev_props;

	// get number of SMs on this GPU
	cutilSafeCall(cudaGetDevice(&dev_id));
	cutilSafeCall(cudaGetDeviceProperties(&dev_props, dev_id));

	float mx;

	cv::Mat im_s = cv::Mat( out_size.h, out_size.w, CV_8UC1 );
	std::vector<int> params;

	params.push_back( CV_IMWRITE_PXM_BINARY);
	params.push_back( 100);

	for( int i=0;i<out_nbpixels;++i) {		
		im_s.data[i] = ( char)( unsigned char)( 0);
	}

	float *temp = (float*) calloc(sizeof(float), out_nbpixels);

	Static::Pathway oStaticPathway( in_size_cropped, 1.0f);

	while(1){

		while( (!is_input_ready || is_static_ready) && !is_completed);

		if( is_completed) break;

//		printf ("T2::Frame = %d\n", frame_idx);

		oStaticPathway.Apply( h_odata_s, h_idata, out_size_cropped, in_size_cropped);

		mx = h_odata_s[0];
		for( int i=0;i<out_nbpixels_cropped;++i) {
			if (h_odata_s[i]>mx) {

				mx = h_odata_s[i];
			}
		}

		for( int j=0;j<out_size_cropped.h;++j) {
			for( int i=0;i<out_size_cropped.w;++i) {

				im_s.data[(i+out_offset_w) + (j+out_offset_h)*out_size.w] =  ( char)( unsigned char)( h_odata_s[i + j*out_size_cropped.w]/mx*255.0f);
			}
		}

		std::stringstream ss;
		ss << "../images/static/" << video_name << "_"<< frame_idx << "_sta.png";

		cv::imwrite( ss.str(), im_s );
/*
		cv::imshow("Static", im_s);
		char c = cv::waitKey(30);

		if( c == 27 ) break;
*/
		{
			// get exclusive ownership of mutex (wait for light to tuen green)
			boost::mutex::scoped_lock lock_it( g_mutex ) ;
			// ok, now we have exclusive access to the light

			is_static_ready = true;	

			// destructor for lock_it will release the mutex	
		}
	}

	printf ("T2::Ended\n");

	im_s.release();
	free(temp);
}


void ComputeDynamicThread() // get input frame
{
	cudaSetDevice(0);

	/// <summary>
	/// Get device properties.
	/// </summary>
	int dev_id;
	cudaDeviceProp dev_props;

	// get number of SMs on this GPU
	cutilSafeCall(cudaGetDevice(&dev_id));
	cutilSafeCall(cudaGetDeviceProperties(&dev_props, dev_id));

	float mx;

	cv::Mat im_d = cv::Mat( out_size.h, out_size.w, CV_8UC1 );
	std::vector<int> params;

	params.push_back( CV_IMWRITE_PXM_BINARY);
	params.push_back( 100);

	for( int i=0;i<out_nbpixels;++i) {
		im_d.data[i] = ( char)( unsigned char)( 0);		
	}

	Dynamic::Pathway oDynamicPathway( in_size_cropped, 0.5f );

	float **maps = 0;
	maps = ( float**) malloc ( queue_len * sizeof( float*));
	for( int i=0;i<queue_len;++i)
		maps[i] = ( float*) malloc ( out_nbpixels_cropped * sizeof( float));

	float *temp = (float*) calloc(sizeof(float), out_nbpixels);

	while(1) {

		while( (!is_input_ready || is_dynamic_ready) && !is_completed);

		if (is_completed) break;

//		printf ("T3::Frame = %d\n", frame_idx);

		oDynamicPathway.Apply( h_odata_d, h_idata, out_size_cropped, in_size_cropped, "../Clip.txt", frame_idx);

		if(frame_idx!=0) {
/*
			// TODO maps should be local
			if ( queue_full) {

				for( int j=0;j<out_nbpixels_cropped;++j) {

					maps[queue_idx][j] = h_odata_d[j];
				}

				temporal_filtering( h_odata_d, maps, out_size_cropped);

				if( ++queue_idx == queue_len) queue_idx = 0;				
			}
			else {
				for( int j=0;j<out_nbpixels_cropped;++j) {

					maps[queue_idx][j] = h_odata_d[j];
				}

				if( ++queue_idx == queue_len) {

					queue_full = true;
					queue_idx = 0;
				}
			}
*/
			mx = h_odata_d[0];
			for( int j=0;j<out_nbpixels_cropped;++j) {

				if( h_odata_d[j]>mx)
					mx = h_odata_d[j];
			}

			for( int j=0;j<out_size_cropped.h;++j) {
				for( int i=0;i<out_size_cropped.w;++i) {

					im_d.data[(i+out_offset_w) + (j+out_offset_h)*out_size.w] =  ( char)( unsigned char)( h_odata_d[i + j*out_size_cropped.w]/mx*255.0f);
				}
			}

			std::stringstream ss;
			ss << "../images/dynamic/" << video_name << "_"<< frame_idx << "_dyn.png";
	
			cv::imwrite( ss.str(), im_d );
/*
			cv::imshow("Dynamic", im_d);
			char c = cv::waitKey(30);

			if( c == 27 ) break;
*/

		}

		{
			// get exclusive ownership of mutex (wait for light to tuen green)
			boost::mutex::scoped_lock lock_it( g_mutex ) ;
			// ok, now we have exclusive access to the light

			is_dynamic_ready = true;

			// destructor for lock_it will release the mutex
		}
	}

	printf ("T3::Ended\n");

	for( int i=0;i<queue_len;++i)
		free(maps[i]);
	free(maps);

	free(temp);
}

void ComputeFusionThread() // get input frame
{
	float mx;

	float *h_odata_sd = (float*) malloc( out_size_cropped.w*out_size_cropped.h * sizeof(float));	

	cv::Mat im = cv::Mat( in_size.h, in_size.w, CV_8UC1);

	cv::Mat im_sd = cv::Mat( out_size.h, out_size.w, CV_8UC1 );
	std::vector<int> params;

	params.push_back( CV_IMWRITE_PXM_BINARY);
	params.push_back( 100);

	for( int i=0;i<out_nbpixels;++i) {

		im_sd.data[i] = ( char)( unsigned char)( 0);		
	}

	while(1) {

		is_input_ready = false;

		while( !is_input_ready && !is_completed);

		if( is_completed) break;

		printf ("T4::Frame = %d\n", frame_idx);

		is_static_ready  = false;
		is_dynamic_ready = false;

		while( !is_static_ready || !is_dynamic_ready);

		if( frame_idx!=0) {

			Utility::Fusion( h_odata_sd, h_odata_s, h_odata_d, out_size_cropped);

			mx = h_odata_sd[0];
			for( int j=0;j<out_nbpixels_cropped;++j) {

				if( h_odata_sd[j]>mx)
					mx = h_odata_sd[j];
			}

			for( unsigned int j=0;j<out_size_cropped.h;++j) {
				for( unsigned int i=0;i<out_size_cropped.w;++i) {

					im_sd.data[(i+out_offset_w) + (j+out_offset_h)*out_size.w] =  ( char)( unsigned char)( h_odata_sd[i + j*out_size_cropped.w]/mx*255.0f);
				}
			}

			std::stringstream ss;
			ss << "../images/fusion/" << video_name << "_"<< frame_idx << "_fus.png";

			cv::imwrite( ss.str(), im_sd );

			cv::imshow("Fusion", im_sd);
			char c = cv::waitKey(30);

			if( c == 27 ) break;

		}

		++frame_idx;
	}
/*
	{
		// get exclusive ownership of mutex (wait for light to tuen green)
		boost::mutex::scoped_lock lock_it( g_mutex ) ;
		// ok, now we have exclusive access to the light

		is_completed = true;

		// destructor for lock_it will release the mutex
	}
*/
	printf ("T4::Ended\n");

	free( h_odata_sd);
}


/// <summary>
/// Temporal median filtering.
/// <param name="out">Destination image</param>
/// <param name="in">Source maps</param>
/// <param name="level_size">Image size</param>
/// <returns>Returns the smoothed dynamic salience map.</returns>
/// </summary>
void temporal_filtering(
						float *out
						, float** in
						, siz_t in_size_cropped
						)
{
	float *arr = (float *) malloc( queue_len*sizeof(float));

	float average;
	float tmp;

	unsigned int min_idx;

	int middle = queue_len/2;

	for( int idx=0;idx<in_size_cropped.w*in_size_cropped.h;++idx)
	{
		for( int i=0;i<queue_len;++i)
			arr[i] = in[i][idx];

		for( int i=0;i<queue_len-1;++i){

			min_idx = i;
			for( int j=i+1;j<queue_len;++j){

				if( arr[min_idx]>arr[j])
					min_idx = j;
			}

			tmp = arr[i];
			arr[i] = arr[min_idx];
			arr[min_idx] = tmp;
		}

		if( queue_len%2==0) average = ( float)( arr[middle-1]+arr[middle])/2;
		else average = arr[middle];

		out[idx] = average;
	}

	free(arr);
}

