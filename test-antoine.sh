
ipath=/home/anis/Documents/images

queuelen=5

mspath=$ipath/static
mdpath=$ipath/dynamic_q$queuelen
msdpath=$(printf "%s/fusion_q%d" $ipath $queuelen)

if [ -n "$mspath" ] ; then
rm -r $mspath
mkdir $mspath
echo "Static removed."
fi
if [ -n "$mdpath" ] ; then
rm -r $mdpath
mkdir $mdpath
echo "Dynamic removed."
fi
if [ -n "$mfpath" ] ; then
echo "Do nothing."
fi
if [ -n "$msdpath" ] ; then
rm -r $msdpath
mkdir $msdpath
echo "Fusion removed."
fi

FILES="$(find /home/anis/Documents/videos -type f -name *.avi)"

for file in $FILES ; do
	filename=$(basename "$file")
	filepath=$(dirname "$file")
	extension="${filename##*.}"
	filename="${filename%.*}"

	echo $filepath
	echo $filename

	./saliency --two-path -i $filepath -t $queuelen -m $msdpath --save-images $filename
done

